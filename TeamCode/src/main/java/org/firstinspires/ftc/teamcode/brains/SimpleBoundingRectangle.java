package org.firstinspires.ftc.teamcode.brains;

import org.firstinspires.ftc.teamcode.util.Vector2D;
import org.firstinspires.ftc.teamcode.util.Vector2D;

/**
 * This basic bounding rectangle is stationary and oriented with the field
 */
public class SimpleBoundingRectangle {

    protected Vector2D width = new Vector2D();
    protected Vector2D position = new Vector2D(); // location of the center of the object

    public SimpleBoundingRectangle(){

    }

    public SimpleBoundingRectangle(double x, double y, double dx, double dy){
        position.set(x, y);
        width.set(dx, dy);
    }

    public void setPosition(Vector2D position){
        this.position.set(position);
    }

    public Vector2D getPosition(){
        return position.copy();
    }

    public Vector2D getWidth() { return width.copy(); }

    public double getDx(){
        return width.getX();
    }

    public double getDy(){
        return width.getY();
    }

    // the radius of a bounding circle
    public double boundingRadius(){
        return width.magnitude()/2;
    }

    public boolean contains(Vector2D point){
        if(point.getX() < position.getX()-width.getX()/2) {
            return false;
        }
        if(point.getX() > position.getX()+width.getX()/2){
            return false;
        }
        if(point.getY() < position.getY()-width.getY()/2){
            return false;
        }
        if(point.getY() > position.getY()+width.getY()/2){
            return false;
        }
        return true;
    }

    public boolean havingCollision(SimpleBoundingRectangle rectangle){
        return collisionDistance(rectangle) <= 0;
    }

    public double collisionDistance(SimpleBoundingRectangle rectangle){
        return distanceBetweenCenterPoints(rectangle) - boundingRadius() - rectangle.boundingRadius();
    }

    public double distanceBetweenCenterPoints(SimpleBoundingRectangle rectangle){
        // distance from center of one to the center of the other
        return rectangle.getPosition().subtract(position).magnitude();
    }

}
