package org.firstinspires.ftc.teamcode;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.hardware.bosch.NaiveAccelerationIntegrator;
import com.qualcomm.hardware.rev.Rev2mDistanceSensor;
import com.qualcomm.hardware.rev.RevColorSensorV3;
import com.qualcomm.hardware.rev.RevTouchSensor;
import com.qualcomm.robotcore.hardware.CompassSensor;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.GyroSensor;
import com.qualcomm.robotcore.hardware.Gyroscope;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.TouchSensor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcontroller.external.samples.SensorBNO055IMU;
import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AngularVelocity;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.Position;
import org.firstinspires.ftc.robotcore.external.navigation.Velocity;
import org.firstinspires.ftc.teamcode.brains.Knowledge;
import org.firstinspires.ftc.teamcode.util.Vector2D;
import org.firstinspires.ftc.teamcode.util.Vector3D;

import java.util.Locale;

// Values recorded here are directly observed from sensors
public class Sensors {
    Telemetry telemetry;

    private boolean initialized = false;

    public Vector3D orientation = new Vector3D();
    public Vector2D position2D = new Vector2D();

    // 9-DOF:
    // Magnetometer (indicates magnetic north, can be useful to correct gyroscope, but is subject to false magnetic fields)
    // Accelerometer (indicates robot acceleration)
    // Gyroscope (indicates robot orientation but may drift)
    private BNO055IMU gyro;
    private BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
    private Orientation angles;
    private Acceleration acceleration;
    private AngularVelocity angularVelocity;
    private Position position;

    // Time (time passed since the last data snapshot is useful for physics calculations)
    private ElapsedTime runtime = new ElapsedTime();

    // Color Sensor (not used)
    // RevColorSensorV3 revColorSensorV3;

    // Camera

    // Touch Sensors
    private TouchSensor touchFL;
    private TouchSensor touchFR;
    private TouchSensor touchBR;
    private TouchSensor touchBL;
    public boolean bFL;
    public boolean bFR;
    public boolean bBR;
    public boolean bBL;

    // Limiter Switches
    private DigitalChannel magneticElevatorLimiterSensor;
    public boolean elevatorLimiter;

    // Range Sensors
    private Rev2mDistanceSensor distanceSensorA;
    private Rev2mDistanceSensor distanceSensorB;
    public double distanceA;
    public double distanceB;
    public int deltaLeftMotorPosition;
    public int deltaRightMotorPosition;
    public int leftMotorPositionOld;
    public int rightMotorPositionOld;
    public int leftMotorPosition;
    public int rightMotorPosition;
    public int elevatorPosition;
    public int shoulderPosition;

    private double time = 0;
    private double lastTime = 0;
    private double dt = 0; // the time interval since the last update function call

    public double getDt(){
        return dt;
    }
    public double getTime() { return time; }

    public void initialize(HardwareMap hardwareMap, Telemetry telemetry, Knowledge knowledge){

        this.telemetry = telemetry;

        try {
            //revColorSensorV3 = hardwareMap.get(RevColorSensorV3.class, "color");
            distanceSensorA = hardwareMap.get(Rev2mDistanceSensor.class, "distanceA");
            distanceSensorB = hardwareMap.get(Rev2mDistanceSensor.class, "distanceB");

            // initialize the position to start location and velocity to zero, int indicating poll frequency in milliseconds 100 is 10x/sec
            Vector2D teamStartPosition = knowledge.teamStartPosition.getPosition();
            Position startPosition = new Position();
            startPosition.x = teamStartPosition.getX();
            startPosition.y = teamStartPosition.getY();
            startPosition.z = 0.0;
            Velocity startVelocity = new Velocity();
            startVelocity.xVeloc = 0;
            startVelocity.yVeloc = 0;
            startVelocity.zVeloc = 0;

            gyro = hardwareMap.get(BNO055IMU.class, "imu");
            parameters.angleUnit = BNO055IMU.AngleUnit.DEGREES;
            parameters.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
            parameters.calibrationDataFile = "BNO055IMUCalibration.json";
            parameters.loggingEnabled = true;
            parameters.loggingTag = "IMU";
  //          parameters.accelerationIntegrationAlgorithm.initialize(parameters, startPosition, startVelocity);
            gyro.initialize(parameters);

 //           gyro.startAccelerationIntegration(startPosition, startVelocity, 100);

            touchFL = hardwareMap.get(TouchSensor.class, "touchFL");
            touchFR = hardwareMap.get(TouchSensor.class, "touchFR");
            touchBR = hardwareMap.get(TouchSensor.class, "touchBR");
            touchBL = hardwareMap.get(TouchSensor.class, "touchBL");

            magneticElevatorLimiterSensor = hardwareMap.get(DigitalChannel.class, "elevatorSwitch");
            //magneticLimiterB = hardwareMap.get(DigitalChannel.class, "magneticB");

            runtime.reset();

            initialized = true;
        }
        catch (Exception e){
            telemetry.addData("Sensor Init Failed! ", e.toString());
            telemetry.update();
        }
    }

    public void update(Actuators actuators) { // in use soon
        try {
            time = runtime.seconds();
            dt = time - lastTime;
            lastTime = time;

            bFL = touchFL.isPressed();
            bFR = touchFR.isPressed();
            bBR = touchBR.isPressed();
            bBL = touchBL.isPressed();

            elevatorLimiter = !(magneticElevatorLimiterSensor.getState());

            distanceA = distanceSensorA.getDistance(DistanceUnit.INCH);
            distanceB = distanceSensorB.getDistance(DistanceUnit.INCH);

            leftMotorPosition = actuators.getLeftMotorPosition();
            rightMotorPosition = actuators.getRightMotorPosition();
            deltaLeftMotorPosition = leftMotorPosition - leftMotorPositionOld;
            deltaRightMotorPosition = rightMotorPosition - rightMotorPositionOld;
            leftMotorPositionOld = leftMotorPosition;
            rightMotorPositionOld = rightMotorPosition;

            elevatorPosition = actuators.getElevatorPosition();
            shoulderPosition = actuators.getShoulderPosition();

            // TODO: Some of these steps are unnecessary and can be removed, then re-tested
            angles   = gyro.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
            double heading = formatAngle(angles.angleUnit, angles.firstAngle);
            double roll  = formatAngle(angles.angleUnit, angles.secondAngle);
            double pitch = formatAngle(angles.angleUnit, angles.thirdAngle);
            orientation.set(pitch, roll, heading);

            acceleration = gyro.getOverallAcceleration();
            angularVelocity = gyro.getAngularVelocity();
            position = gyro.getPosition();//.toUnit(DistanceUnit.INCH);
            position2D.set(position.x, position.y);

            telemetry.addData("Touch FL: ", bFL);
            telemetry.addData("Touch FR: ", bFR);
            telemetry.addData("Touch BR: ", bBR);
            telemetry.addData("Touch BL: ", bBL);

            telemetry.addData("Elevator Switch: ", elevatorLimiter);

            telemetry.addData("Distance A: ", distanceA);
            telemetry.addData("Distance B: ", distanceB);
            telemetry.addData("Position: ", position.toString());
            telemetry.addData("Orientation: ", angles.toString());
        }

        catch (Exception e) {
            telemetry.addData("Sensor Test Update Ouch!", e.toString());
        }
    }

    Double formatAngle(AngleUnit angleUnit, double angle) {
        return formatDegrees(AngleUnit.DEGREES.fromUnit(angleUnit, angle));
    }

    Double formatDegrees(double degrees){
        return AngleUnit.DEGREES.normalize(degrees);
    }
}
