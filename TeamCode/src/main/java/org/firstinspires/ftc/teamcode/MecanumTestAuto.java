package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

@Autonomous(name = "Mecanum Auto", group = "Auto")
public class MecanumTestAuto extends LinearOpMode {
    public void runOpMode() throws InterruptedException {
        // Initialize

        DistanceSensor rangeSensorA = hardwareMap.get(DistanceSensor.class, "rangeSensorA");
        DistanceSensor rangeSensorB = hardwareMap.get(DistanceSensor.class, "rangeSensorB");

        DcMotor frontLeftDriveMotor = this.hardwareMap.dcMotor.get("frontLeft");
        DcMotor backLeftDriveMotor = this.hardwareMap.dcMotor.get("backLeft");
        DcMotor frontRightDriveMotor = this.hardwareMap.dcMotor.get("frontRight");
        DcMotor backRightDriveMotor = this.hardwareMap.dcMotor.get("backRight");

        frontLeftDriveMotor.setDirection(DcMotor.Direction.REVERSE);
        backLeftDriveMotor.setDirection(DcMotor.Direction.REVERSE);

        frontLeftDriveMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backLeftDriveMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontRightDriveMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backRightDriveMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        // Wait for the game to start (driver presses PLAY)
        telemetry.addData("Waiting for Play", "Wait for Referees and then Press Play");
        telemetry.update();
        waitForStart();

        // Run autonomous
        double startTime = this.time;
        while(this.time - startTime < 100) {
            frontLeftDriveMotor.setPower(0.00);
            backLeftDriveMotor.setPower(0.00);
            frontRightDriveMotor.setPower(0.00);
            backRightDriveMotor.setPower(0.00);

            double rangeA = rangeSensorA.getDistance(DistanceUnit.CM);
            double rangeB = rangeSensorB.getDistance(DistanceUnit.CM);

            telemetry.addData("Sensor A: ", rangeA);
            telemetry.addData("Sensor B: ", rangeB);
            telemetry.update();

        }
        frontLeftDriveMotor.setPower(0);
        backLeftDriveMotor.setPower(0);
        frontRightDriveMotor.setPower(0);
        backRightDriveMotor.setPower(0);


    }
}