package org.firstinspires.ftc.teamcode.scripts;

import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.brains.Knowledge;

public class TestDrive extends Script{
    double speed = 1.0;
    double bearing = 180;

    @Override
    public ReturnMessage run(Knowledge knowledge, GamePadState gamePadState) {
        if(knowledge.sensors.getTime() < 5) {
            gamePadState.leftStickY = 0;
            gamePadState.rightStickX = 0;
        }
        else if(knowledge.sensors.getTime() < 8){
            gamePadState.leftStickY = Math.cos(Math.toRadians(bearing)) * speed;
            gamePadState.rightStickX = -Math.sin(Math.toRadians(bearing)) * speed;
        }
        else if(knowledge.sensors.getTime() < 13) {
            gamePadState.leftStickY = 0;
            gamePadState.rightStickX = 0;
        }
        else if(knowledge.sensors.getTime() < 16){
            bearing = 0;
            gamePadState.leftStickY = Math.cos(Math.toRadians(bearing)) * speed;
            gamePadState.rightStickX = -Math.sin(Math.toRadians(bearing)) * speed;
        }
        else{
            gamePadState.leftStickY = 0;
            gamePadState.rightStickX = 0;
        }
        return ReturnMessage.Working;
    }
}
