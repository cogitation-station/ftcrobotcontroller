package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;

public class RobotConfig {
    enum Drivetrain{
        Mechanum, Tracks;
    }
    public final static Drivetrain drivetrain = Drivetrain.Mechanum;

    enum MotorEncoders{
        Yes, No;
    }
    public final static MotorEncoders motorEncoders = MotorEncoders.No;

    public final static DcMotor.RunMode runMode = DcMotor.RunMode.RUN_TO_POSITION;  // use: RUN WITHOUT ENCODER, RUN TO POSITION, or RUN WITH ENCODER
}
