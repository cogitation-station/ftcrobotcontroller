package org.firstinspires.ftc.teamcode.analysis;

import org.firstinspires.ftc.teamcode.util.Vector3D;

import java.util.ArrayList;

public class ParametricArm {
    ArrayList<ParametricArmSegment> armSegments = new ArrayList<>();
    Vector3D position = new Vector3D(); // position of the base of the arm, where x is forward, y is port, z is up, as measured from ground level at the center of the base

    // returns the position of the end of the arm segment, using angle and length of each segment
    public Vector3D positionOfEndpoint(int seg){
        return reach(seg, 0, 0).add(position);
    }

    // recursive sum of length and height
    private Vector3D reach(int lastSeg, int curSeg, double totalAngle){
        if(curSeg > lastSeg){
            return new Vector3D();
        }
        ParametricArmSegment seg = armSegments.get(curSeg);
        double angle = totalAngle + seg.angle;
        return  reach(lastSeg, curSeg+1, angle).add(seg.length * Math.sin(Math.toRadians(angle)), 0, -seg.length * Math.cos(Math.toRadians(angle)));
    }

    public boolean armPositionWithinRangeOfMotion(){
        for(int i=0; i<armSegments.size(); i++){
            if(!armSegments.get(i).isAngleWithinRangeOfMotion()){
                return false;
            }
        }
        return true;
    }

    public boolean canReach(double height, double distance){
        return Math.sqrt(height*height + distance*distance) < armLength();
    }

    public double armLength(){
        double total = 0;
        for(int i=0; i<armSegments.size(); i++){
            total += armSegments.get(i).length;
        }
        return total;
    }

    public boolean canLift(double loadWeight, double height, double distance) {
        if(!canReach(height, distance))
            return false;
        ParametricArmPosition position = new ParametricArmPosition();
        return suppliedTorqueExceeds(requiredTorque(loadWeight, position));

    }

    public boolean suppliedTorqueExceeds(ArrayList<Double> loadTorques){
        return true;
    }

    public ArrayList<Double> requiredTorque(double loadWeight, ParametricArmPosition position){
        return new ArrayList<>();
    }

}
