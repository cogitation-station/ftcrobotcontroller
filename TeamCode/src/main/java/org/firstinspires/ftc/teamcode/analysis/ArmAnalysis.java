package org.firstinspires.ftc.teamcode.analysis;

import org.firstinspires.ftc.teamcode.util.Vector3D;

/**
 * ARM ANALYSIS:
 * The objective of this arm analysis is to find the arm design which optimizes a the driving factors.
 * Driving Factors:
 *      Fewest number of segments to do the job
 *      Smallest length of segments to do the job
 *      Minimum torque required at each joint to do the job
 *      Least material required to do the job
 * The Job is to pass the following tests:
 *      Pass Under
 *      Pass Over
 *      Can Fold and Fit in 18"
 *      Can Lift at (position, weight)
 *      Can Reach (position)
 *      Doesn't Break Under Load (weight)
 *      Doesn't Tip Over Under Load (position, weight)
 *
 * MODEL:
 * The parametric model of the robot and arm are represented by its data.
 *
 * SEARCH:
 * To find the best model, a search is conducted by iteration.
 */
public class ArmAnalysis {

    public boolean canFitInBox(ParametricArm parametricArm){
        // is the arm within its allowed range of motion
        if(!parametricArm.armPositionWithinRangeOfMotion()){
            return false;
        }

        // if any joints are outside the box, it doesn't fit
        for(int i=0; i<parametricArm.armSegments.size(); i++){
            if(isPointOutsideBox(parametricArm.positionOfEndpoint(i))){
                return false;
            }
        }
        // if any line segments of the arms intersect

        return true;
    }

    public boolean isPointOutsideBox(Vector3D p){
        if(p.x > 9.0) return true;
        if(p.x < -9.0) return true;
        if(p.y > 9.0) return true;
        if(p.y < -9.0) return true;
        if(p.z > 18.0) return true;
        if(p.z < 0.0) return true;
        return false;
    }


}
