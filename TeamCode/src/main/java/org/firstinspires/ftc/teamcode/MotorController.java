package org.firstinspires.ftc.teamcode;

public class MotorController {

    // Front Left Motor 00
    double frontLeft = 0.0;

    // Front Right Motor 01
    double frontRight = 0.0;

    // Back Right Motor 02
    double backRight = 0.0;

    // Back Left Motor 03
    double backLeft = 0.0;

    public void simpleMechanumUpdate(GamePadState gamePadState){

        double xL = flatten(gamePadState.leftStickX);
        double yL = flatten(gamePadState.leftStickY);
        double xR = flatten(gamePadState.rightStickX);
        double yR = flatten(gamePadState.rightStickY);

        // get the magnitude of the stick deflection
        double magnitude = Math.sqrt(xL*xL + yL*yL + xR*xR);

        // add the vector components, rescaled to a maximum of the magnitude given
        if(magnitude > .001) {
            frontLeft = -(-yL + xL + xR);//magnitude;
            frontRight = (-yL - xL - xR);//magnitude;
            backRight = (-yL + xL - xR);//magnitude;
            backLeft = -(-yL - xL + xR);//magnitude;
        }
        else{
            frontLeft = 0.0;
            frontRight = 0.0;
            backRight = 0.0;
            backLeft = 0.0;
        }
    }

    /**
     * when y is up, frontLeft and frontRight = 1
     *
     * when y is down, frontLeft and frontRight = -1
     * when x is right, frontLeft = 1, and frontRight = -1
     * when x is left, frontLeft = -1, and frontRight = 1
     * @param gamePadState
     */
    public void oneStickTracksUpdate(GamePadState gamePadState){
         double x = flatten(gamePadState.leftStickX);
         double y = flatten(gamePadState.leftStickY);

         // set front motors
         frontLeft = -y + x;
         frontRight = -y - x;
         if(frontRight > 1.0){
             frontRight = 1.0;
         }
         else if(frontRight < -1.0){
             frontRight = -1.0;
         }
        if(frontLeft > 1.0){
            frontLeft = 1.0;
        }
        else if(frontLeft < -1.0){
            frontLeft = -1.0;
        }

         // repeat for back motors
         backLeft = frontLeft;
         backRight = frontRight;
    }

    public void twoStickTracksUpdate(GamePadState gamePadState){
        double x = flatten(gamePadState.rightStickX);
        double y = flatten(gamePadState.leftStickY);

        // set front motors
        frontLeft = -y + x;
        frontRight = -y - x;
        if(frontRight > 1.0){
            frontRight = 1.0;
        }
        else if(frontRight < -1.0){
            frontRight = -1.0;
        }
        if(frontLeft > 1.0){
            frontLeft = 1.0;
        }
        else if(frontLeft < -1.0){
            frontLeft = -1.0;
        }

        // repeat for back motors
        backLeft = frontLeft;
        backRight = frontRight;
    }
    
    public double flatten(double value){
        if(value > 1.0){
            value = 1.0;
        }
        else if(value < -1.0){
            value = -1.0;
        }
        return value * Math.abs(value);
    }

    public void controllerMotorTest(GamePadState gamePadState, double sec){
        frontLeft = gamePadState.leftStickY;
        frontRight = gamePadState.rightStickY;
        backRight = 0.0;
        backLeft = 0.0;

    }

    public void motorSequenceTest(GamePadState gamePadState, double sec){
        frontLeft = 0.0;
        frontRight = 0.0;
        backRight = 0.0;
        backLeft = 0.0;

        if(sec > 1.0 && sec < 2.0) {
            // store these in actuator state
            frontLeft = 1.0; // Range.clip(drive + turn, -1.0, 1.0) ;
        }
        if(sec > 2.0 && sec < 3.0) {
            frontRight = 1.0; // Range.clip(drive - turn, -1.0, 1.0) ;
        }
        if(sec > 3.0 && sec < 4.0) {
            backRight = 1.0;
        }
        if(sec > 4.0 && sec < 5.0) {
            backLeft = 1.0;
        }
    }
}
