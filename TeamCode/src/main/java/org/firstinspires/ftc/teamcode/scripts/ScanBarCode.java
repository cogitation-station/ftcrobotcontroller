package org.firstinspires.ftc.teamcode.scripts;

import org.firstinspires.ftc.teamcode.systems.ArmPosition;
import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.util.Vector2D;
import org.firstinspires.ftc.teamcode.brains.Knowledge;

public class ScanBarCode extends Script{

    private ArmPosition armPosition;
    double angleOfMinimalRange = 0;
    double minimalRange = 0;

    public ScanBarCode(ArmPosition armPosition){
        this.armPosition = armPosition;
    }

    public static ScanBarCode to(ArmPosition armPosition){
        return new ScanBarCode(armPosition);
    }

    @Override
    public ReturnMessage run(Knowledge knowledge, GamePadState gamePadState) {
        // each iteration, record the orientation where the range sensor value is the smallest
        if(knowledge.sensors.distanceA < minimalRange){
            minimalRange = knowledge.sensors.distanceA;
            angleOfMinimalRange = knowledge.robot.getOrientation();
        }

        // the run is over when the orientation angle reaches the end point
        if(knowledge.robot.getOrientation() > 5){ // TODO: FIX
            // calculate the barcode position
            Vector2D bearingToBarcode = knowledge.readTeamSideBarcode.subtract(knowledge.robot.getPosition());
            double angleToBarcode = bearingToBarcode.bearingAngle(); // must be in degrees!
            double magnitude = bearingToBarcode.magnitude(); // must be in inches!
            double angularSpacing = 8.38/magnitude*180/Math.PI; // arcLength = radius * angle (in radians)  // 8.38 in is the spacing between points of the barcode and is used as the arcLength to calculate angle here, then converted to degrees
            if(angleOfMinimalRange > angleToBarcode + angularSpacing/2){ // todo: this needs to be fixed
                // its on the left,

            }
            else if(angleOfMinimalRange < angleToBarcode - angularSpacing/2){
                // its on the right,

            }
            else{
                // its in the center,
            }

            // store the encoded arm position

            // done
            return ReturnMessage.Done;
        }

        return ReturnMessage.Working;
    }
}
