package org.firstinspires.ftc.teamcode.scripts;

import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.util.EasingFunction;
import org.firstinspires.ftc.teamcode.util.Vector2D;
import org.firstinspires.ftc.teamcode.util.Vector2D;
import org.firstinspires.ftc.teamcode.brains.Knowledge;

public class DriveTo extends Script{

    public final static double DISTANCE_THRESHOLD = 10; // inches

    private Vector2D goalPosition;

    private double maxSpeed;
    private double speed;

    public DriveTo(Vector2D goalPosition, double maxSpeed){
        this.goalPosition = goalPosition;
        this.maxSpeed = maxSpeed;
    }

    public static DriveTo position(Vector2D goalPosition, double maxSpeed){
        return new DriveTo(goalPosition, maxSpeed);
    }

    // turn toward and drive to a nearby point
    public ReturnMessage run(Knowledge knowledge, GamePadState gamePadState){
        speed = maxSpeed;

        // get the bearing to the destination (in world coordinates)
        Vector2D bearing = goalPosition.copy().subtract(knowledge.robot.getPosition());
        // if bearing magnitude is less than some threshold, return done
        double magnitude = bearing.magnitude();
        double angle = bearing.bearingAngle();
        if(magnitude < DISTANCE_THRESHOLD){ // TODO: NEED TO PERFORM EASING ON SPEED TO AVOID OVERSHOOTING
            gamePadState.leftStickY = 0;
            gamePadState.rightStickX = 0;
            return ReturnMessage.Done;
        }

        if(magnitude < 30){ // TODO: NEED TO PERFORM EASING ON SPEED TO AVOID OVERSHOOTING
            speed = .5*maxSpeed;
        }

        // back range
        if(knowledge.sensors.distanceB < 10){
            speed = maxSpeed * .35;
        }
        // front range
        if(knowledge.sensors.distanceA < 10){
            speed = maxSpeed * .35;
        }
        // front buttons
        if(knowledge.sensors.bFL || knowledge.sensors.bFR){
            speed = 0;
            return ReturnMessage.Done;
        }
        // back buttons
        if(knowledge.sensors.bBL || knowledge.sensors.bBR){
            speed = 0;
            return ReturnMessage.Done;
        }

        // rotate the bearing to the robot's coordinate system
        //Vector2D robotBearing = bearing.copy().rotate(-knowledge.robot.getHeading());
        double direction = Vector2D.range180ToNeg180(angle - knowledge.robot.getOrientation());
        // set the game pad to drive in that direction relative to the robot
        gamePadState.leftStickY = -Math.cos(Math.toRadians(direction))*speed;//*EasingFunction(speed, .5*speed, );  // forward or backward
        gamePadState.rightStickX = -Math.sin(Math.toRadians(direction))*speed; // left or right
        knowledge.telemetry.addData("Debug DriveTo BearingMag : ", magnitude);
        knowledge.telemetry.addData("Debug DriveTo BearingAngle : ", angle);
        knowledge.telemetry.addData("Debug DriveTo LSY : ", gamePadState.leftStickY);
        knowledge.telemetry.addData("Debug DriveTo RSX : ", gamePadState.rightStickX);
        return ReturnMessage.Working;
    }
}
