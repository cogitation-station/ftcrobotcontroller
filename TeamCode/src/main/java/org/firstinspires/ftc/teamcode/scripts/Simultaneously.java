package org.firstinspires.ftc.teamcode.scripts;

import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.brains.Knowledge;

import java.util.ArrayList;

public class Simultaneously extends Script{
    private ArrayList<Script> scripts = new ArrayList<>();

    public static Simultaneously go(){
        return new Simultaneously();
    }

    public Simultaneously addScript(Script script){
        scripts.add(script);
        return this;
    }

    /**
     * Simultaneously perform all the scripts on this list
     * @return
     */
    public Script.ReturnMessage run(Knowledge knowledge, GamePadState gamePadState){
        ArrayList<Script> done = new ArrayList<>();
        // run each of the scripts
        for(Script script: scripts){
            if(script.run(knowledge, gamePadState) == Script.ReturnMessage.Done){
                // add to done list
                done.add(script);
            }
        }

        // remove done scripts
        for(Script script: done){
            scripts.remove(script);
        }

        // if all the scripts are done
        if(scripts.isEmpty()){
            return Script.ReturnMessage.Done;
        }

        // otherwise, continue working
        return Script.ReturnMessage.Working;
    }
}
