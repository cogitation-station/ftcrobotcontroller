package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.teamcode.systems.ArmController;
import org.firstinspires.ftc.teamcode.systems.ArmManager;
import org.firstinspires.ftc.teamcode.systems.DrivetrainController;
import org.firstinspires.ftc.teamcode.systems.DuckSpinnerController;

public class Actuators {
    public Telemetry telemetry;

    public DcMotor frontLeft;
    public DcMotor frontRight;
    public DcMotor backRight;
    public DcMotor backLeft;

    public DcMotorEx fL;
    public DcMotorEx fR;
    public DcMotorEx bR;
    public DcMotorEx bL;

    public DcMotorEx left;
    public DcMotorEx right;

    public DcMotorEx elevator;
    public DcMotorEx shoulder;

    public CRServo duckSpinner;
    public Servo elbow;
    public Servo wrist;
    public Servo gripper;

    Gamepad.RumbleEffect customRumbleEffect;

    public void initializeTracks(HardwareMap hardwareMap, Telemetry telemetry) {
        this.telemetry = telemetry;

        frontLeft = hardwareMap.get(DcMotor.class, "frontLeft");
        frontRight = hardwareMap.get(DcMotor.class, "frontRight");
        backRight = hardwareMap.get(DcMotor.class, "backRight");
        backLeft = hardwareMap.get(DcMotor.class, "backLeft");

        frontRight.setDirection(DcMotor.Direction.REVERSE);
        backRight.setDirection(DcMotor.Direction.REVERSE);

        frontLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        // OTHER MOTORS
        duckSpinner = hardwareMap.get(CRServo.class, "duckSpinner");
        /*
        elevator = hardwareMap.get(DcMotorEx.class, "elevator");
        shoulder = hardwareMap.get(DcMotorEx.class, "shoulder");
        elbow = hardwareMap.get(Servo.class, "elbow");
        wrist = hardwareMap.get(Servo.class, "wrist");
        gripper = hardwareMap.get(Servo.class, "gripper");

         */
    }

    public void initialize(HardwareMap hardwareMap, Telemetry telemetry){

        this.telemetry = telemetry;

  //      if(robotConfig.motorEncoders == RobotConfig.MotorEncoders.No) {
            frontLeft = hardwareMap.get(DcMotorEx.class, "frontLeft");
            frontRight = hardwareMap.get(DcMotorEx.class, "frontRight");
            backRight = hardwareMap.get(DcMotorEx.class, "backRight");
            backLeft = hardwareMap.get(DcMotorEx.class, "backLeft");
  //      }
  /*      else if(robotConfig.motorEncoders == RobotConfig.MotorEncoders.Yes) {
            fL = hardwareMap.get(DcMotorEx.class, "frontLeft");
            fR = hardwareMap.get(DcMotorEx.class, "frontRight");
            bR = hardwareMap.get(DcMotorEx.class, "backRight");
            bL = hardwareMap.get(DcMotorEx.class, "backLeft");

            if(robotConfig.runMode == DcMotor.RunMode.RUN_USING_ENCODER) {
                fL.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                fR.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                bR.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                bL.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            }
            else if(robotConfig.runMode == DcMotor.RunMode.RUN_TO_POSITION) {
                fL.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                fR.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                bR.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                bL.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
            }
        }
*/
        duckSpinner = hardwareMap.get(CRServo.class, "duckSpinner");
       // elevator = hardwareMap.get(CRServo.class, "elevator");

        //shoulder = hardwareMap.get(Servo.class, "shoulder");
        //elbow = hardwareMap.get(Servo.class, "elbow");
        //wrist = hardwareMap.get(Servo.class, "wrist");
        //gripper = hardwareMap.get(Servo.class, "gripper");

        customRumbleEffect = new Gamepad.RumbleEffect.Builder()
                .addStep(0.0, 1.0, 500)  //  Rumble right motor 100% for 500 mSec
                .addStep(0.0, 0.0, 300)  //  Pause for 300 mSec
                .addStep(1.0, 0.0, 250)  //  Rumble left motor 100% for 250 mSec
                .addStep(0.0, 0.0, 250)  //  Pause for 250 mSec
                .addStep(1.0, 0.0, 250)  //  Rumble left motor 100% for 250 mSec
                .build();
    }

    /**
     * This is the full initialize
     * @param hardwareMap
     * @param telemetry
     */
    public void initializeFull(HardwareMap hardwareMap, Telemetry telemetry) {
        this.telemetry = telemetry;

        //DC MOTORS
        left = hardwareMap.get(DcMotorEx.class, "left");
        right = hardwareMap.get(DcMotorEx.class, "right");
        shoulder = hardwareMap.get(DcMotorEx.class, "shoulder");
        elevator = hardwareMap.get(DcMotorEx.class, "elevator");

        right.setDirection(DcMotor.Direction.REVERSE);

        left.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        right.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        shoulder.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        elevator.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        left.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        right.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        shoulder.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        elevator.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        // SERVO MOTORS
        duckSpinner = hardwareMap.get(CRServo.class, "duckSpinner");
        gripper = hardwareMap.get(Servo.class, "gripper");
        wrist = hardwareMap.get(Servo.class, "wrist");
        elbow = hardwareMap.get(Servo.class, "elbow");
    }

    public int getLeftMotorPosition(){
        return left.getCurrentPosition();
    }

    public int getRightMotorPosition(){
        return right.getCurrentPosition();
    }

    public int getElevatorPosition(){
        return elevator.getCurrentPosition();
    }

    public int getShoulderPosition(){
        return shoulder.getCurrentPosition();
    }

    public void updateMotors(DrivetrainController drivetrainController, ArmManager armManager){
        left.setTargetPosition(drivetrainController.leftTicks);
        right.setTargetPosition(drivetrainController.rightTicks);
        left.setPower(drivetrainController.left);
        right.setPower(drivetrainController.right);
        left.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        right.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        shoulder.setTargetPosition(armManager.shoulderTicks);
        elevator.setTargetPosition(armManager.elevatorTicks);
        shoulder.setPower(1);
        elevator.setPower(1);
        shoulder.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        elevator.setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }

    public void updateMotors(MotorController motorController){
  //      if(robotConfig.motorEncoders == RobotConfig.MotorEncoders.No) {
            frontLeft.setPower(motorController.frontLeft);
            frontRight.setPower(motorController.frontRight);
            backRight.setPower(motorController.backRight);
            backLeft.setPower(motorController.backLeft);
 //       }
 /*       else if(robotConfig.motorEncoders == RobotConfig.MotorEncoders.Yes) {
            if(RobotConfig.runMode == DcMotor.RunMode.RUN_USING_ENCODER) {
                fL.setVelocity(powerToTicksPerSecond(motorController.frontLeft)); // ticks per second
                fR.setVelocity(powerToTicksPerSecond(motorController.frontRight));
                bR.setVelocity(powerToTicksPerSecond(motorController.backRight));
                bL.setVelocity(powerToTicksPerSecond(motorController.backLeft));
            }
            else if(robotConfig.runMode == DcMotor.RunMode.RUN_TO_POSITION){
                // can't do this during initialization???
                fL.setTargetPosition(20); // ticks

                fL.setMode(DcMotor.RunMode.RUN_TO_POSITION);

                fL.setVelocity(200); // ticks per sec

                fL.getCurrentPosition();
                fL.getVelocity();
            }
        }*/
    }

    public double powerToTicksPerSecond(double power){
        return 100*power; //????
    }

    public void updateServos(ArmController armController){
        if(armController.goal != null) {
            elevator.resetDeviceConfigurationForOpMode();
            elevator.setPower(armController.elevatorPower); //?
            elevator.setDirection(armController.elevatorDirection); //?
   //         shoulder.setPosition(degToPosition(armController.goal.shoulderAngle));
            elbow.setPosition(degToPosition(armController.goal.elbowAngle));
            wrist.setPosition(degToPosition(armController.goal.wristAngle));
            gripper.setPosition(degToPosition(armController.goal.gripperAngle));
        }
    }

    public void updateServos(DuckSpinnerController duckSpinnerController){
        duckSpinner.setPower(duckSpinnerController.duckPower);
        duckSpinner.setDirection(duckSpinnerController.direction);
    }

    public void updateServos(DuckSpinnerController duckSpinnerController, ArmManager armManager){
        double elbowPos = degToPosition(armManager.elbowDeg);
        double wristPos = degToPosition(armManager.wristDeg);
        double pistonPos = 0;

        if (armManager.pistonGo) {
            pistonPos = .9;
        }

        elbow.setPosition(elbowPos);
        wrist.setPosition(wristPos);
        gripper.setPosition(pistonPos);

        duckSpinner.setPower(duckSpinnerController.duckPower);
        duckSpinner.setDirection(duckSpinnerController.direction);
    }

    public void updateRumble(Gamepad gamepad){
        gamepad.runRumbleEffect(customRumbleEffect);
        gamepad.rumble(0.9, 0, 200);  // 200 mSec burst on left motor.
        gamepad.rumble(gamepad.left_trigger, gamepad.right_trigger, Gamepad.RUMBLE_DURATION_CONTINUOUS);
        gamepad.rumbleBlips(3);
        gamepad.stopRumble();
    }

    public static double degToPosition(double deg){
        if(deg > 150){
            deg = 150;
        }
        if(deg < -150){
            deg = -150;
        }
        return (deg + 150)/300;
    }

    public static double positionToDeg(double pos){
        return pos * 300 - 150;
    }
}
