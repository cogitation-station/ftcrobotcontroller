package org.firstinspires.ftc.teamcode.brains;

import org.firstinspires.ftc.teamcode.Sensors;
import org.firstinspires.ftc.teamcode.brains.Knowledge;
import org.firstinspires.ftc.teamcode.util.Vector2D;

public class TracksPositionTracker {

    public final static double TRACK_RADIUS = mmToIn(115/2);
    public final static double TRACK_SEPARATION = mmToIn(329);
    public final static double DRIVETRAIN_PULSES_PER_REV = 537.7;

    public static Vector2D estimatePositionChange(Knowledge knowledge){

        try {
            // UPDATE THE POSITION USING THE MOTOR ENCODERS, AIDED BY THE ORIENTATION CHANGE
            double left = ticksToDistance(knowledge.sensors.deltaLeftMotorPosition, TRACK_RADIUS); // left tracks movement distance since last update
            double right = ticksToDistance(knowledge.sensors.deltaRightMotorPosition, TRACK_RADIUS); // right tracks movement distance since last update
            double dThetaPredictedRad = deltaThetaPredictedRad(left, right, TRACK_SEPARATION); // the change in the facing angle as calculated from track lengths
            // the directly measured value is more accurate because the tracks can slip, thus we can calculate the degree of slipping for each track by looking at the difference between dTheta and dTheta2, then reduce the larger track path by the amount of slipping
            double dThetaMeasuredRad = Math.toRadians(knowledge.robot.getDeltaOrientation()); // convert to radians
            double slipping = trackSlipping(dThetaMeasuredRad, dThetaPredictedRad, TRACK_SEPARATION);
            if(slipping > 0){ // it turned more left than the tracks would indicate, meaning it slipped on the left
                left -= slipping;
            }
            else{ // it turned more to the right than the tracks would indicate, meaning it slipped on the right
                right += slipping;
            }
            double forward = forwardPositionChange(left, right, TRACK_SEPARATION, dThetaMeasuredRad); // amount the robot moves forward
            double side = sidePositionChange(left, right, TRACK_SEPARATION, dThetaMeasuredRad); // amount the robot moves to the side (left)
            double oldBearingRad = Math.toRadians(knowledge.robot.getOldOrientation()); // the old bearing of the robot before this move
            double dx = deltaX(forward, side, oldBearingRad); // x-position change of the robot in world coordinates
            double dy = deltaY(forward, side, oldBearingRad); // y-position change of the robot in world coordinates

            knowledge.telemetry.addData("TrackPosition: ", knowledge.robot.getPosition().toString());
            knowledge.telemetry.addData("TrackFacing: ", "(%.2f)", knowledge.robot.getOrientation());
            knowledge.telemetry.addData("Tracks: ", "left: (%.2f), right: (%.2f)", left, right);
            knowledge.telemetry.addData("TrackSlipping: ", "(%.2f)", slipping);

            return new Vector2D(dx, dy);

        }
        catch (Exception e){

        }

        return new Vector2D(0, 0);
    }

    public static double ticksToDistance(double ticks, double radius){
        // Gear Ratio Ratio = 19.2:1
        // Encoder Shaft: 28 pulses per revolution
        // Gearbox output: 537.7 pulses per revolution
        return 2 * Math.PI * radius * ticks / DRIVETRAIN_PULSES_PER_REV;
    }

    public static double mmToIn(double mm){
        return mm / 25.4;
    }

    public static double forwardPositionChange(double left, double right, double separation, double dThetaRad){
        Double factor = Math.sin(dThetaRad)/dThetaRad;
        if(factor.isInfinite() || factor.isNaN()){
            factor = 1.0;
        }
        return (left + right)/2 * factor;
    }

    public static double sidePositionChange(double left, double right, double separation, double dThetaRad){
        Double factor = (1-Math.cos(dThetaRad))/dThetaRad;
        if(factor.isInfinite() || factor.isNaN()){
            factor = 0.0;
        }
        if(left > right){
            return -(left + right)/2 * factor;
        }
        return (left + right)/2 * factor;
    }

    // This calculates the predicted heading change of the robot based on the assumption that the center point of the tracks do not slip and each has a specific amount of distance motion (left and right)
    public static double deltaThetaPredictedRad(double left, double right, double separation){
        return (right - left)/separation; // calculated bearing change of the robot
    }

    // This calculates the amount of rotational track slipping that occurred based on the difference in the actual and predicted heading change of the robot
    public static double trackSlipping(double deltaThetaRadActual, double deltaThetaRadPredicted, double separation){
        return separation * (deltaThetaRadActual - deltaThetaRadPredicted);
    }

    public static double deltaX(double forward, double side, double bearingRad){
        return Math.cos(bearingRad) * forward + Math.cos(bearingRad + Math.PI/2) * side;
    }

    public static double deltaY(double forward, double side, double bearingRad){
        return -Math.sin(bearingRad) * forward - Math.sin(bearingRad + Math.PI/2) * side;
    }

}
