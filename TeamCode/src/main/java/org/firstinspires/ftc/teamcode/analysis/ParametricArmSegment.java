package org.firstinspires.ftc.teamcode.analysis;

public class ParametricArmSegment {
    double length = 0; // length in mm
    double weight = 0; // weight in lb
    double r = 0; // the distance to the center of mass of the segment
    double jointRadius = 0; // joint radius in mm
    double minAngle = 0; // minimum angle of range of motion of the joint
    double maxAngle = 0; // maximum angle of range of motion of the joint
    double maxTorque = 0; // maximum torque of motor moving this segment
    double angle = 0; // the current angle of the joint

    public boolean isAngleWithinRangeOfMotion(){
        return minAngle <= angle && angle <= maxAngle;
    }
}
