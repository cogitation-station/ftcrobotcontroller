package org.firstinspires.ftc.teamcode.systems;


import com.qualcomm.robotcore.hardware.DcMotorSimple;

import org.firstinspires.ftc.teamcode.Actuators;
import org.firstinspires.ftc.teamcode.GamePadState;

import java.util.ArrayList;

/**
 * The ArmController changes the state of the arm
 *
 * ARM MOTIONS:
 * 1) Folded to Passing Under
 * 2) Passing Under to Front Ready
 * 3) Front Ready to Grabbing
 * 4) Grabbing to Front Ready
 * 5) Front Ready to Passing Under
 * 6) Passing Under to Back Ready
 * 7) Back Ready to Passing Under
 * 8) Passing Under to Folded
 * 9) Back Ready to Capping Level
 * 10) Back Ready to Top Level
 * 11) Back Ready to Mid Level
 * 12) Back Ready to Low Level
 * 13) Capping Level to Back Ready
 * 14) Top Level to Back Ready
 * 15) Mid Level to Back Ready
 * 16) Low Level to Back Ready
 * 17) Any Position to Any + Pusher Extended
 * 18) Any Position to Any + Pusher Retracted
 *
 */
public class ArmController {

    public double elevatorPower;
    public DcMotorSimple.Direction elevatorDirection;
    boolean done = false;



    public ArmPosition armPosition;
    public ArmPosition goal;

    // this is a sequence of motion sets
    public ArrayList<ArmPosition> motionSequence = new ArrayList();

    public ArmController(){
        armPosition = new ArmPosition(ArmPosition.foldedCompact); // this needs to be folded compact
    }

    /**
     * ARM CONTROL HOT BUTTONS
     * 1) Front Ready: X
     * 2) Grabbing: A
     * 3) Capping Level: D-left
     * 4) Top Level: D-up
     * 5) Mid Level: D-right
     * 6) Low Level: D-down
     * 7) Pusher: B
     * 8) Folded: D-left AND
     * 9) Back Ready: Y
     * @param gamePadState
     */
    public void controllerUpdate(GamePadState gamePadState, Actuators actuators){
        // 0. read arm position
        armPosition.getActuatorValues(actuators);

        // 1) if the controller redirects the arm, change the motion sequence
        // GRAB
        if(gamePadState.a){
            // if arm in front,
            if(armPosition.shoulderAngle >= ArmPosition.FORWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.frontReady);
                motionSequence.add(ArmPosition.grabFront);
                motionSequence.add(ArmPosition.frontReady);
            }
            // else arm in back,
            else if(armPosition.shoulderAngle <= ArmPosition.BACKWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.frontReady);
                motionSequence.add(ArmPosition.grabFront);
                motionSequence.add(ArmPosition.frontReady);
            }
           // else, folded under
            else {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.frontReady);
                motionSequence.add(ArmPosition.grabFront);
                motionSequence.add(ArmPosition.frontReady);
            }

        }
        // DROP
        else if(gamePadState.b){
            // Make a drop from any position
            ArmPosition armNow = new ArmPosition(armPosition);
            armNow.gripperAngle = -90;
            ArmPosition dropBlock = new ArmPosition(armPosition);
            dropBlock.gripperAngle = 90;
            motionSequence.clear();
            motionSequence.add(dropBlock);
            motionSequence.add(armNow);
        }
        // FRONT READY
        else if(gamePadState.x){
            // if arm in front,
            if(armPosition.shoulderAngle >= ArmPosition.FORWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.frontReady);
            }
            // else arm in back,
            else if(armPosition.shoulderAngle <= ArmPosition.BACKWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.frontReady);
            }
            // else, folded under
            else {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.frontReady);
            }
        }
        // BACK READY
        else if(gamePadState.y){
            // if arm in front,
            if(armPosition.shoulderAngle >= ArmPosition.FORWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.foldedBack);
            }
            // else arm in back,
            else if(armPosition.shoulderAngle <= ArmPosition.BACKWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
            }
            // else, folded under
            else {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
            }
        }
        // CAP
        else if(gamePadState.dPadLeft){
            // if arm in front,
            if(armPosition.shoulderAngle >= ArmPosition.FORWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.capLevel);
            }
            // else arm in back,
            else if(armPosition.shoulderAngle <= ArmPosition.BACKWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.capLevel);
            }
            // else, folded under
            else {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.capLevel);
            }
        }
        // TOP LEVEL
        else if(gamePadState.dPadUp){
            // if arm in front,
            if(armPosition.shoulderAngle >= ArmPosition.FORWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.topLevel);
            }
            // else arm in back,
            else if(armPosition.shoulderAngle <= ArmPosition.BACKWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.topLevel);
            }
            // else, folded under
            else {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.topLevel);
            }
        }
        // MID LEVEL
        else if(gamePadState.dPadRight){
            // if arm in front,
            if(armPosition.shoulderAngle >= ArmPosition.FORWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.midLevel);
            }
            // else arm in back,
            else if(armPosition.shoulderAngle <= ArmPosition.BACKWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.midLevel);
            }
            // else, folded under
            else {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.midLevel);
            }
        }
        // LOW LEVEL
        else if(gamePadState.dPadDown){
            // if arm in front,
            if(armPosition.shoulderAngle >= ArmPosition.FORWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.lowLevel);
            }
            // else arm in back,
            else if(armPosition.shoulderAngle <= ArmPosition.BACKWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.lowLevel);
            }
            // else, folded under
            else {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.lowLevel);
            }
        }

        // 2) while an unmet goal ArmPosition remains in the motion sequence, get the next goal
        done = motionSequence.isEmpty();
        while(!done) {
            goal = motionSequence.get(0); // might be null or throw exception
            if(armPosition.matches(goal)){
                motionSequence.remove(goal);
                done = motionSequence.isEmpty();
            }
            else{
                done = true;
            }
        }
    }
}
