package org.firstinspires.ftc.teamcode.scripts;

import org.firstinspires.ftc.teamcode.systems.ArmPosition;
import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.brains.Knowledge;

/**
 * PlacePreloadedBlock SEQUENTIALLY
 *      MoveArmTo(frontReady)
 *      DriveTo(readRedBarcodeA) // use points for your color and side
 *      TurnTo (scanLeftFacing)
 *      SIMULTANEOUSLY: TurnTo (scanRightFacing), ScanBarCode
 *      SIMULTANEOUSLY: NavigateTo(teamShippingHub), MoveArmTo(barCodedHeight)
 *      ReleaseBlock
 */
public class PlacePreloadedBlock extends Script{

    Sequentially sequentially = new Sequentially();

    public PlacePreloadedBlock(Knowledge knowledge, double maxSpeed){
        sequentially
                .addScript(MoveArmTo.position(ArmPosition.frontReady))
                .addScript(DriveTo.position(knowledge.readTeamSideBarcode, maxSpeed))
                .addScript(TurnTo.angle(knowledge.scanStartFacing, maxSpeed))
                .addScript(Simultaneously.go().addScript(TurnTo.angle(knowledge.scanEndFacing, maxSpeed)).addScript(ScanBarCode.to(knowledge.barcodeArmPosition)))
                .addScript(Simultaneously.go().addScript(NavigateTo.destination(knowledge.teamShippingHub, maxSpeed)).addScript(MoveArmTo.position(knowledge.barcodeArmPosition))) // here for example, the barCodedHeight is read on the previous step but is not known here, so it can't be stored but needs to be referenced somehow
                .addScript(new ReleaseBlock());
    }

    @Override
    public ReturnMessage run(Knowledge knowledge, GamePadState gamePadState) {
        return sequentially.run(knowledge, gamePadState);
    }
}
