package org.firstinspires.ftc.teamcode.systems;


import com.qualcomm.robotcore.hardware.DcMotor;

import org.firstinspires.ftc.teamcode.Actuators;
import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.Sensors;

import java.util.ArrayList;

public class ArmManager {
    //locations
    public int elevatorTop = 15300;
    public int elevatorMid = 7650;
    public int elevatorLow = -1;

    public ArrayList<Double> elevatorMoves = new ArrayList<Double>(0);
    public ArrayList<Double> shoulderMoves = new ArrayList<Double>(0);
    public ArrayList<Double> elbowMoves = new ArrayList<Double>(0);
    public ArrayList<Double> wristMoves = new ArrayList<Double>(0);

    //elevator (ticks), shoulder (deg), elbow (deg), wrist (deg)
    public double[][] testMoves = {{elevatorTop, elevatorTop, elevatorTop, elevatorTop},{0, 90, 90, 90},{0, 0, 45, 45},{0, 0, 0, -45}}; // 4 steps

    public int step = 0;

    //Dc Motor Ticks
    public int shoulderTicks = 0; // 15 ticks = 1 deg

    public int elevatorTicks = 0;

    //SERVO deg
    public double wristDeg = 0;
    public double elbowDeg = 0;

    //PISTON
    public boolean pistonGo = false;

    public ArmPos currentPos = ArmPos.STOWED;
    public ArmPos targetPos = ArmPos.STOWED;

    public int elevatorResets = 0;

    public boolean elevatorIsActive = false;
    public boolean shoulderIsActive = false;
    public boolean elbowIsActive = false;
    public boolean wristIsActive = false;

    // elevator
    public boolean dpu;
    public boolean dpd;
    // piston
    public boolean a;
    // wrist
    public boolean rb;
    public boolean lb;
    //elbow
    public boolean x;
    public boolean b;
    //shoulder
    public boolean dpl;
    public boolean dpr;
    // alt mode
    public boolean altMode;
    //reset arm ticks
    public boolean y;
    //elevator reset
    public boolean elevatorStop;

    public void armManagerInt(Actuators actuators) {
        wristDeg = actuators.wrist.getPosition()*300-150; // converting from [0, 1] to [-150, 150] degrees
        elbowDeg = actuators.elbow.getPosition()*300-150;
    }

    public void armManualControl(GamePadState gamePadState, Actuators actuators, Sensors sensors) {
        // elevator
        dpu = gamePadState.dPadUp;
        dpd = gamePadState.dPadDown;
        // piston
        a = gamePadState.a;
        // wrist
        rb = gamePadState.rightBumper;
        lb = gamePadState.leftBumper;
        //elbow
        x = gamePadState.x;
        b = gamePadState.b;
        //shoulder
        dpl = gamePadState.dPadLeft;
        dpr = gamePadState.dPadRight;
        // alt mode
        altMode = gamePadState.altMode;
        //reset arm ticks
        y = gamePadState.y;
        //elevator reset
        elevatorStop = !sensors.elevatorLimiter;

        //alt mode controls
        if (altMode) {
            // elevator
            if (dpu && elevatorTicks < elevatorTop) {
                elevatorTicks+=50;
            }

            else if (dpd && !elevatorStop) {
                elevatorTicks-=50;
            }

            else if (elevatorStop) {
                actuators.elevator.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                elevatorResets++;
                elevatorTicks = 0;
            }

            //shoulder //  && shoulderTicks < 2310 //  && shoulderTicks > -2310
            if (dpr) {
                shoulderTicks+=28;
            }

            else if (dpl) {
                shoulderTicks-=28;
            }

            //elbow
            if (x && elbowDeg < 90) {
                elbowDeg+=2;
            }

            else if (b && elbowDeg > -90) {
                elbowDeg-=2;
            }

            //wrist - now limited to [-90, 90]
            if (lb && wristDeg < 90) {
                wristDeg+=2;
            }

            else if (rb && wristDeg > -90) {
                wristDeg-=2;
            }

            if (y) {
                wristDeg = 0;
                elbowDeg = 0;
            }
        }

        // normal controls
        else {
            if (y) {
                actuators.shoulder.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                shoulderTicks = 0;
            }

            if (b) {
                targetPos = ArmPos.TEST;
                moveTo(ArmPos.TEST);
            }

            checkActions(actuators);
        }

        //piston
        if (a) {
            pistonGo = true;
        }

        else {
            pistonGo = false;
        }
    }

    public double flatten(double value){
        if(value > 1.0){
            value = 1.0;
        }
        else if(value < -1.0){
            value = -1.0;
        }
        return value * Math.abs(value);
    }

    enum ArmPos{
        GRAB_READY,
        GRABBING,
        FORWARD_STATE,
        BACKWARD_STATE,
        RELEASE_TOP,
        RELEASE_MID,
        RELEASE_LOW,
        STOWED,
        NEUTRAL,
        TEST
    }

    public void moveTo(ArmPos armPos) {
        if (armPos == ArmPos.TEST) {
            for (int i = 0; i < 4; i++) {
                elevatorMoves.add(testMoves[0][i]);
                shoulderMoves.add(testMoves[1][i]);
                elbowMoves.add(testMoves[2][i]);
                wristMoves.add(testMoves[3][i]);
            }
        }
        step = 0;
    }

    public void nextAction() {
        if (targetPos != currentPos && !shoulderIsActive && !elevatorIsActive && !elbowIsActive && !wristIsActive) {
            if (shoulderMoves.size()<step+1 && elevatorMoves.size()<step+1 && elbowMoves.size()<step+1 && wristMoves.size()<step+1) {
                currentPos = targetPos;
                clearMoves();
                step = 0;
            }

            else {
                setActive();
                step++;
            }
        }
    }

    public void checkActions(Actuators actuators) {
        if (targetPos != currentPos) {
            if (shoulderIsActive && actuators.shoulder.getCurrentPosition() < shoulderTicks+5 && shoulderTicks-5 < actuators.shoulder.getCurrentPosition()) {
                shoulderIsActive = false;
            }
            if (elevatorIsActive && actuators.elevator.getCurrentPosition() < elevatorTicks+5 && elevatorTicks-5 < actuators.elevator.getCurrentPosition()) {
                elevatorIsActive = false;
            }
            if (elbowIsActive && actuators.elbow.getPosition() < elbowDeg+2.5&& elbowDeg-2.5 < actuators.elbow.getPosition()) {
                elbowIsActive = false;
            }
            // if the wrist.getPosition() is within +/-2.5 of the wristDeg, active is false
            if (wristIsActive && actuators.wrist.getPosition() < wristDeg+2.5&& wristDeg-2.5 < actuators.wrist.getPosition()) {
                wristIsActive = false;
            }

            nextAction();
        }
    }

    private int shoulderDegToTicks(double deg) {
        return (int)Math.round(deg*(5281.1/360));
    }

    private void clearMoves() {
        shoulderMoves.clear();
        elevatorMoves.clear();
        elbowMoves.clear();
        wristMoves.clear();

        shoulderMoves.trimToSize();
        elevatorMoves.trimToSize();
        elbowMoves.trimToSize();
        wristMoves.trimToSize();
    }

    private void setActive() {
        elevatorIsActive = true;
        shoulderIsActive = true;
        elbowIsActive = true;
        wristIsActive = true;

        shoulderTicks = shoulderDegToTicks(shoulderMoves.get(step));
        elevatorTicks = elevatorMoves.get(step).intValue();
        elbowDeg = elbowMoves.get(step);
        wristDeg = wristMoves.get(step);
    }
}