package org.firstinspires.ftc.teamcode.brains;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.teamcode.systems.ArmPosition;
import org.firstinspires.ftc.teamcode.util.Vector2D;
import org.firstinspires.ftc.teamcode.Sensors;

// values recorded here represents states that the robot believes to be true
public class Knowledge {

    public Telemetry telemetry;

    // KNOWLEDGE OF THE ROBOT:
    public MovableBoundingRectangle robot = new MovableBoundingRectangle(0, 0, ROBOT_WIDTH_X, ROBOT_WIDTH_Y, 90);
    public final static double ROBOT_WIDTH_X = 18;
    public final static double ROBOT_WIDTH_Y = 18;

    // ROBOT SENSOR STATES
    public Sensors sensors;

    // KNOWLEDGE OF THE PLAYING FIELD:
    // location area of the warehouse
    public SimpleBoundingRectangle blueWarehouse = new SimpleBoundingRectangle(-(72-43.5/2), 144-43.5/2, 43.5, 43.5); // unit is inches
    public SimpleBoundingRectangle redWarehouse = new SimpleBoundingRectangle(72-43.5/2, 144-43.5/2, 43.5, 43.5);

    // location area of the freight
    public SimpleBoundingRectangle blueFreight = new SimpleBoundingRectangle(-(72-11.5), 144-11.5, 23.0, 23.0);
    public SimpleBoundingRectangle redFreight = new SimpleBoundingRectangle(72-11.5, 144-11.5, 23.0, 23.0);

    // location area of the blue/red alliance shipping hub
    public SimpleBoundingRectangle blueShippingHub = new SimpleBoundingRectangle(-24, 60, 18, 18);
    public SimpleBoundingRectangle redShippingHub = new SimpleBoundingRectangle(24, 60, 18, 18);

    // location area of the blue/red alliance storage unit
    public SimpleBoundingRectangle blueStorage = new SimpleBoundingRectangle(-36, 12, 23, 23);
    public SimpleBoundingRectangle redStorage = new SimpleBoundingRectangle(36, 12, 23, 23);

    // location area of the shared shipping hub
    public SimpleBoundingRectangle sharedShippingHub = new SimpleBoundingRectangle(0, 120, 18, 18);

    // location area of red/blue starting position A
    public SimpleBoundingRectangle blueStartingPositionA = new SimpleBoundingRectangle(-72+8, 36, 18, 18);
    public SimpleBoundingRectangle redStartingPositionA = new SimpleBoundingRectangle(72-8, 36, 18, 18);

    // location area of red/blue starting position B
    public SimpleBoundingRectangle blueStartingPositionB = new SimpleBoundingRectangle(-72+8, 84, 18, 18);
    public SimpleBoundingRectangle redStartingPositionB = new SimpleBoundingRectangle(72-8, 84, 18, 18);

    // location area of the red/blue carousel
    public SimpleBoundingRectangle blueCarousel = new SimpleBoundingRectangle(-(72-3), 3, 7.5, 7.5);  // 15 in diameter
    public SimpleBoundingRectangle redCarousel = new SimpleBoundingRectangle(72-3, 3, 7.5, 7.5); // TODO: CHECK THE Y VALUE ON BOTH, THUS THE X TOO

    // barrier locations
    // TODO: There are 3 barriers to each side
    private double barrierDx = 2.883*2;
    private double barrierDy = 29.5; // TODO: check these two numbers
    public SimpleBoundingRectangle redBarrier1 = new SimpleBoundingRectangle(barrierDy/2, 96, barrierDx, barrierDy);
    public SimpleBoundingRectangle redBarrier2 = new SimpleBoundingRectangle(3*barrierDy/2, 96, barrierDx, barrierDy);
    public SimpleBoundingRectangle redBarrier3 = new SimpleBoundingRectangle(24, 96+barrierDy/2+barrierDx/2, barrierDy, barrierDx);
    public SimpleBoundingRectangle blueBarrier1 = new SimpleBoundingRectangle(-barrierDy/2, 96, barrierDx, barrierDy);
    public SimpleBoundingRectangle blueBarrier2 = new SimpleBoundingRectangle(-3*barrierDy/2, 96, barrierDx, barrierDy);
    public SimpleBoundingRectangle blueBarrier3 = new SimpleBoundingRectangle(24, 96+barrierDy/2+barrierDx/2, barrierDy, barrierDx);

    // wall locations
    public SimpleBoundingRectangle nWall = new SimpleBoundingRectangle(0, 144, 144, .25);
    public SimpleBoundingRectangle eWall = new SimpleBoundingRectangle(72, 72, .25, 144);
    public SimpleBoundingRectangle sWall = new SimpleBoundingRectangle(0, 0, 144, .25);
    public SimpleBoundingRectangle wWall = new SimpleBoundingRectangle(-72, 72, .25, 144);

    // bar code locations
    public SimpleBoundingRectangle redBarcodeA = new SimpleBoundingRectangle(36, 36, 2, 2);
    public SimpleBoundingRectangle blueBarcodeA = new SimpleBoundingRectangle(-36, 36, 2, 2);
    public SimpleBoundingRectangle redBarcodeB = new SimpleBoundingRectangle(36, 84, 2, 2);
    public SimpleBoundingRectangle blueBarcodeB = new SimpleBoundingRectangle(-36, 84, 2, 2);

    // navigation points
    public Vector2D readRedBarcodeA = new Vector2D(36+12, 36);
    public Vector2D readBlueBarcodeA = new Vector2D(-36-12, 36);
    public Vector2D readRedBarcodeB = new Vector2D(36+12, 84);
    public Vector2D readBlueBarcodeB = new Vector2D(-36-12, 84);

    // KNOWLEDGE OF THE GAME STATE:
    public ArmPosition barcodeArmPosition = ArmPosition.topLevel; // set to the correct ArmPosition while scanning barcode
    public Vector2D readTeamSideBarcode = readRedBarcodeA; // set to the correct Vector2D when reading team and side
    public SimpleBoundingRectangle teamShippingHub = redShippingHub; // set to the correct SimpleBoundingRectangle when reading team
    public SimpleBoundingRectangle teamStartPosition = redStartingPositionA; // set to the correct site when reading team and side
    public double teamStartOrientation = 180; // the original facing of the robot, also used to correct orientation updates
    public SimpleBoundingRectangle teamParkingPosition = redFreight;
    public SimpleBoundingRectangle teamCarousel = redCarousel; // set to correct site when reading team
    public double scanStartFacing = 270;
    public double scanEndFacing = 90;

    // current team color (red or blue)
    public enum TeamColor{
        Red, Blue;
    }
    public TeamColor teamColor = TeamColor.Red;

    // current start position (A or B)
    public enum PlayerAB{
        PlayerA, PlayerB;
    }
    public PlayerAB playerAB = PlayerAB.PlayerA;

    // playing remote or live event
    public enum RemoteVsLive{
        Live, Remote;
    }
    public RemoteVsLive remoteVsLive = RemoteVsLive.Remote;

    // position indicated by barcode
    public enum Barcode{
        Top, Mid, Low, Unread;
    }
    public Barcode barcode = Barcode.Unread;


    // KNOWLEDGE OF OTHER ROBOTS:
    // location, orientation, velocity, angular velocity, of other robot (useful to avoid collisions)


    /**
     * Update merges important sensors data into the knowledge structure, putting it into a useful form
     * First we put position from 9-dof and merge it with motor encoder data,
     * then correct it possibly with camera, range finder, wall touches, and barrier impacts.
     * @param sensors
     */
    public void update(Sensors sensors){

        try {
            // UPDATE THE ORIENTATION FROM THE GYRO
            robot.updateOrientation(sensors.orientation.z + teamStartOrientation, sensors.getDt());

            // UPDATE THE POSITION USING THE MOTOR ENCODERS, AIDED BY THE ORIENTATION CHANGE
            Vector2D deltaPos = TracksPositionTracker.estimatePositionChange(this);
            robot.updatePosition(deltaPos, sensors.getDt()); // update the robot's position

        }
        catch (Exception e){

        }
    }

    public void initialize(Sensors sensors, Telemetry telemetry){
        this.sensors = sensors;
        this.telemetry = telemetry;
        // TODO: read the file
        robot.initializePosition(teamStartPosition.getPosition());
        robot.initializeOrientation(teamStartOrientation);
    }


}
