package org.firstinspires.ftc.teamcode.scripts;

import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.brains.Knowledge;
import org.firstinspires.ftc.teamcode.brains.SimpleBoundingRectangle;

/**
 *  SpinForADuck
 *        Navigate to (DuckSpinner)
 *        Press Spinner to Platform
 *        Spin for (T)
 */
public class ScoreADuck extends Script{

    Sequentially sequentially = new Sequentially();
    public final static double SPIN_TIME = 3; // sec


    public ScoreADuck(Knowledge knowledge, double maxSpeed){
        SimpleBoundingRectangle destination = knowledge.teamCarousel;

        sequentially
        .addScript(NavigateTo.destination(destination, maxSpeed))
        .addScript(TurnToward.position(destination.getPosition(), maxSpeed))
        .addScript(DockWith.position(destination.getPosition(), 0.1))
        .addScript(SpinFor.time(SPIN_TIME));
    }

    @Override
    public ReturnMessage run(Knowledge knowledge, GamePadState gamePadState) {
        return sequentially.run(knowledge, gamePadState);
    }
}
