package org.firstinspires.ftc.teamcode.analysis;

import org.firstinspires.ftc.teamcode.util.Vector2D;

public class ParametricLine {

    public double m; // slope
    public double b; // y-intercept
    public Vector2D p1 = new Vector2D(); // end-point of the line segment
    public Vector2D p2 = new Vector2D(); // other end-point of the line segment

    public ParametricLine(double x1, double y1, double x2, double y2){
        m = (y2-y1)/(x2-x1);
        b = y2 - m * x2;
        p1.set(x1, y1);
        p2.set(x2, y2);
    }

    public ParametricLine(Vector2D p1, Vector2D p2){
        m = (p2.getY()-p1.getY())/(p2.getX()-p1.getX());
        b = p2.getY() - m * p2.getX();
        p1.set(p1);
        p2.set(p2);
    }

    public double y(double x){
        return m * x + b;
    }

    // the intersection point of two lines, does not work if they have the same slope
    private static Vector2D intersectionOfTwoLines(ParametricLine l1, ParametricLine l2){
        return new Vector2D((l1.b-l2.b)/(l2.m-l1.m), l1.m*(l1.b-l2.b)/(l2.m-l1.m) + l1.b);
    }

    public static boolean isLineCollision(ParametricLine l1, ParametricLine l2){
        if(l1.m == l2.m){
            return false;
        }
        Vector2D intersection = intersectionOfTwoLines(l1, l2);
        double d1 = l1.p1.distance(intersection);
        double d2 = l1.p2.distance(intersection);
        double d = l1.p1.distance(l1.p2);
        if(d1 + d2 != d){
            return false;
        }
        d1 = l2.p1.distance(intersection);
        d2 = l2.p2.distance(intersection);
        d = l2.p1.distance(l2.p2);
        if(d1 + d2 != d){
            return false;
        }

        return true;
    }


}
