package org.firstinspires.ftc.teamcode.scripts;

import org.firstinspires.ftc.teamcode.systems.ArmPosition;
import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.brains.Knowledge;

public class MoveArmTo extends Script{

    private ArmPosition armPosition;

    public MoveArmTo(ArmPosition armPosition){
        this.armPosition = armPosition;
    }

    /**
     * ARM CONTROL HOT BUTTONS
     * 1) Front Ready: X
     * 2) Grabbing: A
     * 3) Capping Level: D-left
     * 4) Top Level: D-up
     * 5) Mid Level: D-right
     * 6) Low Level: D-down
     * 7) Pusher: B
     * 8) Folded: D-left AND
     * 9) Back Ready: Y
     */
    public static MoveArmTo position(ArmPosition targetArmPosition){//}, ArmPosition currentArmPosition){
        /*
        // 0. read arm position
        targetArmPosition.getActuatorValues(actuators);

        // 1) if the controller redirects the arm, change the motion sequence
        // GRAB
        if(targetArmPosition == ArmPosition.grabFront){
            // if arm in front,
            if(currentArmPosition.shoulderAngle >= ArmPosition.FORWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.frontReady);
                motionSequence.add(ArmPosition.grabFront);
                motionSequence.add(ArmPosition.frontReady);
            }
            // else arm in back,
            else if(currentArmPosition.shoulderAngle <= ArmPosition.BACKWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.frontReady);
                motionSequence.add(ArmPosition.grabFront);
                motionSequence.add(ArmPosition.frontReady);
            }
            // else, folded under
            else {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.frontReady);
                motionSequence.add(ArmPosition.grabFront);
                motionSequence.add(ArmPosition.frontReady);
            }

        }
        // DROP
        else if(targetArmPosition == ArmPosition.drop){
            // Make a drop from any position
            ArmPosition armNow = new ArmPosition(targetArmPosition);
            armNow.gripperAngle = -90;
            ArmPosition dropBlock = new ArmPosition(targetArmPosition);
            dropBlock.gripperAngle = 90;
            motionSequence.clear();
            motionSequence.add(dropBlock);
            motionSequence.add(armNow);
        }
        // FRONT READY
        else if(gamePadState.x){
            // if arm in front,
            if(targetArmPosition.shoulderAngle >= ArmPosition.FORWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.frontReady);
            }
            // else arm in back,
            else if(targetArmPosition.shoulderAngle <= ArmPosition.BACKWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.frontReady);
            }
            // else, folded under
            else {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.frontReady);
            }
        }
        // BACK READY
        else if(gamePadState.y){
            // if arm in front,
            if(targetArmPosition.shoulderAngle >= ArmPosition.FORWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.foldedBack);
            }
            // else arm in back,
            else if(targetArmPosition.shoulderAngle <= ArmPosition.BACKWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
            }
            // else, folded under
            else {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
            }
        }
        // CAP
        else if(gamePadState.dPadLeft){
            // if arm in front,
            if(targetArmPosition.shoulderAngle >= ArmPosition.FORWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.capLevel);
            }
            // else arm in back,
            else if(targetArmPosition.shoulderAngle <= ArmPosition.BACKWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.capLevel);
            }
            // else, folded under
            else {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.capLevel);
            }
        }
        // TOP LEVEL
        else if(gamePadState.dPadUp){
            // if arm in front,
            if(targetArmPosition.shoulderAngle >= ArmPosition.FORWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.topLevel);
            }
            // else arm in back,
            else if(targetArmPosition.shoulderAngle <= ArmPosition.BACKWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.topLevel);
            }
            // else, folded under
            else {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.topLevel);
            }
        }
        // MID LEVEL
        else if(gamePadState.dPadRight){
            // if arm in front,
            if(targetArmPosition.shoulderAngle >= ArmPosition.FORWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.midLevel);
            }
            // else arm in back,
            else if(targetArmPosition.shoulderAngle <= ArmPosition.BACKWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.midLevel);
            }
            // else, folded under
            else {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.midLevel);
            }
        }
        // LOW LEVEL
        else if(gamePadState.dPadDown){
            // if arm in front,
            if(targetArmPosition.shoulderAngle >= ArmPosition.FORWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedFront);
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.lowLevel);
            }
            // else arm in back,
            else if(targetArmPosition.shoulderAngle <= ArmPosition.BACKWARD) {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.lowLevel);
            }
            // else, folded under
            else {
                motionSequence.clear();
                motionSequence.add(ArmPosition.foldedBack);
                motionSequence.add(ArmPosition.lowLevel);
            }
        }

         */

        return new MoveArmTo(targetArmPosition);
    }

    public ReturnMessage run(Knowledge knowledge, GamePadState gamePadState){

        return ReturnMessage.Done;
    }
}
