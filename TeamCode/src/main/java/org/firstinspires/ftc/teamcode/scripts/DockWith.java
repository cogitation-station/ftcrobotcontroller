package org.firstinspires.ftc.teamcode.scripts;

import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.util.Vector2D;
import org.firstinspires.ftc.teamcode.brains.Knowledge;

/**
 * Similar to DriveTo script, but the intention is to move slowly toward the target position until
 *      1) front bumper is pressed (returning success)
 *      2) it goes too far or for too long (returning interrupt)
 */
public class DockWith extends Script{

    public final static double DISTANCE_THRESHOLD = 10; // millimeters

    private Vector2D goalPosition;

    private double maxSpeed;

    public DockWith(Vector2D goalPosition, double maxSpeed){
        this.goalPosition = goalPosition;
        this.maxSpeed = maxSpeed;
    }

    public static DockWith position(Vector2D goalPosition, double maxSpeed){
        return new DockWith(goalPosition, maxSpeed);
    }

    // turn toward and drive to a nearby point
    public ReturnMessage run(Knowledge knowledge, GamePadState gamePadState){

        // get the bearing to the destination (in world coordinates)
        Vector2D bearing = goalPosition.subtract(knowledge.robot.getPosition());
        // if bearing magnitude is less than some threshold, return done
        double magnitude = bearing.magnitude();
        if(magnitude < DISTANCE_THRESHOLD){
            gamePadState.leftStickY = 0;
            gamePadState.rightStickX = 0;
            return ReturnMessage.Done;
        }

        // rotate the bearing to the robot's coordinate system
        Vector2D robotBearing = bearing.copy().rotate(-knowledge.robot.getOrientation());
        // set the game pad to drive in that direction relative to the robot
        gamePadState.leftStickY = -robotBearing.getY()/magnitude*maxSpeed;
        gamePadState.rightStickX = robotBearing.getX()/magnitude*maxSpeed;
        return ReturnMessage.Working;
    }
}
