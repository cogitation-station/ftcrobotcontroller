package org.firstinspires.ftc.teamcode.systems;

import org.firstinspires.ftc.teamcode.Actuators;

public class ArmPosition {
    public double elevatorZ; // height of the shoulder above the ground
    public double shoulderAngle;
    public double elbowAngle;
    public double wristAngle;
    public double gripperAngle;

    public final static double Z_THRESHOLD = 10; // inches
    public final static double ANGLE_THRESHOLD = 1; // deg

    public final static double LENGTH_SHOULDER = 10; // length of the shoulder segment (inches)
    public final static double LENGTH_ELBOW = 8; // length of the elbow segment (measured from axis to axis)
    public final static double LENGTH_WRIST = 6; // length of the wrist segment (inches)

    public final static double MAX_Z = 10; // 368.3 mm = 14.5 in = 26.5 in - 12 in
    public final static double PULLEY_RADIUS = 19; // mm
    public final static double FORWARD = 45; // deg
    public final static double BACKWARD = -45;


    /**
     * ARM POSITIONS:
     * 1) Folded: arm fits relaxed inside 18" box
     * 2) Front Ready: reaching forward and at least 2" above the ground, can go to grabbing or folded
     * 3) Grabbing: arm is forward and touching the ground (moving from ready position, straight down to grab a floor level cargo and then moving back up to ready)
     * 4) Passing Under: folded under but with elevator raised, as needed, to move the arm to the opposite side
     * 5) Back Ready: folded close and under but facing the back.
     * 6) Capping Level: extended to capping level.
     * 7) Top Level: extended to top level.
     * 8) Mid Level: extended to mid level.
     * 9) Low Level: extended to low level.
     * 10) Pusher Extended: pusher extended to release the cargo
     */
    public static ArmPosition capLevel = new ArmPosition(MAX_Z, -90, 0, 0, -90);
    public static ArmPosition pushCapLevel = new ArmPosition(MAX_Z, -90, 0, 0, 90);
    public static ArmPosition topLevel = new ArmPosition(MAX_Z, -90, 0, 0, -90);
    public static ArmPosition pushTopLevel = new ArmPosition(MAX_Z, -90, 0, 0, 90);
    public static ArmPosition midLevel = new ArmPosition(MAX_Z, -80, 0, 10, -90);
    public static ArmPosition pushMidLevel = new ArmPosition(MAX_Z, -80, 0, 10, 90);
    public static ArmPosition lowLevel = new ArmPosition(MAX_Z, -50, 0, 40, -90);
    public static ArmPosition pushLowLevel = new ArmPosition(MAX_Z, -50, 0, 40, 90);
    public static ArmPosition frontReady = new ArmPosition(MAX_Z, FORWARD, 0, BACKWARD, -90);
    //public static ArmPosition backReady = new ArmPosition(MAX_Z, BACKWARD, 0, BACKWARD, -90);
    public static ArmPosition grabFront = new ArmPosition(0, FORWARD, 0, BACKWARD, -90);
    public static ArmPosition foldedFront = new ArmPosition(MAX_Z, 90, -90, -90, -90);
    public static ArmPosition foldedBack = new ArmPosition(MAX_Z, BACKWARD, -90, -90, -90);
    public static ArmPosition foldedCompact = new ArmPosition(MAX_Z, BACKWARD, -90, -90, -90);

    public ArmPosition(){

    }

    public ArmPosition(double elevatorZ, double shoulderAngle, double elbowAngle, double wristAngle, double gripperAngle){
        this.elevatorZ = elevatorZ;
        this.shoulderAngle = shoulderAngle;
        this.elbowAngle = elbowAngle;
        this.wristAngle = wristAngle;
        this.gripperAngle = gripperAngle;
    }

    public ArmPosition(ArmPosition armPosition){
        this.elevatorZ = armPosition.elevatorZ;
        this.shoulderAngle = armPosition.shoulderAngle;
        this.elbowAngle = armPosition.elbowAngle;
        this.wristAngle = armPosition.wristAngle;
        this.gripperAngle = armPosition.gripperAngle;
    }

    public void setTo(ArmPosition armPosition) {
        this.elevatorZ = armPosition.elevatorZ;
        this.shoulderAngle = armPosition.shoulderAngle;
        this.elbowAngle = armPosition.elbowAngle;
        this.wristAngle = armPosition.wristAngle;
        this.gripperAngle = armPosition.gripperAngle;
    }

    public void getActuatorValues(Actuators actuators){
        shoulderAngle = shoulderPositionToAngle(actuators.shoulder.getCurrentPosition());
//        shoulderAngle = Actuators.positionToDeg(actuators.shoulder.getPosition()); // to degrees
        elbowAngle = Actuators.positionToDeg(actuators.elbow.getPosition());
        wristAngle = Actuators.positionToDeg(actuators.wrist.getPosition());
        gripperAngle = Actuators.positionToDeg(actuators.gripper.getPosition());

        //elevatorZ = actuatorState.elevator.getPower();

    }

    public double shoulderPositionToAngle(int ticks){
      //  if()
        return 0.0;
    }

    public double heightOfShoulder(){
        return elevatorZ;
    }

    public double heightOfElbow(){
        return elevatorZ - LENGTH_SHOULDER * Math.cos(shoulderAngle);
    }

    public double heightOfWrist(){
        return elevatorZ - LENGTH_SHOULDER * Math.cos(shoulderAngle) - LENGTH_ELBOW * Math.cos(shoulderAngle + elbowAngle);
    }

    public double heightOfGripperTip(){
        return elevatorZ - LENGTH_SHOULDER * Math.cos(shoulderAngle) - LENGTH_ELBOW * Math.cos(shoulderAngle + elbowAngle) - LENGTH_WRIST * Math.cos(shoulderAngle + elbowAngle + wristAngle);
    }

    public boolean matches(ArmPosition goal){
        if(Math.abs(this.elevatorZ - goal.elevatorZ) < Z_THRESHOLD){
            if(Math.abs(this.shoulderAngle - goal.shoulderAngle) < ANGLE_THRESHOLD){
                if(Math.abs(this.elbowAngle - goal.elbowAngle) < ANGLE_THRESHOLD){
                    if(Math.abs(this.wristAngle - goal.wristAngle) < ANGLE_THRESHOLD){
                        if(Math.abs(this.gripperAngle - goal.gripperAngle) < ANGLE_THRESHOLD){
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
