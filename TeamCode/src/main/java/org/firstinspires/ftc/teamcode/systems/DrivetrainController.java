package org.firstinspires.ftc.teamcode.systems;

import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.Sensors;

public class DrivetrainController {

    // Front Left Motor 00
    public double left = 0.0;

    // Front Right Motor 01
    public double right = 0.0;

    // Gear Ratio Ratio = 19.2:1
    // Encoder Shaft: 28 pulses per revolution
    // Gearbox output: 537.7 pulses per revolution
    public int leftTicks = 0;
    public int rightTicks = 0;

    public final static int BASE_TICKS = 200;

    /**
     * when y is up, frontLeft and frontRight = 1
     *
     * when y is down, frontLeft and frontRight = -1
     * when x is right, frontLeft = 1, and frontRight = -1
     * when x is left, frontLeft = -1, and frontRight = 1
     * @param gamePadState
     */

    public void twoStickTracksUpdate(GamePadState gamePadState, Sensors sensors){
        double x = flatten(gamePadState.rightStickX);
        double y = flatten(gamePadState.leftStickY);

        left = -y + x;
        right = -y - x;
        if(right > 1.0){
            right = 1.0;
        }
        else if(right < -1.0){
            right = -1.0;
        }
        if(left > 1.0){
            left = 1.0;
        }
        else if(left < -1.0){
            left = -1.0;
        }
        rightTicks = sensors.rightMotorPositionOld + (int)(BASE_TICKS * right);
        leftTicks = sensors.leftMotorPositionOld + (int)(BASE_TICKS * left);
    }

    public double flatten(double value){
        if(value > 1.0){
            value = 1.0;
        }
        else if(value < -1.0){
            value = -1.0;
        }
        return value * Math.abs(value);
    }
}
