package org.firstinspires.ftc.teamcode.systems;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

import org.firstinspires.ftc.teamcode.GamePadState;

public class DuckSpinnerController {

    public double duckPower = 0.0;
    public double startSpinTime = 0.0;
    public DcMotor.Direction direction = DcMotor.Direction.FORWARD;

    // spin only when the right trigger is held
    public void spinOnTrigger(GamePadState gamePadState){
        // if your team is blue, you should spin CW, else CCW

        if(gamePadState.rightTrigger > 0) {
            // test direction and function
            duckPower = gamePadState.rightTrigger;
            direction = DcMotorSimple.Direction.FORWARD;
        }
        else if(gamePadState.leftTrigger > 0) {
            // test direction and function
            duckPower = gamePadState.leftTrigger;
            direction = DcMotorSimple.Direction.REVERSE;
        }
        else{
            duckPower = 0.0;
        }
    }

    // Continue spinning until the duck falls by time
    public void autoSpin(){
        //

    }
}
