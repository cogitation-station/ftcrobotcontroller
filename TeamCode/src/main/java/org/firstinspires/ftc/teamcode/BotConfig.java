package org.firstinspires.ftc.teamcode;

public class BotConfig {
    // CONSTANTS

    double g = 32*12; // gravitational acceleration (32 ft/s^2)(12 in/ft)

    double blockWeight = 113; //nominal weight of block with three weights, verified by mesurement (g)

    double ballWeight = 30.1; //nominal weight of ball, verified by mesurment (g)


// PHYSICAL TEST PARAMETERS FOR THE ROBOT

    double vmin = 11; // vertical distance (height) of the shoulder axis from the wrist joint when reaching forward to the floor level (in)

    double hmin = 11; // horizontal distance from the shoulder axis to the wrist joint when reaching to the floor level (in)

    double lowLevelHeight = 3.0; // +/-1.5 for tipping

    double midLevelHeight = 8.5; // +/-1.5 for tipping

    double topLevelHeight = 14.7; // +/-1.5 for tipping

    double capLevelHeight = 28.3; // 8 comes from the height of the shipping element


// TOWER

    double wtop = 1000; // weight of the tower top (oz)

    double wbottom = 1000; // weight of the tower bottom (oz)


// DRIVETRAIN

    double rWh = 2; // the radius of the drivetrain wheel (in)

    double wDT = 500; // the weight of the drivetrain (oz)

    double nDrivers = 4; // the number of drivetrain motors

    boolean mechanum = true; // indicates if mechanum wheels are being used

    double motorToDriveGearRatio = 1; // the motor to drive gear ratio


// ARM

    double w0 = 0; // counter weight on arm

    double w1 = gramsToOunces(356); // weight of the upper arm (oz)

    double w2 = gramsToOunces(242); // weight of the lower arm (oz)

    double w3 = 4; // weight of the gripper (oz)

    double w4 = gramsToOunces(blockWeight); // weight of the heaviest block or ball (oz)

    double l0 = 3; // length of counter weight (in)

    double l1 = 8; // length of upper arm (in)

    double l2 = 8; // length of lower arm (in)

    double l3 = 6; // length of gripper (in)

    double r0 = 2; // distance from shoulder axis to counter weight center of mass (in)

    double r1 = l1/2; // tapers, distance from shoulder axis to upper arm center of mass (in)

    double r2 = l2/2; // tapers, distance from elbow axis to lower arm center of mass (in)

    double r3 = l3/2; // distance from wrist axis to center of gripper mass (in)

    double r4 = 3; // distance from wrist axis to center of block or ball (in)


// MOTOR SPECS

    double tMotor = 338; // torque of the motor (oz in)

    double sMotor = 312; // motor speed (rpm)

    double wMotor = rpmToRadPerSec(sMotor); // angular frequency (rad/sec)  of motor


// TORQUE SERVO SPECS

    double tTServo = 350; // torque of the servo (oz in)

    double sTServo = 60; // speed of the torque servo (rpm)

    double wTServo = rpmToRadPerSec(sTServo); // angular frequency (rad/sec) of torque servo


// SPEED SERVO SPECS

    double tSServo = 150; // torque of the servo (oz in)

    double sSServo = 145; // speed of the torque servo (rpm)

    double wSServo = rpmToRadPerSec(sSServo); // angular frequency (rad/sec) of torque servo


// FREE PARAMETERS (actuator states)

    double z = 10; // the amount of linear extension of the tower (in)

    double th1 = 45; // the angle of the shoulder motor (deg) relative to its mount on the tower (pointing down is 0)

    double th2 = 90; // the angle of the elbow motor (deg) relative to its mount on the upper arm

    double th3 = -45; // the angle of the wrist motor (deg) relative to its mount on the lower arm


    // DEPENDENT PARAMETERS (resulting from actuator states)

    double ph1 = th1; // the angle of the upper arm relative to the vertical or tower

    double ph2 = th1 + th2; // the angle of the lower arm relative to the vertical or tower

    double ph3 = th1 + th2 + th3; // the angle of the wrist relative to the vertical or tower


    // FUNCTIONS

    double requiredArmLength() {return Math.sqrt(hmin*hmin + vmin*vmin);} // the arm length is l1 + l2 but can be divided any way

    double shoulderTorque() {return r1*w1 + (l1+r2)*w2 + (l1+l2+r3)*w3 + (l1+l2+r4)*w4 - r0*w0;}

    double  elbowTorque() {return  r2*w2 + (l2+r3)*w3 + (l2+r4)*w4;}

    double  wristTorque() {return  r3*w3 + r4*w4;}

    double  linearLiftWeight() {return  w0 + w1 + w2 + w3 + w4 + wtop;}

    double  totalRobotWeight() {return  w0 + w1 + w2 + w3 + w4 + wtop + wbottom + wDT;}

    double  mechanumForce(double f) {return  Math.sin(45)*f; }// mechanum wheel force vector breakdown, reduces acceleration force by a factor of .707
    
    // UNIT CONVERSION FUNCTIONS

    double  gramsToOunces(double grams) {return  grams * 0.035274;}

    double  inchesToMillimeters(double inches) {return  inches * 25.4;}

    double  millimetersToInches(double millimeters) {return  millimeters/25.4;}

    double  rpmToRadPerSec(double rpm) {return  rpm*360/60*Math.PI/180; }// (rev/min)(360 deg/rev)(min/60 sec)(Pi radians/180 deg)

// the driving force per wheel is the motor torque divided by the radius of the wheel * gear ratio, reduced if mechanum wheel

    double  forcePerWheel() {return  mechanum ? Math.sin(45) * motorToDriveGearRatio * tMotor/rWh : motorToDriveGearRatio * tMotor/rWh;}

    double  driveForce() {return  forcePerWheel() * nDrivers;}

    double  maxAccel() {return  g*driveForce()/totalRobotWeight();} // maximum acceleration of the robot
    
    double  topSpeed() {return  mechanum ? Math.sin(45) * wMotor * rWh / motorToDriveGearRatio : wMotor * rWh / motorToDriveGearRatio;  }// theoretical top speed of the wheel or chassis


// TEST FUNCTIONS

    boolean  testArmMovesUnderShoulder() { return l1 < hmin;}

    boolean  testArmReachesOutForwardToGround() { return  (l1 + l2)*(l1 + l2) >= hmin*hmin + vmin*vmin;}

    boolean  canLiftMaxWeightAtShoulder() { return  shoulderTorque() <= tTServo * 1.5;} // using the timing pulley for 24:16 rear ratio

    boolean  canLiftMaxWeightAtElbow() { return  elbowTorque() <= tTServo;}

    boolean  canLiftMaxWeightAtWrist() { return  wristTorque() <= tTServo;}


    void runTests(){

        System.out.println("w1=" + w1 + " oz");

        System.out.println("w2=" + w2 + " oz");

        System.out.println("w3=" + w3 + " oz");



        System.out.println("RUNNING TESTS");

        if(!testArmMovesUnderShoulder()){

            System.out.println("FAILS TEST: Arm Moves Under Shoulder");

        }

        if(!testArmReachesOutForwardToGround()){

            System.out.println("FAILS TEST: Arm Reaches Out Forward To Ground");

        }

        double shoulderTorque = shoulderTorque();

        System.out.println("shoulderTorque=" + shoulderTorque + " oz-in");

        if(!canLiftMaxWeightAtShoulder()){

            System.out.println("FAILS TEST: Can Lift Max Weight At Shoulder");

        }

        double elbowTorque = elbowTorque();

        System.out.println("elbowTorque=" + elbowTorque + " oz-in");

        if(!canLiftMaxWeightAtElbow()){

            System.out.println("FAILS TEST: Can Lift Max Weight At Elbow");

        }

        double wristTorque = wristTorque();

        System.out.println("wristTorque=" + wristTorque + " oz-in");

        if(!canLiftMaxWeightAtWrist()){

            System.out.println("FAILS TEST: Can Lift Max Weight At Wrist");

        }

        System.out.println("TESTING COMPLETE");

    }
}
