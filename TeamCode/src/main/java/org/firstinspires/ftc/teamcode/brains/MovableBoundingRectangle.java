package org.firstinspires.ftc.teamcode.brains;

import org.firstinspires.ftc.robotcore.external.navigation.AngularVelocity;
import org.firstinspires.ftc.teamcode.util.Vector2D;
import org.firstinspires.ftc.teamcode.util.Vector3D;
import org.firstinspires.ftc.teamcode.util.Vector4D;

/**
 * A Movable Object such as a robot
 */
public class MovableBoundingRectangle extends SimpleBoundingRectangle {

    protected double orientation = 0;
    protected double orientationLast = 0;

    protected double angularVelocity = 0;
    protected double angularVelocityLast = 0;

    protected double angularAcceleration = 0;

    Vector2D positionLast = new Vector2D();
    Vector2D velocity = new Vector2D();
    Vector2D velocityLast = new Vector2D();
    Vector2D acceleration = new Vector2D();

    public MovableBoundingRectangle(double x, double y, double dx, double dy, double angle){
        position.set(x, y);
        width.set(dx, dy);
        orientation = angle;
    }

    public void initializePosition(Vector2D p){
        position.set(p);
        positionLast.set(p);
    }

    public void initializePosition(double x, double y){
        position.set(x, y);
        positionLast.set(x, y);
    }

    public void updatePosition(Vector2D dPos, double dt){
        updatePosition(dPos.getX(), dPos.getY(), dt);
    }

    public void updatePosition(double dx, double dy, double dt){

        positionLast.set(position);
        position.add(dx, dy);
        velocityLast.set(velocity);
        velocity.set(position).subtract(positionLast).divideBy(dt);
        acceleration.set(velocity).subtract(velocityLast).divideBy(dt);
    }

    public void initializeOrientation(double angle){
        orientation = limitAngleToRange(angle, 0, 360);
        orientationLast = orientation;
    }

    public void updateOrientation(double angle, double dt) {
        orientationLast = orientation;
        orientation = limitAngleToRange(angle, 0, 360);

        angularVelocityLast = angularVelocity;
        angularVelocity = (orientation - orientationLast)/dt;
        angularAcceleration = (angularVelocity - angularVelocityLast)/dt;
    }

    public static double limitAngleToRange(double value, double lower, double upper){
        while(value > upper){
            value -= upper;
        }
        while(value < lower){
            value += upper;
        }
        return value;
    }

    public double getDeltaOrientation(){ return orientation - orientationLast; }
    public double getOrientation() { return orientation; }
    public double getOldOrientation() { return orientationLast; }
    public double getAngularVelocity() { return angularVelocity; }
    public double getAngularAcceleration() { return angularAcceleration; }
}
