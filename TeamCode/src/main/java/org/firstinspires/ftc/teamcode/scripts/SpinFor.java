package org.firstinspires.ftc.teamcode.scripts;

import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.util.EasingFunction;
import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.brains.Knowledge;

public class SpinFor extends Script{

    private boolean firstTime = true;
    private double time;
    private ElapsedTime runtime = new ElapsedTime();
    private double easingTime = 1.0; // time to reach full speed

    public SpinFor(double time){
        this.time = time;
    }

    public static SpinFor time(double time){
        return new SpinFor(time);
    }

    @Override
    public ReturnMessage run(Knowledge knowledge, GamePadState gamePadState) {
        if(firstTime) {
            runtime.reset();
            firstTime = false;
        }

        if(runtime.seconds() >= time){
            // done
            gamePadState.leftTrigger = 0.0;
            gamePadState.rightTrigger = 0.0;
            return ReturnMessage.Done;
        }

        if(knowledge.teamColor == Knowledge.TeamColor.Red){
            gamePadState.leftTrigger = 1.0;
            gamePadState.rightTrigger = 0.0;
        }
        else{
            gamePadState.leftTrigger = 0.0;
            gamePadState.rightTrigger = 1.0;
        }

        return ReturnMessage.Working;
    }

    // This alternate version eases up the speed to reduce knocking the duck off
    // We can also try braking at the end if needed
    //@Override
    public ReturnMessage run2(Knowledge knowledge, GamePadState gamePadState) {
        if(firstTime) {
            runtime.reset();
            firstTime = false;
        }

        if(runtime.seconds() >= time){
            // done
            gamePadState.leftTrigger = 0.0;
            gamePadState.rightTrigger = 0.0;
            return ReturnMessage.Done;
        }

        if(knowledge.teamColor == Knowledge.TeamColor.Red){
            gamePadState.leftTrigger = EasingFunction.linear(0.0, 1.0, runtime.seconds()/easingTime);
            gamePadState.rightTrigger = 0.0;
        }
        else{
            gamePadState.leftTrigger = 0.0;
            gamePadState.rightTrigger = EasingFunction.linear(0.0, 1.0, runtime.seconds()/easingTime);
        }

        return ReturnMessage.Working;
    }
}
