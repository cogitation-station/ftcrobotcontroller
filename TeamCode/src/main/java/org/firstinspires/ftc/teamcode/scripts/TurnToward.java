package org.firstinspires.ftc.teamcode.scripts;

import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.util.Vector2D;
import org.firstinspires.ftc.teamcode.brains.Knowledge;

/**
 * Turn toward, rotates the robot toward a destination point
 */
public class TurnToward extends Script{

    public final static double ANGLE_THRESHOLD = 3; // degrees

    private Vector2D point;
    private double maxSpeed;

    public TurnToward(Vector2D point, double maxSpeed){
        this.point = point;
        this.maxSpeed = maxSpeed;
    }

    public static TurnToward position(Vector2D point, double maxSpeed){
        return new TurnToward(point, maxSpeed);
    }

    // turn toward a specified orientation
    public Script.ReturnMessage run(Knowledge knowledge, GamePadState gamePadState){
        // get rotational direction and difference
        Vector2D bearingVector = point.copy().subtract(knowledge.robot.getPosition());
        double bearing = bearingVector.bearingAngle();
        double angle = bearing - knowledge.robot.getOrientation();
        if(Math.abs(angle) < ANGLE_THRESHOLD){
            gamePadState.leftStickY = 0;
            gamePadState.rightStickX = 0;
            return Script.ReturnMessage.Done;
        }

        if(angle > 0){
            gamePadState.leftStickY = 0;
            gamePadState.rightStickX = maxSpeed;
        }
        if(angle < 0) {
            gamePadState.leftStickY = 0;
            gamePadState.rightStickX = -maxSpeed;
        }

        return Script.ReturnMessage.Working;
    }
}
