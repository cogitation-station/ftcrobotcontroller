package org.firstinspires.ftc.teamcode.scripts;

import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.util.Vector2D;
import org.firstinspires.ftc.teamcode.util.Vector2D;
import org.firstinspires.ftc.teamcode.brains.Knowledge;
import org.firstinspires.ftc.teamcode.brains.SimpleBoundingRectangle;

import java.util.ArrayList;

/**
 * Starting with a current position and orientation, this script instructs the robot to drive to a
 * new position and orientation.  The script is interrupted if a collision is detected
 * by forward bumpers or by unexpected acceleration readings (experimental)
 * or forward range finders seeing something at very close range.
 * If not interrupted...
 * Set the controls to
 * GamePad.leftStickY = -forwardSpeed
 * GamePad.rightStickX = speedDifferential
 *
 * Robot coordinate system:
 * ^ y (forward)
 * |
 * |
 * ----> x (starboard)
 */
public class NavigateTo extends Script{

    public final static double ANGLE_THRESHOLD = 3; // degrees
    public final static double DISTANCE_THRESHOLD = 10; // millimeters

    private ArrayList<Vector2D> pathPlan = new ArrayList<>();

    private SimpleBoundingRectangle destination;
    private double maxSpeed;

    public NavigateTo(SimpleBoundingRectangle destination, double maxSpeed){
        this.destination = destination;
        this.maxSpeed = maxSpeed;
    }

    public static NavigateTo destination(SimpleBoundingRectangle destination, double maxSpeed){
        return new NavigateTo(destination, maxSpeed);
    }

    /**
     * Performs a slow approach to the destination without touching it
     * @param knowledge
     * @param gamePadState
     * @param destination
     * @param maxSpeed
     * @return
     */
    public Script.ReturnMessage dockWith(Knowledge knowledge, GamePadState gamePadState, SimpleBoundingRectangle destination, double maxSpeed){
        return Script.ReturnMessage.Working;
    }

    /**
     * Attempts to park the robot fully inside the destination or as best as possible without a collision
     * @param knowledge
     * @param gamePadState
     * @param destination
     * @param maxSpeed
     * @return
     */
    public Script.ReturnMessage parkIn(Knowledge knowledge, GamePadState gamePadState, SimpleBoundingRectangle destination, double maxSpeed){
        return Script.ReturnMessage.Working;
    }

    /**
     * Generates a path plan
     * @param knowledge
     * @param gamePadState
     * @return
     */
    public Script.ReturnMessage run(Knowledge knowledge, GamePadState gamePadState) {
        // if a path is not already planned, plan a safe path
        if(pathPlan.isEmpty()) {
            Vector2D bearing = destination.getPosition().subtract(knowledge.robot.getPosition());
            if(bearing.magnitude() < DISTANCE_THRESHOLD){
                return Script.ReturnMessage.Done;
            }
            pathPlan.add(destination.getPosition()); // TODO: Replace this line and above 4 lines with a call to A* pathfinder or scripted paths
        }
        if(pathPlan.isEmpty()){
            return Script.ReturnMessage.Done;
        }

        // continue along the path by driveTo the next waypoint
        Vector2D goalPosition = pathPlan.get(0);
        // if the driveTo function returns Done,
        Script.ReturnMessage message = DriveTo.position(goalPosition, maxSpeed).run(knowledge, gamePadState);
        if(message == Script.ReturnMessage.Done){
            pathPlan.remove(0);
            // if the path plan is now empty,
            if(pathPlan.isEmpty()){
                return Script.ReturnMessage.Done;
            }
            // otherwise, continue working driving
            else{
                return Script.ReturnMessage.Working;
            }
        }
        // if the driveTo returns working or interrupt
        return message;
    }

    private void setPathTo(){}
}
