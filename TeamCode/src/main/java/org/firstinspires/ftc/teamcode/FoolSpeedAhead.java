package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.brains.Knowledge;
import org.firstinspires.ftc.teamcode.scripts.DriveTo;
import org.firstinspires.ftc.teamcode.scripts.Script;
import org.firstinspires.ftc.teamcode.scripts.TestDrive;
import org.firstinspires.ftc.teamcode.systems.ArmManager;
import org.firstinspires.ftc.teamcode.systems.DrivetrainController;
import org.firstinspires.ftc.teamcode.systems.DuckSpinnerController;

/**
 * AUTONOMOUS TO DO:
 * GOALS:
 * 1) Make a mode that just parks
 *      navigates to the parking spot
 * 2) Make a mode that Delivers the preloaded shipping element:
 *      unfolds the arm,
 *      drives forward,
 *      rotates to scan with range finder to detect the shipping element bar code,
 *      navigates to the shipping hub without colliding,
 *      moves the arm to the detected level,
 *      and deposits the block.
 * 3) Make a mode that spins for ducks:
 *      navigates to the duck spinner,
 *      presses the spinner against the wheel,
 *      turns the prescribed amount and stops.
 *
 * STEPS:
 * 1) Develop basic navigation using the sensors:
 *      9-DOF
 *      Motor encoders
 *      Range finders
 *      Bumpers
 *    To maintain a sense of position and orientation,
 *    to avoid collisions with walls, game elements, other robots,
 *    to reduce impact by detecting unexpected touches on the bumpers,
 *    to move safely from the currently believed location to a specific destination
 *      a) Read each sensor and examine its telemetry to get a feel for the data provided
 *      b) Make a plan for how the sensors will be used
 *      c) Divide code development into small steps that can each be tested
 *      d) Build each part and test it
 *
 *  2) Develop parking mode using the basic navigation ability
 *  3) Develop duck spinning mode using navigation ability and sensors to detect the spinner
 *  4) Develop autonomous arm control (can also be used for tele-op)
 *  5) Develop a mode to deliver a preloaded block
 *  6) Put the modes together with variations for different strategies and play positions
 *      a) Consider starting from red (left or right) or blue (left or right)
 *      b) Consider backup modes for hardware failures during game day
 *      c) Any other options that might be useful for team strategy
 */
@Autonomous(name = "Fool Speed Ahead", group = "Auto")
public class FoolSpeedAhead extends LinearOpMode {

    private ElapsedTime runtime = new ElapsedTime();

    private Sensors sensors = new Sensors();
    private GamePadState gamePadState = new GamePadState();
    private GamePadState manualGamePad = new GamePadState();
    private Actuators actuators = new Actuators();

    private Knowledge knowledge = new Knowledge();

    private DrivetrainController drivetrainController = new DrivetrainController();
    private ArmManager armManager = new ArmManager();
    private DuckSpinnerController duckSpinnerController = new DuckSpinnerController();

    private Script script;
    private Script.ReturnMessage msg = Script.ReturnMessage.Working;

    @Override
    public void runOpMode() throws InterruptedException {

        telemetry.addData("Status", "Starting...");
        telemetry.update();

        sensors.initialize(hardwareMap, telemetry, knowledge);
        actuators.initializeFull(hardwareMap, telemetry);
        armManager.armManagerInt(actuators);
        knowledge.initialize(sensors, telemetry);

        //script = ParkAt.destination(knowledge.redWarehouse, .5); // TODO: this is an example that should be replaced by the script specified in the Strategy.txt
        //script = DriveTo.position(knowledge.redShippingHub.getPosition(), 0.5); // did not stop or hit its mark! but otherwise
        //script = ParkAt.destination(knowledge.teamParkingPosition, 0.5); // did not stop or hit its mark!
        //script = TurnTo.angle(270, 1.0); // passed testing
        script = new TestDrive();

        telemetry.addData("Status", "Initialized");
        telemetry.update();

        // Wait for the game to start (driver presses PLAY)
        waitForStart();
        runtime.reset();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {
            // copy sensor data from sensor hardware
            sensors.update(actuators);

            // Copy GamePadState from the Gamepad hardware
            //gamePadState.update(gamepad1); // TODO: this doesn't happen in autonomous, but add it in for the teleop mode

            // Update the knowledge state from the Sensors
            knowledge.update(sensors);

            // Call the script to update the gamePadState from the knowledge and sensor states
            manualGamePad.clearAll();
            if(msg == Script.ReturnMessage.Working) {
                msg = script.run(knowledge, manualGamePad);
            }

            // Update the drivetrain controller state from the gamePadState
            drivetrainController.twoStickTracksUpdate(manualGamePad, sensors);

            // Update the duck spinner controller
            duckSpinnerController.spinOnTrigger(manualGamePad);

            // Update the Arm Manager
            armManager.armManualControl(manualGamePad, actuators, sensors); // TODO: change arm controller to fully manual input, replace arm manager

            // Update the motors and servos so they will move as directed by the 3 systems controllers
            actuators.updateMotors(drivetrainController, armManager);
            actuators.updateServos(duckSpinnerController, armManager);

            // Show the elapsed game time and wheel power
            /*
            telemetry.addData("Status", "Run Time: " + runtime.toString());
            telemetry.addData("Motors", "frontLeft (%.2f), frontRight (%.2f)", drivetrainController.left, drivetrainController.right);
            telemetry.addData("Elevator Resets: ", armManager.elevatorResets);
            telemetry.addData("Elevator Ticks: ", armManager.elevatorTicks);
            telemetry.addData("Shoulder Ticks: ", armManager.shoulderTicks);
            telemetry.addData("Elbow Location: ", actuators.elbow.getPosition()*300-150);
            telemetry.addData("Wrist Location: ", actuators.wrist.getPosition()*300-150);
            telemetry.addData("Piston Location: ", actuators.gripper.getPosition());

             */
            telemetry.update();
        }
    }
}
