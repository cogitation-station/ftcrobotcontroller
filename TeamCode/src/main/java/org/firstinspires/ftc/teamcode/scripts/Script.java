package org.firstinspires.ftc.teamcode.scripts;

import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.brains.Knowledge;
import org.firstinspires.ftc.teamcode.brains.SimpleBoundingRectangle;

/**
 * A script is composed of a sequence of other scripts that must be performed before the action
 * of this script.
 * Once the script is done, it is marked complete.
 * Some scripts have arguments/variables HOW TO DO IT????
 *
 * EXAMPLE SCRIPTS:
 *
 * Fool Script SEQUENTIALLY (in selected order)
 *      PlacePreloadedBlock
 *      ScoreADuck
 *      ParkAt
 *
 * ParkAt(SelectedParking, MaxSpeed)                                               ?
 *      SIMULTANEOUSLY: NavigateTo(selectedParking), MoveArmTo(neutral)
 *
 * PlacePreloadedBlock SEQUENTIALLY
 *      MoveArmTo(frontReady)
 *      DriveTo(readRedBarcodeA) // use points for your color and side
 *      TurnTo (scanLeftFacing)
 *      SIMULTANEOUSLY: TurnTo (scanRightFacing), ScanBarCode
 *      SIMULTANEOUSLY: NavigateTo(teamShippingHub), MoveArmTo(barCodedHeight)
 *      ReleaseBlock
 *
 * ScoreADuck SEQUENTIALLY                                                                               done
 *      SIMULTANEOUSLY: MoveArmTo(neutral), NavigateTo(teamCarousel)
 *      TurnToward(teamCarousel)
 *      DockWith(teamCarousel)
 *      SpinFor(time)
 *
 * NavigateTo(position, maxSpeed)                                                                       in progress
 *      if(!collisionDanger (Robot, X))
 *          moveAlongPath(findPathToFrom(Position, Orientation, Robot.position, Robot.orientation))
 *
 * MoveArmTo(ArmPosition)                                                                            in progress
 *      if(ArmPosition != goalArmPosition)
 *          change the goal and plan a new motion sequence
 *      if(!armInPosition(goalArmPosition))
 *          continueMotionSequence
 *
 * ReadBarCode()
 *      Record Angle of Orientation with shortest distance then extrapolate position
 *
 * Release Block
 *
 * SpinFor(Direction, Time)                                                                            done
 *      Move the spinner in the specified direction
 *
 * DockWith                                                                                             (needs button press to stop)
 *      Move slowly forward until spinner button is pressed unless a collision occurs or went too long
 *
 * SIMULTANEOUSLY                                                                                    done
 *      Perform scripts in parallel
 *
 * SEQUENTIALLY                                                                                      done
 */
public abstract class Script {

    public abstract ReturnMessage run(Knowledge knowledge, GamePadState gamePadState);

    public void test(SimpleBoundingRectangle destination, double maxPower){
        Sequentially spinForADuck = new Sequentially();
        spinForADuck.addScript(NavigateTo.destination(destination, maxPower));
    }

    public enum ReturnMessage{
        Done, Working, Interrupt;
    }
}
