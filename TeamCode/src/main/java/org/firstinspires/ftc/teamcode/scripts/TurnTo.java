package org.firstinspires.ftc.teamcode.scripts;

import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.brains.Knowledge;
import org.firstinspires.ftc.teamcode.util.EasingFunction;

/**
 * Turn to changes the robot's facing to a specific bearing angle
 */
public class TurnTo extends Script{

    public final static double ANGLE_THRESHOLD = 1; // degrees

    private double goalAngle;
    private double maxSpeed;

    public TurnTo(double goalAngle, double maxSpeed){
        this.goalAngle = goalAngle;
        this.maxSpeed = maxSpeed;
    }

    public static TurnTo angle(double goalAngle, double maxSpeed){
        return new TurnTo(goalAngle, maxSpeed);
    }

    // turn toward a specified orientation
    public Script.ReturnMessage run(Knowledge knowledge, GamePadState gamePadState){
        // get rotational direction and difference
        double angle = goalAngle - knowledge.robot.getOrientation();
        if(Math.abs(angle) < ANGLE_THRESHOLD){
            gamePadState.leftStickY = 0;
            gamePadState.rightStickX = 0;
            return Script.ReturnMessage.Done;
        }

        if(angle > 0){
            gamePadState.leftStickY = 0;
            gamePadState.rightStickX = -EasingFunction.linear(maxSpeed, 0.5, 1.0-angle/90.0); // turn left
        }
        if(angle < 0) {
            gamePadState.leftStickY = 0;
            gamePadState.rightStickX = EasingFunction.linear(maxSpeed, 0.5, 1.0+angle/90.0); // turn right
        }

        knowledge.telemetry.addData("TurnTo angle: ", goalAngle );
        knowledge.telemetry.addData("Turn angleDelta: ", angle);
        knowledge.telemetry.addData("RightStick: ", gamePadState.rightStickX);
        return Script.ReturnMessage.Working;
    }
}
