package org.firstinspires.ftc.teamcode.util;

public class OrientationAngle {
    private double angle;

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }
}
