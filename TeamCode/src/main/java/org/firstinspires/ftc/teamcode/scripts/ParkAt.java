package org.firstinspires.ftc.teamcode.scripts;

import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.brains.Knowledge;
import org.firstinspires.ftc.teamcode.brains.SimpleBoundingRectangle;

public class ParkAt extends Script{

    Sequentially sequentially = new Sequentially();
    SimpleBoundingRectangle destination;
    double maxSpeed;

    public ParkAt(SimpleBoundingRectangle destination, double maxSpeed){
        this.destination = destination;
        this.maxSpeed = maxSpeed;

        sequentially.addScript(DriveTo.position(destination.getPosition(), maxSpeed))
                .addScript(TurnTo.angle(90, 1.0));
    }

    public static ParkAt destination(SimpleBoundingRectangle destination, double maxSpeed){
        return new ParkAt(destination, maxSpeed);
    }

    @Override
    public ReturnMessage run(Knowledge knowledge, GamePadState gamePadState) {
        return sequentially.run(knowledge, gamePadState);
    }
}
