package org.firstinspires.ftc.teamcode.scripts;

import org.firstinspires.ftc.teamcode.GamePadState;
import org.firstinspires.ftc.teamcode.brains.Knowledge;

import java.util.ArrayList;

public class Sequentially extends Script{

    private ArrayList<Script> scripts = new ArrayList<>();

    public static Sequentially go(){
        return new Sequentially();
    }

    public Sequentially addScript(Script script){
        scripts.add(script);
        return this;
    }

    /**
     * Simultaneously perform all the scripts on this list
     * @return
     */
    public Script.ReturnMessage run(Knowledge knowledge, GamePadState gamePadState){
        // if all the scripts are done
        if(scripts.isEmpty()){
            return Script.ReturnMessage.Done;
        }

        // get the first task on the list
        Script script = scripts.get(0);

        // run the script, and if not done, return working or error
        Script.ReturnMessage message = script.run(knowledge, gamePadState);
        if(message != Script.ReturnMessage.Done){
            return message;
        }

        // if the script is done, remove it from the list
        scripts.remove(script);

        // otherwise, continue working
        return message;
    }
}
