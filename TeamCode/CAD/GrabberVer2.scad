// import statments
use <ArmVer2.scad>;
use <GrabberDomeVer1.scad>;
include <ArmVer2Num.scad>;
include <GrabberVer2Num.scad>;
include <UtilityKit.scad>;

$fn = 100;

//grabberVer2();

replacmentPiston();

grabberExtend = .05; // set from 0 to 1

module replacmentPiston() {
    translate([0, 0, pusherRadius]) rotate([90, 0, 0]) union() {
        difference() {
            piston();
            translate([-distance*2+extenderX+distance, 0, 0]) cube([distance*4, pusherRadius*5, pusherRadius*5], true);
        }
        translate([extenderX-10+distance, segmentY/4, 0]) cube([20, segmentY/2-.4, pistonRadius*2-2.8], true);
    }
    
    translate([-10, 10, segmentY/2+extenderX/2]) rotate([-90, 0, 0]) difference() {
        piston();
        translate([distance*3+extenderX, 0, 0]) cube([distance*4, pusherRadius*5, pusherRadius*5], true);
        translate([extenderX-11+distance, segmentY/4, 0]) cube([22+.1, segmentY/2, pistonRadius*2-2.4], true);
    }
}

module grabberVer2() {
    translate([hoseOuterRadius*2, 0, 0]) union() {
        translate([(distance)*grabberExtend+20, 0, 0]) piston();
    
        difference() {
            union() {
                //translate([segmentX + distance, 0, 0]) rotate([0, -90, 0]) dome();
                middSection();
                translate([segmentX-segmentZ/3, 0, 0]) endSection();
                pistonShell();
                translate([-segmentZ/2-hoseOuterRadius*2, 0, 0]) upperPreJoint(segmentY, segmentZ);
            }
            hull() {
                translate([21, 0, 0])rotate([0, 90, 0]) cylinder(1000, pistonShellInnerRadius, pistonShellInnerRadius, false);
                translate([21, segmentY/2, 0]) rotate([0, 90, 0]) cylinder(1000, pistonShellInnerRadius, pistonShellInnerRadius, false);
            }
        }
    }
}

module middSection() { // midsection
    middLength = segmentX-segmentZ/3;
    
    // long sticks
    difference() {
        union() {
            translate([0, segmentY/2-edgeThickness, -segmentZ/2]) cube([middLength, edgeThickness, segmentZ], false);
            translate([0, -segmentY/2, -segmentZ/2]) cube([middLength, edgeThickness, segmentZ], false);
        }
        translate([.1+20, -(segmentY+.2)/2, -segmentZ/6]) cube([middLength-20, segmentY+.2, segmentZ/3], false);
    }
    
    // corners
    translate([segmentX-segmentZ/6, segmentY/2-edgeThickness/2, (segmentZ/2-segmentZ/6)]) corner();
    translate([segmentX-segmentZ/6, -(segmentY/2-edgeThickness/2), (segmentZ/2-segmentZ/6)]) corner();
    translate([segmentX-segmentZ/6, segmentY/2-edgeThickness/2, -(segmentZ/2-segmentZ/6)]) rotate([180, 0, 0]) corner();
    translate([segmentX-segmentZ/6, -(segmentY/2-edgeThickness/2), -(segmentZ/2-segmentZ/6)]) rotate([180, 0, 0]) corner();
    
    translate([segmentX-segmentZ/6, 0, 0]) cube([segmentZ/3, segmentY, segmentZ/3], true);
}

module corner() { // makes corners
    difference() {
        cube([segmentZ/2, edgeThickness, segmentZ/3], true);
        rotate([0, 45, 0]) translate([0, 0, segmentZ/4]) cube([segmentZ, edgeThickness+.2, segmentZ/2], true);
    }
}

module pistonShell() {
    hull() {
        translate([segmentX, 0, 0]) rotate([0, 90, 0]) ring(pistonShellX, pistonShellInnerRadius, pistonShellOuterRadius, false);
        translate([segmentX, segmentY/2, 0]) rotate([0, 90, 0]) ring(pistonShellX, pistonShellInnerRadius,  pistonShellOuterRadius, false);
    }
}

module piston() {
    pistonExtender(); // grabby thing on -x end
    translate([extenderX, 0, 0]) rotate([0, 90, 0]) union() {
        hull() {
            cylinder(pistonX, pistonRadius, pistonRadius, false); // piston rod
            translate([0, segmentY/2, 0]) cylinder(pistonX, pistonRadius, pistonRadius, false); // piston rod
        }
        pistonPusher(); // pusher
    }
}

module pistonPusher() {
    difference() {
        union() {
            translate([0, 0, pistonX]) cylinder(pusherThickness, pusherRadius, pusherRadius, false); // pusher
            translate([0, pulleyDistance+hoseOuterRadius+segmentY/2, pistonX]) cylinder(pusherThickness/10*2, extenderX/2, extenderX/2); // grab ring 
            hull() {
                translate([0, pulleyDistance+hoseOuterRadius+segmentY/2, pistonX]) cylinder(pusherThickness/10*2, extenderX/2, extenderX/2); // grab ring 
                translate([0, pusherRadius-extenderX/2, pistonX+pusherThickness/2]) cube([extenderX, extenderX, pusherThickness], true);
            }
        }
        translate([0, pulleyDistance+hoseOuterRadius+segmentY/2, pistonX-.1]) cylinder(pusherThickness*2, extenderX/4, extenderX/4); // grab cut
    }
}

module pistonExtender() { // little grabbey section on the -x end of the piston
    translate([0, 0, 0]) difference() {
        union() {
            translate([0, -segmentY/2, -(pistonRadius*2.5)/2]) cube([extenderX, segmentY, pistonRadius*2.5], false);
            translate([extenderX/2, segmentY/2, 0]) cylinder(pistonRadius*2.5, extenderX/2, extenderX/2, true);
            translate([extenderX/2+extenderX/4, segmentY/2, 0]) cube([extenderX/2, extenderX, pistonRadius*2.5], true);
            translate([extenderX/2, -segmentY/3*2, 0]) rotate([0, 90, 0]) cylinder(extenderX, extenderX/2, extenderX/2, true);
        }
        translate([extenderX/2, -segmentY/3*2, 0]) rotate([0, 90, 0]) cylinder(extenderX*2, crimpY/2, crimpY/2, true);
        translate([extenderX/4, -segmentY/3*2-crimpZ/2, 0]) cube([extenderX, crimpZ, crimpY], true);
    }
}

hoseHolderRadius = hoseOuterRadius + standardThickness;
pulleyDistance = hoseOuterRadius+standardThickness+hoseInnerRadius*2;
turnPulleyRadius = pulleyDistance/2;

module endSection() {
    translate([(holderDepth+turnPulleyRadius*2+lineThickness*2)/2, 0, 0]) cube([holderDepth+turnPulleyRadius*2+lineThickness*2, segmentY, segmentZ/3], true);
    translate([0, -segmentY/2, 0]) hoseHolders();
    rotate([180, 0, 0]) translate([0, -segmentY/2, 0]) hoseHolders2();
}

module hoseHolders() {
    pulleyHolderX = turnPulleyRadius*2+lineThickness*2;
    pulleyHolderY = pulleyDistance+standardThickness;
    pulleyHolderZ = segmentZ/3;
    
    difference() { // pulley holder
        translate([pulleyHolderX/2+holderDepth, -pulleyHolderY/2, 0]) cube([pulleyHolderX, pulleyHolderY, pulleyHolderZ], true); // shell
        translate([(pulleyHolderX+.2)/2+holderDepth-.1, -(pulleyHolderY+.2)/2-.1, 0]) cube([pulleyHolderX+.2, pulleyHolderY+.2, (hoseHolderRadius-standardThickness-lineThickness)*2+lineThickness*4], true); // bore
        translate([holderDepth+lineThickness*2+turnPulleyRadius, -pulleyDistance+standardThickness, 0]) cylinder(segmentZ/3+.2, standardThickness, standardThickness, true); // axle hole
    }
    
    translate([0, -pulleyDistance-hoseOuterRadius*3, 0]) rotate([0, -90, 0]) cylinder(hosePegLength, hoseOuterRadius, hoseOuterRadius);// peg
    
    difference() {
        union() {
            translate([holderDepth+lineThickness*2+turnPulleyRadius, -pulleyDistance+standardThickness, 0]) hosePulley(); // pulley
            hull() {
                translate([holderDepth/2, -(pulleyDistance+hoseOuterRadius*2+standardThickness)/2, 0]) cube([holderDepth, pulleyDistance+hoseOuterRadius*2+standardThickness, segmentZ/3], true);// largest cube
                translate([.5, -(hoseOuterRadius*2-standardThickness)/2-(pulleyDistance+hoseOuterRadius*2+standardThickness), 0]) cube([1, hoseOuterRadius*2-standardThickness, segmentZ/3], true);// peg holder
            }
        }
        translate([0, -pulleyDistance-hoseOuterRadius , 0]) rotate([0, 90, 0]) union() {
            translate([0, 0, .1])cylinder(holderDepth, hoseInnerRadius, hoseInnerRadius); // hose hole inner
            translate([0, 0, -.1]) cylinder(holderDepth-standardThickness+.1, hoseOuterRadius, hoseOuterRadius); // hose hole outer
        }
        translate([(holderDepth+.2)/2-.1, -hoseInnerRadius-standardThickness/2, 0]) cube([holderDepth+.2, hoseInnerRadius*2+standardThickness, hoseInnerRadius*2+lineThickness*2], true); 
    }
}

module hoseHolders2() {
    pulleyHolderX = turnPulleyRadius*2+lineThickness*2;
    pulleyHolderY = pulleyDistance+standardThickness;
    pulleyHolderZ = segmentZ/3;
    
    translate([pulleyHolderX/2+holderDepth, -(pulleyHolderY/5*3)/2, 0]) cube([pulleyHolderX, pulleyHolderY/5*3, pulleyHolderZ], true); // shell
    
    translate([0, -pulleyDistance-hoseOuterRadius-hoseOuterRadius*2, 0]) rotate([0, -90, 0]) cylinder(hosePegLength, hoseOuterRadius, hoseOuterRadius);// peg
    
    difference() {
        union() {
            hull() {
                translate([holderDepth/2, -(pulleyDistance+hoseOuterRadius*2+standardThickness)/2, 0]) cube([holderDepth, pulleyDistance+hoseOuterRadius*2+standardThickness, segmentZ/3], true);// largest cube
                translate([.5, -(hoseOuterRadius*2-standardThickness)/2-(pulleyDistance+hoseOuterRadius*2+standardThickness), 0]) cube([1, hoseOuterRadius*2-standardThickness, segmentZ/3], true);// peg holder
            }
        }
        translate([0, -pulleyDistance-hoseOuterRadius , 0]) rotate([0, 90, 0]) union() {
            translate([0, 0, .1])cylinder(holderDepth, hoseInnerRadius, hoseInnerRadius); // hose hole inner
            translate([0, 0, -.1]) cylinder(holderDepth-standardThickness+.1, hoseOuterRadius, hoseOuterRadius); // hose hole outer
        } 
    }
}

module hosePulley() {
    pulleyCoords = hoseHolderRadius-standardThickness-lineThickness;
    
    cylinder(segmentZ/3, standardThickness-lineThickness, standardThickness-lineThickness, true);

    difference() {
        union() {
            hull() {
                translate([0, 0, pulleyCoords-1]) cylinder(1, turnPulleyRadius, turnPulleyRadius, false);
                translate([0, 0, 0]) cylinder(1, turnPulleyRadius/2, turnPulleyRadius/2, true);
            }
            translate([0, 0, 0]) rotate([0, 0, 0]) hull() {
                translate([0, 0, 0]) cylinder(1, turnPulleyRadius/2, turnPulleyRadius/2, true);
                translate([0, 0, -pulleyCoords]) cylinder(1, turnPulleyRadius, turnPulleyRadius, false);
            }
        }
    }
}