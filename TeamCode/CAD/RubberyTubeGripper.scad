$fn = 200;

function inToMM(in) = 25.4 * in;

height = inToMM(6);
outerRadius = inToMM(2);
thickness = 4;
taper = 6;
innerRadius = outerRadius-thickness;

difference(){
    cylinder(h=height, r1=outerRadius, r2=outerRadius-taper, center=true);
    cylinder(h=height+2, r1=innerRadius, r2=innerRadius-taper, center=true);
}