// import statments
include <UtilityKit.scad>;

$fn = 100;

map();

function inToMm(inches) = inches * 25.4; // translates inches to millimeters

plateEdge = inchesToMillimeters(.5);
plateInner = inchesToMillimeters(23);
plateSize = inchesToMillimeters(24);

tapeThickness = inchesToMillimeters(2);

barrierThickness = inToMm(5.7);
barrierDistanceFromWall = inToMm(13.75);

wareHouseX = inToMm(43.5);
wareHouseY = inToMm(43.5);
distanceFromCenter = 55.3+1.3-35.4; // distanceFromCenter+5.7/2 for the center of barrier

radiusOfShippingHub = 17.9/2;
radiusOfSharedHub = 18/2;

carouselPoleRadius = 1.31/2;
distanceFromWall = 50; // duck spinner

module map() {
    translate([0, 0, -0]) field();
    
    color( "White", 1 ) union() {
        boxTape(24+43.5/2+5.7/2, 24*4+43.5/2+5.7/2, 43.5, 43.5); // Red Warehouse
        boxTape(-(24+43.5/2+5.7/2), 24*4+43.5/2+5.7/2, 43.5, 43.5); // Blue Warehouse
        
        boxTape(24*2.5, 24*5.5, 23, 23); // Red Freight
        boxTape(-24*2.5, 24*5.5, 23, 23); // Blue Freight
        
        boxTape(24*1.5, 24/2, 23, 23); // Red Storage
        boxTape(-24*1.5, 24/2, 23, 23); // Blue Storage
        
        roundTape(24, 24*2.5, 17.9/2); // Red Shipping Hub
        roundTape(-24, 24*2.5, 17.9/2); // Blue Shipping Hub
        
        roundTape(0, 24*5, 9); // Shared Shipping Hub
        
        roundTape(24*3-millimetersToInches(50)-1.31/2, millimetersToInches(50)+1.31/2, 15/2); // Red Carousel
        roundTape(-24*3+millimetersToInches(50)+1.31/2, millimetersToInches(50)+1.31/2, 15/2); // Blue Carousel
        
        tape(24*1.5, 24*3.5, 2, 2); // Red Barcode A-1
        tape(24*1.5, 24*3.5+8.38, 2, 2); // Red Barcode A-2
        tape(24*1.5, 24*3.5-8.38, 2, 2); // Red Barcode A-3
        
        tape(24*1.5, 24*1.5, 2, 2); // Red Barcode B-1
        tape(24*1.5, 24*1.5+8.38, 2, 2); // Red Barcode B-2
        tape(24*1.5, 24*1.5-8.38, 2, 2); // Red Barcode B-3
        
        tape(-24*1.5, 24*3.5, 2, 2); // Blue Barcode A-1
        tape(-24*1.5, 24*3.5+8.38, 2, 2); // Blue Barcode A-2
        tape(-24*1.5, 24*3.5-8.38, 2, 2); // Blue Barcode A-3
        
        tape(-24*1.5, 24*1.5, 2, 2); // Blue Barcode B-1
        tape(-24*1.5, 24*1.5+8.38, 2, 2); // Blue Barcode B-2
        tape(-24*1.5, 24*1.5-8.38, 2, 2); // Blue Barcode B-3
        
        tape(24*3-3, 24*1.5, 2, 2); // Red Start A
        tape(24*3-3, 24*3.5, 2, 2); // Red Start B
        
        tape(-24*3+3, 24*1.5, 2, 2); // Blue Start A
        tape(-24*3+3, 24*3.5, 2, 2); // Blue Start B
    }
}

module tape(locationX = 0, locationY = 0, sizeX = 23, sizeY = 23) {
    translate([inToMm(locationX), inToMm(locationY), 0]) cube([inToMm(sizeX), inToMm(sizeY), 1], true);
}

module boxTape(locationX = 0, locationY = 0, sizeX = 23, sizeY = 23) {
    translate([inToMm(locationX), inToMm(locationY), 0]) difference() {
        cube([inToMm(sizeX), inToMm(sizeY), 1], true);
        cube([inToMm(sizeX)-tapeThickness, inToMm(sizeY)-tapeThickness, 2], true);
    }
}

module roundTape(locationX = 0, locationY = 0, radius = plateInner/2) {
    translate([inToMm(locationX), inToMm(locationY), 0]) ring(1, inToMm(radius)-tapeThickness/2, inToMm(radius), true);
}

module field() {
    ground();
    color( "SlateGrey", 1 ) translate([0, inchesToMillimeters(24*4), inchesToMillimeters(1.1/2)]) barrier(); // BARRIER
    
    color( "Red", 1 ) translate([inchesToMillimeters(24), inchesToMillimeters(24*2.5), 0]) shippingHub(); // RED
    color( "Blue", 1 ) translate([-inchesToMillimeters(24), inchesToMillimeters(24*2.5), 0]) shippingHub(); // BLUE
    
    color( "SlateGrey", 1 ) translate([0, inchesToMillimeters(24*5), 0]) sharedShippingHub(); // SHARED
    
    color( "Red", 1 ) translate([-(distanceFromWall+inchesToMillimeters(1.31/2)-inchesToMillimeters(24*3)), distanceFromWall+inchesToMillimeters(1.31/2), 0]) carousel(); // RED
    
    color( "Blue", 1 ) translate([distanceFromWall+inchesToMillimeters(1.31/2)-inchesToMillimeters(24*3), distanceFromWall+inchesToMillimeters(1.31/2), 0]) carousel(); // BLUE
}

module shippingHub() {
    cylinder(inchesToMillimeters(3), inchesToMillimeters(17.9/2), inchesToMillimeters(17.9/2), false);
    translate([0, 0, inchesToMillimeters(13.2)]) cylinder(inchesToMillimeters(1.5), inchesToMillimeters(11.8/2), inchesToMillimeters(11.8/2), false);
    translate([0, 0, inchesToMillimeters(7)]) cylinder(inchesToMillimeters(1.5), inchesToMillimeters(15/2), inchesToMillimeters(15/2), false);
    cylinder(inchesToMillimeters(20.3), inchesToMillimeters(1.3/2), inchesToMillimeters(1.3/2), false);
}

module sharedShippingHub() {
    cylinder(inchesToMillimeters(3), inchesToMillimeters(9), inchesToMillimeters(9), false);
    cylinder(inchesToMillimeters(20.2), inchesToMillimeters(1.1/2), inchesToMillimeters(1.1/2), false);
}

module barrier() {
    translate([0, -inchesToMillimeters(2.3), 0]) rotate([0, 90, 0]) cylinder(inchesToMillimeters(113.2), inchesToMillimeters(1.1/2), inchesToMillimeters(1.1/2), true);
    translate([0, inchesToMillimeters(2.3), 0]) rotate([0, 90, 0]) cylinder(inchesToMillimeters(113.2), inchesToMillimeters(1.1/2), inchesToMillimeters(1.1/2), true);
    
    translate([inchesToMillimeters(distanceFromCenter+1.1/2), inchesToMillimeters(2.85), 0]) rotate([-90, 0, 0]) cylinder(inchesToMillimeters(29.8), inchesToMillimeters(1.1/2), inchesToMillimeters(1.1/2), false);
    translate([inchesToMillimeters(distanceFromCenter+5.7-1.1/2), inchesToMillimeters(2.85), 0]) rotate([-90, 0, 0]) cylinder(inchesToMillimeters(29.8), inchesToMillimeters(1.1/2), inchesToMillimeters(1.1/2), false);
    
    translate([-inchesToMillimeters(distanceFromCenter+1.1/2), inchesToMillimeters(2.85), 0]) rotate([-90, 0, 0]) cylinder(inchesToMillimeters(29.8), inchesToMillimeters(1.1/2), inchesToMillimeters(1.1/2), false);
    translate([-inchesToMillimeters(distanceFromCenter+5.7-1.1/2), inchesToMillimeters(2.85), 0]) rotate([-90, 0, 0]) cylinder(inchesToMillimeters(29.8), inchesToMillimeters(1.1/2), inchesToMillimeters(1.1/2), false);
}

module carousel() {
    cylinder(inchesToMillimeters(13.3), inchesToMillimeters(1.31/2), inchesToMillimeters(1.31/2), false);
    translate([0, 0, inchesToMillimeters(13.3-.47)]) cylinder(inchesToMillimeters(.47), inchesToMillimeters(15/2), inchesToMillimeters(15/2), false);
}

module ground() {
    color( "mediumSlateBlue", 0.9 ) translate([-inchesToMillimeters(72), 0, -14]) union() {
        for(i = [0 : 2]) {
            for(j = [0 : 5]) {
                translate([inchesToMillimeters(24*i+.5), inchesToMillimeters(24*j+.5), 0]) cube([plateInner, plateInner, 14], false);
            }
        }
    }
    color( "Crimson", 0.9 ) translate([0, 0, -14]) union() {
        for(i = [0 : 2]) {
            for(j = [0 : 5]) {
                translate([inchesToMillimeters(24*i+.5), inchesToMillimeters(24*j+.5), 0]) cube([plateInner, plateInner, 14], false);
            }
        }
    }
    color( "DarkSlateGrey", 1 ) translate([-inchesToMillimeters(72), 0, -28]) cube([inchesToMillimeters(144), inchesToMillimeters(144), 14], false);
}