// import statments
include <UtilityKit.scad>;

$fn = 100;

handleLength = inchesToMillimeters(4);
thickness = 10;

cube([thickness, handleLength, thickness], false);