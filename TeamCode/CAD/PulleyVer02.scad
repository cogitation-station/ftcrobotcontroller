$fn = 100;

pulley();

function inToMM(in) = 25.4*in;

module pulley(){
    capThickness = 2;
    channelThickness = 5;
    boreRadius = 14/2; // metric spacing (GOBILDA)
    screwHoleRadius = 4/2;
    innerRadius = 35/2;
    outerRadius = 38/2;
    capRadius = 48/2;
    holeDistance = 12; // metric spacing
    //holeDistance = inToMM(.77/2); // english spacing (Actobotics)
    //boreRadius = inToMM(.125);
    echo(inToMM(1/16));
    crimpHoleThickness = 5;
    
    difference(){
        union(){
            // core
            cylinder(channelThickness*2+capThickness*3, outerRadius, outerRadius, center=false);
            // bottom cap
            cylinder(capThickness, capRadius, capRadius, center=false);
            // bottom channel helper
            translate([0, 0, capThickness]) cylinder(channelThickness, capRadius, outerRadius, center=false);
            translate([0, 0, capThickness+channelThickness]) cylinder(channelThickness, outerRadius, capRadius, center=false);
            // middle cap
            translate([0, 0, capThickness+channelThickness*2]) cylinder(capThickness, capRadius, capRadius, center=false);
            // upper channel helper
            translate([0, 0, capThickness*2+channelThickness*2]) cylinder(channelThickness, capRadius, outerRadius, center=false);
            translate([0, 0, capThickness*2+channelThickness*3]) cylinder(channelThickness, outerRadius, capRadius, center=false);

            // top cap
            translate([0, 0, 2*capThickness+4*channelThickness]) cylinder(capThickness, capRadius, capRadius, center=false);

        }
            
        // inner hollow
        translate([0, 0, capThickness]) cylinder(4*channelThickness+3*capThickness+1, innerRadius, innerRadius, center=false);
        // bore hole
        cylinder(channelThickness*3, boreRadius, boreRadius, center=true);
        // screw holes
        translate([0, holeDistance, 0]) cylinder(3*channelThickness, screwHoleRadius, screwHoleRadius, center=true);
        translate([0, -holeDistance, 0]) cylinder(3*channelThickness, screwHoleRadius, screwHoleRadius, center=true);
        translate([holeDistance, 0, 0]) cylinder(3*channelThickness, screwHoleRadius, screwHoleRadius, center=true);
        translate([-holeDistance, 0, 0]) cylinder(3*channelThickness, screwHoleRadius, screwHoleRadius, center=true);
        // side holes
        translate([innerRadius/2, 0, 2*capThickness+channelThickness*3]) rotate([0, 90, 0]) cylinder((outerRadius*2-innerRadius/2), channelThickness/2, channelThickness/2, center=false);
        translate([innerRadius/2, 0, capThickness+channelThickness]) rotate([0, 90, 0]) cylinder((outerRadius*2-innerRadius/2), channelThickness/2, channelThickness/2, center=false);
    }
    
    // cable clamping hole
    translate([innerRadius/sqrt(2)-channelThickness/4, innerRadius/sqrt(2)-channelThickness/4, 4*channelThickness+3*capThickness-crimpHoleThickness/2])
    difference(){
        cylinder(h=crimpHoleThickness, r=channelThickness, center=true);
        cylinder(h=crimpHoleThickness*2, r=channelThickness/2, center=true);
    }
    
    translate([innerRadius/sqrt(2)-channelThickness/4, -innerRadius/sqrt(2)+channelThickness/4, 4*channelThickness+3*capThickness-crimpHoleThickness/2])
    difference(){
        cylinder(h=crimpHoleThickness, r=channelThickness, center=true);
        cylinder(h=crimpHoleThickness*2, r=channelThickness/2, center=true);
    }
}