// import statments
include <UtilityKit.scad>;

$fn = 100;

// editable numbers  10.5 diameter =  5.25 radius
mountingObjectRadius = 50;
pistonRadius = 2.5;
pistonHolderRadius = (pistonRadius+lineThickness*2+2.1);
echo(pistonHolderRadius=pistonHolderRadius);
pistonHolderRadius = 5.3;
standardThickness = 6;
holderRadius = 5;
cageRingCount = 8;
pulleyRadius = 19;
distanceFromMountToRing = PI*pulleyRadius; // half circumfrence of pulley

// non editable numbers
innerRadius = mountingObjectRadius-standardThickness/2;
outerRadius = mountingObjectRadius+standardThickness/2;

dome();

module dome() {
    //ring(100000, pistonRadius+lineThickness, pistonHolderRadius, false); // test piston tube
    
    difference() {
        union() {
            cage();
            //makePrintable(); // comment out this line to remove printing supports
        }
        sphere(innerRadius);
        translate([0, -(outerRadius*2+1)/2, -.01]) cube([outerRadius+1, outerRadius*2+1, distanceFromMountToRing+1], false); // comment this line out to make the object not cut in half
    }
}

module cage() {
    ring(standardThickness, innerRadius, outerRadius, false); //large mount ring
    difference() {
        translate([0, 0, 0]) rotate([0, 0, 0]) union() {
            for(i = [1 : cageRingCount]) {
                rotation = 360/(cageRingCount)*i;
                hull() {
                    rotate([90, 0, rotation-90]) ring(standardThickness, innerRadius, outerRadius, true);
                
                    translate([0, 0, distanceFromMountToRing-standardThickness]) rotate([0, 0, rotation]) union() {
                        translate([-standardThickness/2, 0, 0]) cube([standardThickness, mountingObjectRadius/3, standardThickness], false);
                        translate([0, mountingObjectRadius/3, standardThickness/2]) rotate([0, 90, 0]) cylinder(standardThickness, standardThickness/2, standardThickness/2, true);
                    }
                }
            }
        }
        
        rotate([0, 180, 0]) cylinder(outerRadius*5+1, outerRadius+1, outerRadius+1, false);
        translate([0, 0, distanceFromMountToRing+.001]) cylinder(outerRadius*5+1, outerRadius+1, outerRadius+1, false);
        rotate([0, 0, 0]) cylinder(outerRadius*5+1, pistonHolderRadius, pistonHolderRadius, false);
    }
    // make it thicker around mounting hole
    translate([0, 0, 0 + distanceFromMountToRing-standardThickness]) ring(standardThickness, pistonHolderRadius, pistonHolderRadius+standardThickness, false);
}

module makePrintable() {
    ring(1, pistonHolderRadius+standardThickness/2-.5, pistonHolderRadius+standardThickness/2+.5, false);
    translate([0, 0, distanceFromMountToRing/3]) ring(1, pistonHolderRadius+standardThickness/2-.5, pistonHolderRadius+standardThickness/2+.5, false);
    translate([0, 0, distanceFromMountToRing/3*2]) ring(1, pistonHolderRadius+standardThickness/2-.5, pistonHolderRadius+standardThickness/2+.5, false);
    for(i = [1 : cageRingCount]) {
        rotation = 360/(cageRingCount)*i;
        rotate([0, 0, rotation]) translate([0, pistonHolderRadius+standardThickness/2, 0]) cylinder(distanceFromMountToRing-lineThickness/2-standardThickness, .5, .5, false);
    }
}