// import statments
include <UtilityKit.scad>;

$fn = 100;

standardThickness = 2;

segmentY = 75/4;
pulleyRadius = 19;

pistonRadius = 2.5;

edgeThickness = (segmentY/4-lineThickness*2);
distance = PI*pulleyRadius; // half circumfrence of pulley // also is the distance the piston can extend

pistonShellX = distance/2;
pistonShellInnerRadius = pistonRadius+lineThickness;
pistonShellOuterRadius = pistonRadius+lineThickness+2;

pistonX = pistonShellX+distance+10;

extenderX = 10;

segmentX = distance + extenderX + 30;

pusherRadius = inchesToMillimeters(.6);
pusherThickness = 12;

holderDepth = 15;

hosePegLength = 10;

