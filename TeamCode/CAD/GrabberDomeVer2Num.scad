// import statments
use <ArmVer2.scad>;
use <GrabberVer2.scad>;
include <ArmVer2Num.scad>;
include <GrabberVer2Num.scad>;
include <UtilityKit.scad>;

standardThickness = 7;

middLength = segmentX-segmentZ/3;
hoseHolderRadius = hoseOuterRadius + standardThickness;
pulleyDistance = hoseOuterRadius+standardThickness+hoseInnerRadius*2;
turnPulleyRadius = pulleyDistance/2;
pulleyCoords = hoseHolderRadius-standardThickness-lineThickness;

grabberLength = -middLength-hoseOuterRadius-holderDepth-pulleyDistance-hoseOuterRadius*2;

springRadius = 50;
pistonRadius = 2.5;
holderRadius = 5;
cageRingCount = 12;

innerRadius = springRadius-standardThickness/2;
outerRadius = springRadius+standardThickness/2;

domeDepth = inchesToMillimeters(2.5);

module showGrabber() {
    translate([0, 0, grabberLength]) rotate([0, -90, 0]) grabberVer2();
}