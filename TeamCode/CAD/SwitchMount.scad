// import statments
include <UtilityKit.scad>;

$fn = 100;

switchY = 27;
switchX = 11;

standardThickness = 3;
mountThickness = 4;
borderSize = 10;

build();

module build() {
    translate([switchX/2+borderSize, 0, standardThickness/2]) switchPlate();
    channelMount();
}

module switchPlate() {
    difference() {
        union() {
            cube([switchX+borderSize*2, switchY+borderSize*2, standardThickness], true);
        }
        cube([switchX, switchY, standardThickness+.2], true);
    }
}

module channelMount() {
    translate([0, 0, gridSize*2+standardThickness]) rotate([0, 90, 180]) mount();
    translate([-mountThickness, -(switchY+borderSize*2)/2, 0]) cube([mountThickness, switchY+borderSize*2, standardThickness], false);
}

module mount() {
    difference() {
        union() {
            ring(mountThickness, centerHoleRadius, gridSize*2, false); // ring
            translate([0, -gridSize*2, 0]) cube([gridSize*2, gridSize*4, mountThickness], false);
        }
        translate([0, 0, -.1]) uChannelCutter();
    }
}