// import statments
include <UtilityKit.scad>;

$fn = 100;

thickness = 5;
size = inchesToMillimeters(2.5);

blue();

module blue() {
    cylinder(thickness, size/2, size/2, true);
}

module red() {
    cube([size, size, thickness], true);
}