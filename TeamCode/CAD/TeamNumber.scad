// import statments
include <UtilityKit.scad>;

$fn = 100;

font = "Ebrima:Bold";

sizeX = 300;
sizeY = 80;
sizeZ = 4;

borderSize = 10;

//uChannel(11);

translate([-sizeX/2-11.5, -sizeY/2, sizeZ/2]) color( "Crimson", 0.9 ) resize([sizeX,sizeY,sizeZ]) linear_extrude(height = 5, center = true, convexity = 0, twist = 0, slices = 20, scale = 1.0) text(text = "19895", font=font, size = 80, halign = 50);

translate([-sizeX/2-11.5, -50, sizeZ/2]) color( "Crimson", 0.9 ) resize([0,10,sizeZ], true) linear_extrude(height = 5, center = true, convexity = 0, twist = 0, slices = 20, scale = 1.0) text(text = "Dragomight Industries", font=font, size = 80, halign = 50);

difference() {
    translate([0, 0, -sizeZ/2]) color( "DarkSlateGray", 1)minkowski() {
        cube([sizeX+borderSize+gridSize*3, sizeY+borderSize, sizeZ/2], true);
        cylinder(sizeZ/2, sizeY/8, sizeY/8, true);
    }
    
    translate([gridSize*3*7, gridSize*4, -sizeZ*4]) cylinder(sizeZ*5, smallHoleRadius, smallHoleRadius, false);
    translate([-gridSize*3*7, gridSize*4, -sizeZ*4]) cylinder(sizeZ*5, smallHoleRadius, smallHoleRadius, false);
    translate([gridSize*3*7, -gridSize*4, -sizeZ*4]) cylinder(sizeZ*5, smallHoleRadius, smallHoleRadius, false);
    translate([-gridSize*3*7, -gridSize*4, -sizeZ*4]) cylinder(sizeZ*5, smallHoleRadius, smallHoleRadius, false);
}