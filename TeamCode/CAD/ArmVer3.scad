// import statments
include <ArmVer3Num.scad>;
use <WristVer1.scad>;
include <UtilityKit.scad>;

$fn = 100;

build();

echo(getSegmentY(6));

module build() {
    firstSegment(inchesToMillimeters(segmentLength), 6, segmentZ);
    basicSegment(inchesToMillimeters(segmentLength), 6, segmentZ);
    translate([inchesToMillimeters(segmentLength)-segmentZ, 0, 0]) basicSegment(inchesToMillimeters(segmentLength), 4, segmentZ);
    translate([(inchesToMillimeters(segmentLength)-segmentZ)*2+segmentZ*1.5, 0, 0]) gripperSlot();
}

module gripperSlot() {
    holderY = getHoseHolderY(2);
    thickness = getJointOuterThickness(4);
    segmentY = getAltSegmentY(2);
    translate([thickness, 0, 0]) difference() {
        union() {
            hull() {
                translate([0, holderY/2, -segmentZ/2]) cube([thickness*2, (segmentY-holderY)/2, segmentZ], false);
                translate([-thickness, holderY/2, -segmentZ/2]) cube([thickness, thickness+hoseOuterRadius*2, segmentZ]);
            }
            hull() {
                translate([0, -segmentY/2, -segmentZ/2]) cube([thickness*2, (segmentY-holderY)/2, segmentZ], false);
                translate([-thickness, -segmentY/2+thickness+lineThickness, -segmentZ/2]) cube([thickness, thickness+hoseOuterRadius*2, segmentZ]);
            }
        }
        translate([thickness*1.25, (holderY)/2+thickness+hoseOuterRadius, 0]) cube([thickness*2+2, (segmentY-holderY)/2-standardThickness*2-thickness, segmentZ-standardThickness*2], true);
        translate([thickness*1.25, -((holderY)/2+thickness+hoseOuterRadius), 0]) cube([thickness*2+2, (segmentY-holderY)/2-standardThickness*2-thickness, segmentZ-standardThickness*2], true);
        
        translate([thickness, 0, 0]) rotate([90, 0, 0]) screwHole(segmentY+1, true);
        translate([thickness, 0, segmentZ/3]) rotate([90, 0, 0]) screwHole(segmentY+1, true);
        translate([thickness, 0, -segmentZ/3]) rotate([90, 0, 0]) screwHole(segmentY+1, true);
    }
}

module basicSegment(segmentX, hoseCount, segmentZ) {
    if (hoseCount > 4) {
        innerJoint(segmentZ, hoseCount, getHoseHolderY(hoseCount), getJointOuterThickness(hoseCount), getSegmentY(hoseCount));
        translate([segmentX-segmentZ, 0, 0]) outerJoint(segmentZ, hoseCount-2, getHoseHolderY(hoseCount-2), getJointOuterThickness(hoseCount-2), getSegmentY(hoseCount-2));
        translate([-segmentZ/2, 0, 0]) midSection(segmentX, hoseCount-2, hoseCount, segmentZ);
    }
    
    else if (hoseCount == 4){
        altSegment(segmentX, hoseCount, segmentZ);
    }
    
    else {
        echo("unstable joint count! [pick a bigger number]");
    }
}

module altSegment(segmentX, hoseCount, segmentZ) {
    innerJoint(segmentZ, hoseCount, getHoseHolderY(hoseCount), getJointOuterThickness(hoseCount), getSegmentY(hoseCount));
    translate([-segmentZ/2, 0, 0]) altMidSection(segmentX, hoseCount, segmentZ);
    translate([(segmentX-segmentZ), 0, 0]) innerJoint(segmentZ, hoseCount-2, getHoseHolderY(hoseCount-2), getJointOuterThickness(hoseCount), getAltSegmentY(hoseCount-2));
    translate([segmentX-segmentZ, 0, 0]) outerJoint(segmentZ, hoseCount-2, getHoseHolderY(hoseCount-2), getJointOuterThickness(hoseCount), getAltSegmentY(hoseCount-2));
}

module firstSegment(segmentX, hoseCount, segmentZ) {
    outerJoint(segmentZ, hoseCount, getHoseHolderY(hoseCount), getJointOuterThickness(hoseCount), getSegmentY(hoseCount));
    translate([-segmentX+segmentZ, 0, 0]) firstJoint(segmentX, hoseCount, segmentZ);
}

module firstJoint(segmentX, hoseCount, segmentZ) {
    thickness = getJointOuterThickness(hoseCount);
    jointY = getSegmentY(hoseCount);
    difference() {
        union() {
            rotate([90, 0, 0]) cylinder(axleLength, segmentZ/2, segmentZ/2, true);
            hull() {
                translate([segmentX-segmentZ*2.75, (jointY-thickness)/2, 0]) cube([segmentZ/2, thickness, segmentZ], true);
                translate([segmentZ/4*3, axleLength/2-thickness/2, 0]) cube([segmentZ/2, thickness, segmentZ], true);
            }
            hull() {
                translate([segmentX-segmentZ*2.75, -(jointY-thickness)/2, 0]) cube([segmentZ/2, thickness, segmentZ], true);
                translate([segmentZ/4*3, -axleLength/2+thickness/2, 0]) cube([segmentZ/2, thickness, segmentZ], true);
            }
            translate([segmentZ/4, axleLength/2-thickness/2, 0]) cube([segmentZ/2, thickness, segmentZ], true);
            translate([segmentZ/4, -axleLength/2+thickness/2, 0]) cube([segmentZ/2, thickness, segmentZ], true);
        }
        rexAxle();
        
        translate([0, -(axleLength-thickness*2)/4, 0]) union() {
            for(j = [0 : 1]) {
                for(i = [0 : hoseCount/2]) {
                    translate([0, i*((axleLength-thickness*2)/hoseCount), -segmentZ/3+segmentZ/3*2*j]) rotate([0, -90, 0]) cylinder(segmentZ, hoseHoleRadius, hoseHoleRadius, true);
                }
            }
        }
    }
}

module rexAxle() { // rex axle
    rotate([90, 0, 0]) union() {
        linear_extrude(height = axleLength+1, center = true, convexity = 10, 0, slices = 20, scale = 1.0, $fn = 16) circle(r=4.25, $fn=6);//the actual axle is 4mm but we add .25 to ensure the axle will fit
        rotate([0, 0, 45]) union() {
            translate([-gridSize, gridSize, 0]) cylinder(axleLength+1, smallHoleRadius*1.25, smallHoleRadius*1.25, true);
            translate([gridSize, -gridSize, 0]) cylinder(axleLength+1, smallHoleRadius*1.25, smallHoleRadius*1.25, true);
        }
    }
}

module midSection(segmentX, loHoseCount, hiHoseCount, segmentZ) {
    hiThickness = getJointOuterThickness(hiHoseCount);
    loThickness = getJointOuterThickness(loHoseCount);
    jointY = getSegmentY(loHoseCount);
    holderY = getHoseHolderY(hiHoseCount);
    
    translate([segmentZ*2, 0, 0]) hull() {
        translate([-segmentZ/4, holderY/2+(hiThickness+hoseOuterRadius*2)/2, 0]) cube([segmentZ/2, hiThickness+hoseOuterRadius*2, segmentZ], true);
        translate([segmentX-segmentZ*4-segmentZ/4, jointY/2-loThickness/2, 0]) cube([segmentZ/2, loThickness, segmentZ], true);
    }
    
    translate([segmentZ*2, 0, 0]) hull() {
        translate([-segmentZ/4, -holderY/2-(hiThickness+hoseOuterRadius*2)/2, 0]) cube([segmentZ/2, hiThickness+hoseOuterRadius*2, segmentZ], true);
        translate([segmentX-segmentZ*4-segmentZ/4, -jointY/2+loThickness/2, 0]) cube([segmentZ/2, loThickness, segmentZ], true);
    }
}

module altMidSection(segmentX, hoseCount, segmentZ) {
    thickness = getJointOuterThickness(hoseCount);
    jointY = getAltSegmentY(hoseCount-2);
    holderY = getHoseHolderY(hoseCount);
    
    translate([segmentZ*2, 0, 0]) hull() {
        translate([-segmentZ/4, holderY/2+(thickness+hoseOuterRadius*2)/2, 0]) cube([segmentZ/2, thickness+hoseOuterRadius*2, segmentZ], true);
        translate([segmentX-segmentZ*4-segmentZ/4, jointY/2-thickness/2, 0]) cube([segmentZ/2, thickness, segmentZ], true);
    }
    
    translate([segmentZ*2, 0, 0]) hull() {
        translate([-segmentZ/4, -holderY/2-(thickness+hoseOuterRadius*2)/2, 0]) cube([segmentZ/2, thickness+hoseOuterRadius*2, segmentZ], true);
        translate([segmentX-segmentZ*4-segmentZ/4, -jointY/2+thickness/2, 0]) cube([segmentZ/2, thickness, segmentZ], true);
    }
}

module innerJoint(jointZ, hoseCount, holderY, thickness, jointY) {
    union() {
        difference() {
            union() {
                translate([0, holderY/2, 0]) rotate([-90, 0, 0]) cylinder(thickness+hoseOuterRadius*2, jointZ/2, jointZ/2, false); // core y+
                translate([0, -holderY/2, 0]) rotate([90, 0, 0]) cylinder(thickness+hoseOuterRadius*2, jointZ/2, jointZ/2, false); // core y-
                
                translate([jointZ/2, holderY/2+thickness/4, 0]) cube([jointZ, thickness/2, jointZ], true); // conector y+ a
                translate([jointZ/2, holderY/2+thickness/4*3+hoseOuterRadius*2, 0]) cube([jointZ, thickness/2, jointZ], true); // conector y+ b
                
                translate([jointZ/2, -holderY/2-thickness/4, 0]) cube([jointZ, thickness/2, jointZ], true); // conector y- a
                translate([jointZ/2, -(holderY/2+thickness/4*3+hoseOuterRadius*2), 0]) cube([jointZ, thickness/2, jointZ], true); // conector y- b
                
                translate([0, holderY/2+thickness, 0]) rotate([-90, 0, 0]) cylinder((jointY-holderY)/2-thickness-1, jointZ/4, jointZ/4, false); // axle y+
                translate([0, -holderY/2-thickness, 0]) rotate([90, 0, 0]) cylinder((jointY-holderY)/2-thickness, jointZ/4, jointZ/4, false); // axle y-
            }
            hull() {
                translate([PAIN(jointZ/2), holderY/2+(thickness+hoseOuterRadius*2)/2, PAIN(jointZ/2)]) sphere(hoseInnerRadius);
                translate([-PAIN(jointZ/2), holderY/2+thickness/4, PAIN(jointZ/2)]) sphere(hoseInnerRadius);
            }
            hull() {
                translate([PAIN(jointZ/2), -holderY/2-(thickness+hoseOuterRadius*2)/2, -PAIN(jointZ/2)]) sphere(hoseInnerRadius);
                translate([-PAIN(jointZ/2), -holderY/2-thickness/4, -PAIN(jointZ/2)]) sphere(hoseInnerRadius);
            }
            
            translate([0, jointY/2-moniterLength+.1, 0]) rotate([90, 0, 0]) cylinder(knobLength+.1, knobRadius, knobRadius); // knob hole
            translate([0, jointY/2, 0]) rotate([90, 0, 0]) cylinder(moniterLength, moniterWidth/2, moniterWidth/2); // moniter hole
        }
            
        translate([jointZ*1.25, jointY/2-thickness-(thickness+hoseOuterRadius*2)/2-lineThickness, 0]) cube([jointZ/2, thickness+hoseOuterRadius*2, jointZ], true);
        translate([jointZ*1.25, -(jointY/2-thickness-(thickness+hoseOuterRadius*2)/2)+lineThickness, 0]) cube([jointZ/2, thickness+hoseOuterRadius*2, jointZ], true);
        
        
        translate([-jointZ/4+jointZ+SUFFERING(jointZ/2), 0, 0]) difference() { // HOSE HOLDER
            translate([jointZ/4, 0, 0]) rotate([90, 0, 0]) cylinder(holderY+thickness*2+hoseOuterRadius*4, jointZ/4, jointZ/4, true);
            translate([0, -holderY/2+hoseHoleRadius+standardThickness, 0]) union() {
                for(i = [0 : hoseCount-1]) {
                    translate([0, i*(hoseHoleRadius*2+standardThickness), 0]) union() {
                        translate([jointZ/4, 0, 0]) rotate([0, 90, 0]) cylinder(jointZ/4+1, hoseHoleRadius, hoseHoleRadius, false);
                        rotate([0, 90, 0]) cylinder(jointZ/4+1, hoseInnerRadius, hoseInnerRadius, false);
                    }
                }
            }
        }
    }
}

module outerJoint(jointZ, hoseCount, holderY, thickness, jointY) {
    difference() {
        union() {
            difference() {
                rotate([90, 0, 0]) cylinder(jointY, jointZ/2, jointZ/2, true); // core
                rotate([90, 0, 0]) cylinder(holderY+thickness*2+lineThickness*2+hoseOuterRadius*4, jointZ/2+1, jointZ/2+1, true); // core of inner
            }
            translate([-jointZ*1.5/2, jointY/2-thickness/2, 0]) cube([jointZ*1.5, thickness, jointZ], true); // conector x+
            translate([-jointZ*1.5/2, -jointY/2+thickness/2, 0]) cube([jointZ*1.5, thickness, jointZ], true); // conector x-
        }
        rotate([90, 0, 0]) cylinder(jointY+1, jointZ/4+lineThickness, jointZ/4+lineThickness, true); // axle
    }
    rotate([0, 180, 0]) difference() { // HOSE HOLDER
        union() {
            hull() {
                translate([jointZ/4, 0, 0]) rotate([90, 0, 0]) cylinder(holderY-lineThickness*2, jointZ/4, jointZ/4, true); // core x+
                translate([jointZ*1.25, 0, 0]) rotate([90, 0, 0]) cylinder(holderY-lineThickness*2, jointZ/4, jointZ/4, true); // core x-
            }
            translate([jointZ*1.25, 0, 0]) rotate([90, 0, 0]) cylinder(jointY, jointZ/4, jointZ/4, true); // core xl
        }
        
        translate([jointZ-1, holderY/2+(thickness+hoseOuterRadius*2)/2, 0]) union() {
            translate([jointZ/4, 0, 0]) rotate([0, 90, 0]) cylinder(jointZ*20+1, hoseHoleRadius, hoseHoleRadius, false);
            rotate([0, 90, 0]) cylinder(jointZ/4+1, hoseInnerRadius, hoseInnerRadius, false);
        }
        
        translate([jointZ-1, -holderY/2-(thickness+hoseOuterRadius*2)/2, 0]) union() {
            translate([jointZ/4, 0, 0]) rotate([0, 90, 0]) cylinder(jointZ*20+1, hoseHoleRadius, hoseHoleRadius, false);
            rotate([0, 90, 0]) cylinder(jointZ/4+1, hoseInnerRadius, hoseInnerRadius, false);
        }
        
        translate([0, -holderY/2+hoseHoleRadius+standardThickness, 0]) union() {
            for(i = [0 : hoseCount-1]) {
                translate([0, i*(hoseHoleRadius*2+standardThickness), 0]) union() {
                    translate([jointZ/4, 0, 0]) rotate([0, 90, 0]) cylinder(jointZ*20+1, hoseHoleRadius, hoseHoleRadius, false);
                    rotate([0, 90, 0]) cylinder(jointZ/4+1, hoseInnerRadius, hoseInnerRadius, false);
                }
            }
        }
    }
}