$fn = 100;

pulley();

module pulley(){
    capThickness = 2;
    channelThickness = 5;
    boreRadius = 14/2;
    screwHoleRadius = 4/2;
    innerRadius = 35/2;
    outerRadius = 38/2;
    capRadius = 48/2;
    holeDistance = 12;
    crankOffset = 19;
    
    difference(){
        union(){
            // core
            cylinder(channelThickness*2+capThickness*3, outerRadius, outerRadius, center=false);
            // bottom cap
            cylinder(capThickness, capRadius, capRadius, center=false);
            // bottom channel helper
            translate([0, 0, capThickness]) cylinder(channelThickness, capRadius, outerRadius, center=false);
            translate([0, 0, capThickness+channelThickness]) cylinder(channelThickness, outerRadius, capRadius, center=false);
            // middle cap
            translate([0, 0, capThickness+channelThickness*2]) cylinder(capThickness, capRadius, capRadius, center=false);
            // upper channel helper
            translate([0, 0, capThickness*2+channelThickness*2]) cylinder(channelThickness, capRadius, outerRadius, center=false);
            translate([0, 0, capThickness*2+channelThickness*3]) cylinder(channelThickness, outerRadius, capRadius, center=false);

            // top cap
            translate([0, 0, 2*capThickness+4*channelThickness]) cylinder(capThickness, capRadius, capRadius, center=false);
            // axle
            translate([0, 0, -channelThickness]) cylinder(channelThickness, boreRadius, boreRadius, center=false);
            // crank
            translate([0, crankOffset, 3*capThickness+4*channelThickness]) cylinder(channelThickness*3, boreRadius, boreRadius, center=true);
            
           

        }
            
        // inner hollow
        translate([0, 0, capThickness]) cylinder(4*channelThickness+capThickness, innerRadius, innerRadius, center=false);
 
        // side holes
        translate([innerRadius/2, 0, 2*capThickness+channelThickness*3]) rotate([0, 90, 0]) cylinder((outerRadius*2-innerRadius/2), channelThickness/2, channelThickness/2, center=false);
        translate([innerRadius/2, 0, capThickness+channelThickness]) rotate([0, 90, 0]) cylinder((outerRadius*2-innerRadius/2), channelThickness/2, channelThickness/2, center=false);
    }
}