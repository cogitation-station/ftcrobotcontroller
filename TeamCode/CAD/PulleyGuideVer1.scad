// import statments
include <PulleyGuideVer1Num.scad>;
include <UtilityKit.scad>;
use <PulleyVer1.scad>;

$fn = 100;

testFit();

module testFit() {
    translate([-pulleyDistanceFromUChannel-48/2, -48*4, totalThickness+2.5]) rotate([0, 180, 0]) uChannel(18);
    translate([0, 0, standardThickness+lineThickness+pulleyThickness]) rotate([180, 0, 0]) pulley();
    case();
}

module case() {
    difference() {
        union() {
            cylinder(totalThickness, totalRadius, totalRadius, false); // central cyilnder
            translate([-mountX, -totalRadius, totalThickness-standardThickness]) cube([mountX, totalRadius*2, standardThickness], false); // mount plate
            translate([0, totalRadius-standardThickness, totalThickness]) rotate([0, 180, 0]) rTriangle(standardThickness, mountX, totalThickness);
            translate([0, -totalRadius, totalThickness]) rotate([0, 180, 0]) rTriangle(standardThickness, mountX, totalThickness);
        }
        translate([0, 0, standardThickness]) cylinder(pulleyThickness+lineThickness*2, pulleyRadius+gapSize, pulleyRadius+gapSize, false); // pulley hole
        translate([0, -pulleyRadius*1.5, -.5])cube([pulleyRadius*2, pulleyRadius*3, totalThickness+1], false); // CUT IT IN HALF
        translate([0, 0, totalThickness-standardThickness*2]) cylinder(totalThickness/3, pulleyMountRadius+2, pulleyMountRadius+2, false); // pulleyMountHole
        translate([-mountX+24, 0, totalThickness-standardThickness*2]) moreHoleChannelCutter(standardThickness*3); // screw holes
    }
}