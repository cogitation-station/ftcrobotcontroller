// import statments
include <ArmVer2Num.scad>;
use <GrabberVer2.scad>;
use <WristVer1.scad>;
include <UtilityKit.scad>;

$fn = 100;

build(45, -90, 45);

module build(shoulderAngle = 0, elbowAngle = 0, grabberAngle = 0) {
    rotate([0, shoulderAngle ,0]) union() {
    color( "PowderBlue", 1 ) translate([0, 0, -segmentZ/2]) upperSegment();
        translate([upSegmentX-segmentZ, 0, 0]) rotate([0, elbowAngle, 0]) translate([0, 0, -segmentZ/2])union() {
            color( "LightSteelBlue", 1 ) lowerSegment();
            translate([lowSegmentX-segmentZ, 0, segmentZ/2]) rotate([0, grabberAngle, 0]) translate([segmentZ/2, 0, 0]) union() {
                color( "SteelBlue", 1 ) grabberVer2();
                color( "SteelBlue", 1 ) translate([hoseOuterRadius*2, 0, 0]) wristCableOffload();
            }
        }
    }
}

module upperSegment() {
    hoseHoleRadius = hoseOuterRadius+lineThickness/2;
    cubeX = upSegmentX-((segmentZ+hoseOuterRadius*2+hoseHolderDepth)+(segmentZ+hoseOuterRadius*2));
    holePos = segmentZ/2+hoseOuterRadius*2+cubeX/3*2+wallThickness;
    
    difference() {
        union() {
            translate([segmentZ/2+hoseOuterRadius*2, -upSegmentY/2, 0]) cube([cubeX, wallThickness, segmentZ], false);
            translate([segmentZ/2+hoseOuterRadius*2, upSegmentY/2-wallThickness, 0]) cube([cubeX, wallThickness, segmentZ], false);
        }
        //+y
        translate([holePos, (upSegmentY/2-wallThickness/2), segmentZ/2]) rotate([0, 90, 22.5]) cylinder(wallThickness*5, hoseHoleRadius, hoseHoleRadius, true); // hose hole 1 +y
        translate([holePos-wallThickness*4, (upSegmentY/2-wallThickness/2), segmentZ/2]) rotate([0, 90, 22.5]) cylinder(wallThickness*5, hoseHoleRadius, hoseHoleRadius, true); // hose hole 2 +y
        
        //-y
        translate([holePos, -(upSegmentY/2-wallThickness/2), segmentZ/2]) rotate([0, 90, -22.5]) cylinder(wallThickness*5, hoseHoleRadius, hoseHoleRadius, true); // hose hole 1 -y
        translate([holePos-wallThickness*4, -(upSegmentY/2-wallThickness/2), segmentZ/2]) rotate([0, 90, -22.5]) cylinder(wallThickness*5, hoseHoleRadius, hoseHoleRadius, true); // hose hole 2 -y
    }
    
    translate([0, 0, segmentZ/2]) upperAxleJoint(upSegmentY, segmentZ); //upper joint
    
    //positive y hose holders
    translate([segmentZ/2+hoseOuterRadius*2+cubeX-hoseHolderDepth/2+hoseHolderDepth+hoseOuterRadius*2+segmentZ/2, upSegmentY/2+boxY/2, segmentZ/2]) rotate([90, 180, 0]) holderBox(); // hoseHolder 1 +y
    translate([segmentZ/2+hoseOuterRadius*2+cubeX-hoseHolderDepth/2+hoseHolderDepth+hoseOuterRadius*2+segmentZ/2, upSegmentY/2+boxY/2+standardThickness/2+hoseOuterRadius*4, segmentZ/2]) rotate([90, 180, 0]) holderBox(); // hoseHolder 2 +y
    
    //negetive y hose holders
    translate([segmentZ/2+hoseOuterRadius*2+cubeX-hoseHolderDepth/2+hoseHolderDepth+hoseOuterRadius*2+segmentZ/2, -(upSegmentY/2+boxY/2), segmentZ/2]) rotate([-90, 180, 0]) holderBox(); // hoseHolder 1 y
    translate([segmentZ/2+hoseOuterRadius*2+cubeX-hoseHolderDepth/2+hoseHolderDepth+hoseOuterRadius*2+segmentZ/2, -(upSegmentY/2+boxY/2+standardThickness/2+hoseOuterRadius*4), segmentZ/2]) rotate([-90, 180, 0]) holderBox(); // hoseHolder 2 -y
    
    translate([segmentZ/2+hoseOuterRadius*2+cubeX-hoseHolderDepth/2+hoseHolderDepth+hoseOuterRadius*2+segmentZ/2, upSegmentY/2+boxY/2, segmentZ/2]) holderBoxSupport(2);
    
    translate([segmentZ/2+hoseOuterRadius*2+cubeX-hoseHolderDepth/2+hoseHolderDepth+hoseOuterRadius*2+segmentZ/2, -(upSegmentY/2+boxY/2), segmentZ/2]) rotate([180, 0, 0]) holderBoxSupport(2);
    
    translate([segmentZ/2+hoseOuterRadius*2+cubeX, 0, segmentZ/2]) lowerPreJoint(upSegmentY, segmentZ); // lower joint
}

module lowerSegment() {
    hoseHoleRadius = hoseOuterRadius+lineThickness/2;
    cubeX = lowSegmentX-((segmentZ+hoseOuterRadius*2+hoseHolderDepth)+(segmentZ+hoseOuterRadius*2));
    holePos = segmentZ/2+hoseOuterRadius*2+cubeX/3*2+wallThickness;
    
    difference() {
        union() {
            translate([segmentZ/2+hoseOuterRadius*2, -lowSegmentY/2, 0]) cube([cubeX, wallThickness/2, segmentZ], false);
            translate([segmentZ/2+hoseOuterRadius*2, lowSegmentY/2-wallThickness/2, 0]) cube([cubeX, wallThickness/2, segmentZ], false);
        }
        //+y
        translate([holePos-wallThickness, (lowSegmentY/2-wallThickness/2), segmentZ/2]) rotate([0, 90, -22.5]) cylinder(wallThickness*5, hoseHoleRadius, hoseHoleRadius, true); // hose hole +y
        
        //-y
        translate([holePos-wallThickness, -(lowSegmentY/2-wallThickness/2), segmentZ/2]) rotate([0, 90, 22.5]) cylinder(wallThickness*5, hoseHoleRadius, hoseHoleRadius, true); // hose hole -y
    }
    
    translate([0, 0, segmentZ/2]) upperPreJoint(lowSegmentY, segmentZ); //upper joint
    
    translate([segmentZ/2+hoseOuterRadius*2+cubeX, 0, segmentZ/2]) lowerPreJoint(lowSegmentY, segmentZ); // lower joint
    
    translate([segmentZ/2+hoseOuterRadius*2+cubeX-hoseHolderDepth/2+hoseHolderDepth+hoseOuterRadius*2+segmentZ/2, lowSegmentY/2+boxY/2, segmentZ/2]) holderBoxSupport();
    
    translate([segmentZ/2+hoseOuterRadius*2+cubeX-hoseHolderDepth/2+hoseHolderDepth+hoseOuterRadius*2+segmentZ/2, -(lowSegmentY/2+boxY/2), segmentZ/2]) rotate([180, 0, 0]) holderBoxSupport();
    
    translate([segmentZ/2+hoseOuterRadius*2+cubeX-hoseHolderDepth/2+hoseHolderDepth+hoseOuterRadius*2+segmentZ/2, lowSegmentY/2+boxY/2, segmentZ/2]) rotate([90, 180, 0]) holderBox(); // hoseHolder  +y
    
    translate([segmentZ/2+hoseOuterRadius*2+cubeX-hoseHolderDepth/2+hoseHolderDepth+hoseOuterRadius*2+segmentZ/2, -(lowSegmentY/2+boxY/2), segmentZ/2]) rotate([-90, 180, 0]) holderBox(); // hoseHolder -y
    
    translate([segmentZ/2+lineThickness*2+hoseHolderDepth/2, 0, segmentZ/2]) elbowCableOffload();
}

module holderBoxSupport(holderBoxCount = 1) {
    for (i = [0 : 1]) {
        translate([0, 0, (segmentZ/3)/2-(segmentZ/3)*i]) rotate([0, 180*i, 0]) difference() {
            hull() {
                translate([0, -hoseOuterRadius-standardThickness/2, segmentZ/6]) cube([hoseHolderDepth, 1, segmentZ/3], true);
                translate([0, -standardThickness/2+hoseOuterRadius*1+hoseOuterRadius*2*(holderBoxCount-1), 0]) cube([hoseHolderDepth, (standardThickness+hoseOuterRadius*4)*holderBoxCount, 1], true);
            }
            hull() {
                translate([0, -hoseOuterRadius-standardThickness/2, segmentZ/6]) cube([hoseHolderDepth-standardThickness*2, 1.1, segmentZ/3+.1], true);
                translate([0, -standardThickness/2+hoseOuterRadius*1+hoseOuterRadius*2*(holderBoxCount-1), 0]) cube([hoseHolderDepth-standardThickness*2, (standardThickness+hoseOuterRadius*4)*holderBoxCount+.1, 1.1], true);
            }
        }
    }
}

module elbowCableOffload() {
    difference() {
        union() {
            translate([0, lowSegmentY/2+upSegmentY/8, 0]) cube([hoseHolderDepth, upSegmentY/4, segmentZ], true);
            translate([0, -(lowSegmentY/2+upSegmentY/8), 0]) cube([hoseHolderDepth, upSegmentY/4, segmentZ], true);
        }
        cube([hoseHolderDepth-standardThickness*2, 100, 100], true);
    }
    
    //positive y hose holders
    translate([0, upSegmentY/2+boxY/2, 0]) rotate([90, 0, 0]) holderBox(); // hoseHolder 1 +y
    translate([0, upSegmentY/2+boxY/2+standardThickness/2+hoseOuterRadius*4, 0]) rotate([90, 0, 0]) holderBox(); // hoseHolder 2 +y
    
    translate([0, upSegmentY/2+boxY/2, 0]) holderBoxSupport(2);
    
    //negetive y hose holders
    translate([0, -(upSegmentY/2+boxY/2), 0]) rotate([-90, 0, 0]) holderBox(); // hoseHolder 1 y
    translate([0, -(upSegmentY/2+boxY/2+standardThickness/2+hoseOuterRadius*4), 0]) rotate([-90, 0, 0]) holderBox(); // hoseHolder 2 -y
    
    translate([0, -(upSegmentY/2+boxY/2), 0]) rotate([180, 0, 0]) holderBoxSupport(2);
}

module wristCableOffload() {
    difference() {
        union() {
            translate([0, upSegmentY/8+lowSegmentY/8, 0]) cube([hoseHolderDepth, lowSegmentY/4, segmentZ], true);
            translate([0, -(upSegmentY/8+lowSegmentY/8), 0]) cube([hoseHolderDepth, lowSegmentY/4, segmentZ], true);
        }
        cube([hoseHolderDepth-standardThickness*2, 100, 100], true);
    }
    
    //positive y hose holders
    translate([0, lowSegmentY/2+boxY/2, 0]) rotate([90, 0, 0]) holderBox(); // hoseHolder +y
    
    translate([0, lowSegmentY/2+boxY/2, 0]) holderBoxSupport();
    
    //negetive y hose holders
    translate([0, -(lowSegmentY/2+boxY/2), 0]) rotate([-90, 0, 0]) holderBox(); // hoseHolder y
    
    translate([0, -(lowSegmentY/2+boxY/2), 0]) rotate([180, 0, 0]) holderBoxSupport();
}

module upperAxleJoint(segmentY = 0, segmentZ = 0) { // length = segmentZ+hoseOuterRadius*2
        edgeThickness = (segmentY/4-lineThickness*2);
    cubeX = segmentZ/2+hoseOuterRadius*2;
    
    difference() {
        translate([0, segmentY/2 ,0]) rotate([90, 180, 0]) difference() {
            union() {
                cylinder(segmentY, segmentZ/2, segmentZ/2, false);
                translate([-cubeX, -segmentZ/2, 0]) cube([cubeX, segmentZ, edgeThickness], false);
                translate([-cubeX, -segmentZ/2, segmentY-edgeThickness]) cube([cubeX, segmentZ, edgeThickness], false);
            }
        }
        translate([0, 0, -.1]) rexAxle();
        
        hoseHoleRadius = hoseOuterRadius+lineThickness/2;
        
        //middle hose holes
        translate([0, 0, segmentZ/3]) rotate([0, 90, 0]) cylinder(segmentZ*10.5, hoseHoleRadius, hoseHoleRadius, true);
        translate([0, 0, -segmentZ/3]) rotate([0, 90, 0]) cylinder(segmentZ*10.5, hoseHoleRadius, hoseHoleRadius, true);
        
        //-y hose holes
        translate([0, -hoseHoleRadius*4, segmentZ/3]) rotate([0, 90, 0]) cylinder(segmentZ*10.5, hoseHoleRadius, hoseHoleRadius, true);
        translate([0, -hoseHoleRadius*4, -segmentZ/3]) rotate([0, 90, 0]) cylinder(segmentZ*10.5, hoseHoleRadius, hoseHoleRadius, true);
        
        //+y hose holes
        translate([0, hoseHoleRadius*4, segmentZ/3]) rotate([0, 90, 0]) cylinder(segmentZ*10.5, hoseHoleRadius, hoseHoleRadius, true);
        translate([0, hoseHoleRadius*4, -segmentZ/3]) rotate([0, 90, 0]) cylinder(segmentZ*10.5, hoseHoleRadius, hoseHoleRadius, true);
    }
}

module upperPreJoint(segmentY = 0, segmentZ = 0) { // length = segmentZ+hoseOuterRadius*2
    edgeThickness = (segmentY/4-lineThickness*2);
    cubeX = segmentZ/2+hoseOuterRadius*2;
    
    difference() {
        translate([0, segmentY/2 ,0]) rotate([90, 180, 0]) difference() {
            union() {
                cylinder(segmentY, segmentZ/2, segmentZ/2, false);
                translate([-cubeX, -segmentZ/2, 0]) cube([cubeX, segmentZ, edgeThickness], false);
                translate([-cubeX, -segmentZ/2, segmentY-edgeThickness]) cube([cubeX, segmentZ, edgeThickness], false);
            }
            translate([0, 0, -.1]) cylinder(segmentY+.2, segmentZ/4+lineThickness, segmentZ/4+lineThickness);
        }
        translate([0, 0, segmentZ/3]) rotate([0, 90, 0]) cylinder(segmentZ*10.5, hoseInnerRadius, hoseInnerRadius, true);
        translate([0, 0, -segmentZ/3]) rotate([0, 90, 0]) cylinder(segmentZ*10.5, hoseInnerRadius, hoseInnerRadius, true);
        translate([-(sqrt(segmentZ/4*segmentZ/4)+hoseOuterRadius/4), 0, segmentZ/3+1]) cube([crimpX+hoseOuterRadius/2, crimpY, crimpZ], true);
        translate([-(sqrt(segmentZ/4*segmentZ/4)+hoseOuterRadius/4), 0, -segmentZ/3-1]) cube([crimpX+hoseOuterRadius/2, crimpY, crimpZ], true);
    }
}

module lowerPreJoint(segmentY = 0, segmentZ = 0) { // length = segmentZ+hoseOuterRadius*2+hoseHolderDepth;
    edgeThickness = (segmentY/4-lineThickness*2);
    cubeX = segmentZ/2+hoseOuterRadius*2;
    
    hosePeg();
    
    difference() {
        translate([(segmentZ/2+hoseOuterRadius*2+hoseHolderDepth), 0, 0]) union() {
            translate([0, segmentY/2, 0]) rotate([90, 0, 0]) union() {
                cylinder(segmentY, segmentZ/4-lineThickness*2, segmentZ/4-lineThickness*2);
                cylinder(edgeThickness, segmentZ/2, segmentZ/2);
                translate([0, 0, segmentY-edgeThickness]) cylinder(edgeThickness, segmentZ/2, segmentZ/2);
            }
    
            translate([-cubeX, -segmentY/2, -segmentZ/2]) cube([cubeX, edgeThickness, segmentZ], false);
            translate([-cubeX, segmentY/2-edgeThickness, -segmentZ/2]) cube([cubeX, edgeThickness, segmentZ], false);
            translate([-hoseHolderDepth/2-cubeX, 0, 0]) cube([hoseHolderDepth, segmentY, segmentZ], true);
        }
        hoseHoleCutter();
    }
}

module rexAxle() { // rex axle
    rotate([90, 0, 0]) union() {
        linear_extrude(height = upSegmentY+1, center = true, convexity = 10, 0, slices = 20, scale = 1.0, $fn = 16) circle(r=4.25, $fn=6);//the actual axle is 4mm but we add .25 to ensure the axle will fit
        rotate([0, 0, 45]) union() {
            translate([-gridSize, gridSize, 0]) cylinder(upSegmentY+1, smallHoleRadius*1.25, smallHoleRadius*1.25, true);
            translate([gridSize, -gridSize, 0]) cylinder(upSegmentY+1, smallHoleRadius*1.25, smallHoleRadius*1.25, true);
        }
    }
}

module hoseHoleCutter() {
    for(i = [0 : 1]) {
        rotate([180*i, 0, 0]) translate([0, 0, segmentZ/3]) union() {
            translate([-.1, 0, 0]) rotate([0, 90, 0]) cylinder(hoseHolderDepth+.2, hoseInnerRadius, hoseInnerRadius);
            translate([-.1, 0, 0]) rotate([0, 90, 0]) cylinder(hoseHolderDepth+.1-hoseHolderDepth/5, hoseOuterRadius, hoseOuterRadius);
        }
    }
}

module hosePeg() {
    for(i = [0 : 1]) {
        rotate([180*i, 0, 0]) translate([0, hoseOuterRadius*2, segmentZ/3]) rotate([0, -90, 0]) cylinder(hosePegLength, hoseOuterRadius, hoseOuterRadius);
    }
}

module holderBox() {
    difference() {
        union() {
            cube([hoseHolderDepth, boxZ, boxY], true); // base object
            translate([0, 0, -boxZ/2]) cube([hoseHolderDepth, boxZ, hoseOuterRadius*2], true);// more base object
            translate([hoseHolderDepth/2, 0, -hoseOuterRadius*2-lineThickness]) rotate([0, 90, 0]) cylinder(hoseHolderDepth, hoseOuterRadius, hoseOuterRadius, false);
        }
        translate([-hoseHolderDepth/2+standardThickness, 0, 0]) rotate([0, 90, 0]) cylinder(hoseHolderDepth+2, hoseOuterRadius+lineThickness, hoseOuterRadius+lineThickness, false);
        rotate([0, 90, 0]) cylinder(hoseHolderDepth+2, hoseInnerRadius, hoseInnerRadius, true);
    }
}