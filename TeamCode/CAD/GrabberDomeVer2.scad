// import statments
use <ArmVer2.scad>;
use <GrabberVer2.scad>;
include <ArmVer2Num.scad>;
include <GrabberVer2Num.scad>;
include <GrabberDomeVer2Num.scad>;
include <UtilityKit.scad>;

$fn = 300;

//showGrabber();
//grabberDomeVer2();
halfDome();

module halfDome() {
    difference() {
        grabberDomeVer2();
        translate([0, -springRadius-standardThickness*1.5, -domeDepth/2]) cube([springRadius+standardThickness*2, springRadius*2+standardThickness*3, domeDepth*2], false);
    }
    
    translate([3, 0, 0]) difference() {
        grabberDomeVer2();
        translate([-springRadius-standardThickness*2, -springRadius-standardThickness*1.5, -domeDepth/2]) cube([springRadius+standardThickness*2, springRadius*2+standardThickness*3, domeDepth*2], false);
    }
}

module grabberDomeVer2() {
    difference() {
        union() {
            cage();
            neck();
        }
        rotate([0, 0, 180]) translate([0, (springRadius-segmentY)/2-springRadius+standardThickness/4, domeDepth/4+standardThickness/4-.1]) cube([standardThickness+.1, springRadius-standardThickness/2-segmentY, domeDepth/2+.1+standardThickness/2], true);
    }
}

module neck() { // the part that connects to the grabber
    hull() {
        translate([0, segmentY+standardThickness*1.5, standardThickness/2]) cube([standardThickness*2, standardThickness*2, standardThickness], true);
        translate([0, springRadius, -standardThickness/2+domeDepth/2]) cube([standardThickness*2, standardThickness, standardThickness], true);
    }
    
    difference() {
        hull() {
            cylinder(standardThickness, segmentY+standardThickness, segmentY+standardThickness, false);
            cylinder(standardThickness+2, segmentY-standardThickness, segmentY-standardThickness, false);
        }
        hull() {
            translate([0, 0, -pistonShellX]) cylinder(pistonShellX*2, pistonShellOuterRadius+.1, pistonShellOuterRadius+.1, false);
            translate([0, segmentY/2, -pistonShellX]) cylinder(pistonShellX*2, pistonShellOuterRadius+.1, pistonShellOuterRadius+.1, false);
        }
    }
}

module cage() { // the thing the goes from the neck to where the spring will go
    translate([0, 0, domeDepth-standardThickness]) ring(standardThickness, springRadius-standardThickness/2, springRadius+standardThickness/2, false);
    
    translate([0, 0, standardThickness/2]) union() {
        for(i = [1 : cageRingCount]) {
            rotation = 360/(cageRingCount)*i;
            hull() {
                rotate([0, 0, rotation]) translate([segmentY+standardThickness*1.5, 0, 0]) cube([standardThickness*2, standardThickness, standardThickness], true);
                rotate([0, 0, rotation]) translate([springRadius, 0, (domeDepth-standardThickness*1.5)/2]) cube([standardThickness, standardThickness, standardThickness], true);
            }
            rotate([0, 0, rotation]) translate([springRadius, 0, (domeDepth-standardThickness/2)-domeDepth/4]) cube([standardThickness, standardThickness, domeDepth/2], true);
        }
    }
}