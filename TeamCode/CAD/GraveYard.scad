// import statments
include <UtilityKit.scad>;

$fn = 100;

module ArmVer3_1() {
    midJointCount = 2; // the number of joints that seperate the wirst and the shoulder

    // sizes of first segment
    startZ = 50; // thickness of first segment NOT JOINT
    startY = 70; // width of shoulder joint NOT WIDTH OF START SEGMENT
    startLength = inchesToMillimeters(8); // distance from the center of the shoulder to the center of the next joint
    startWallThickness = 10; // thickness of connector pieces at start of arm

    // sizes of final segment
    endZ = 30;
    endY = 50; // width of wrist joint NOT THE WIDTH OF WRIST SEGMENT
    endLength = inchesToMillimeters(8); // distance from center of the joint to the assumed end of grabber
    endWallThickness = 5; // thickness of connector pieces at end of arm

    //constants
    standardThickness = 2;
    pulleyRadius = 38/2; // radius of joints

    //hose numbers
    hoseOuterRadius = inchesToMillimeters(.25/2); // (in)
    hoseInnerRadius = inchesToMillimeters(.169/2); // (in)

    // totals
    totalLength = startLength + endLength + ((startLength - endLength )/( midJointCount + 1 ))*(midJointCount * ( midJointCount+1))+endLength*midJointCount;// GENIOUS
    
    sizeTool();
    
    module sizeTool() {
        //mid segments
        if (midJointCount >= 1) {
            for(i = [1 : midJointCount]) {
                y = ((startY-endY)/(midJointCount+1))*i+endY;
                z = ((startZ-endZ)/(midJointCount+1))*i+endZ;
                length = ((startLength-endLength)/(midJointCount+1))*i+endLength;
                previousLength = ((startLength-endLength)/(midJointCount+1))*(midJointCount*(i))+   endLength*(i-1);
                if (i > 1) {
                    translate([-length/2-endLength-previousLength+totalLength, 0, 0]) cube([length, y, z    ],  true);
                }
            
                else {
                        translate([-length/2-endLength+totalLength, 0, 0]) cube([length, y, z], true);
                }
            }
        }
    
        //final segment
        translate([totalLength-endLength/2, 0, 0]) cube([endLength, endY, endZ], true);
        
        //first segment
        translate([startLength/2, 0, 0]) cube([startLength, startY, startZ], true);
    }
}