// import statments
include <UtilityKit.scad>;

$fn = 100;

channelMount();
translate([-plateThickness, -uChannelWidth/2*4, 0]) rotate([0, 90, 0]) uChannel(10);

module channelMount(holderThickness = 10, Length = 3) {
    length = Length+1;
    
    
    translate([0, 0, 0]) difference() {
        hull() {
            translate([0, -gridSize*3*length/2, 0]) rotate([0, 90, 0]) hoseHolder(); //hose mount -y
            rotate([180, 0, 0]) translate([0, -gridSize*3*length/2, 0]) rotate([0, 90, 0]) hoseHolder(); //hose mount +y
        }
        
        translate([-1, -gridSize*3*length/2, 0]) rotate([0, 90, 0]) holeCutter(); // screw holes -y
        translate([-1, gridSize*3*length/2, 0]) rotate([0, 90, 0]) holeCutter(); // screw holes +y
        }
        
        

    module holeCutter() { // mounting holes
        rotate([0, 0, 45]) union() {
            translate([-gridSize, gridSize, 0]) cylinder(uChannelWidth, smallHoleRadius+.2, smallHoleRadius+.2,false);
            translate([-gridSize, -gridSize, 0]) cylinder(uChannelWidth, smallHoleRadius+.2, smallHoleRadius+.2, false);
            translate([gridSize, gridSize, 0]) cylinder(uChannelWidth, smallHoleRadius+.2, smallHoleRadius+.2, false);
            translate([gridSize, -gridSize, 0]) cylinder(uChannelWidth, smallHoleRadius+.2, smallHoleRadius+.2, false);
        
            translate([0, 0, -.5]) cylinder(holderThickness*2, centerHoleRadius, centerHoleRadius, false); // hose hole
        }
    }

    module hoseHolder() {
        difference() {
            translate([0, 0, 0]) ring(holderThickness, centerHoleRadius, gridSize*2+smallHoleRadius+plateThickness/2, false); // ring
            translate([0, (gridSize*2+smallHoleRadius+plateThickness+1)/2, (holderThickness+1)-.1]) cube([(gridSize*2+smallHoleRadius+plateThickness+1)*2, gridSize*2+smallHoleRadius+plateThickness+1, holderThickness+1], true);
        }
    }
}