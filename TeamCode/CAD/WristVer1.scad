// import statments
use <ArmVer2.scad>;
include <ArmVer2Num.scad>;
include <GrabberVer2Num.scad>;
include <UtilityKit.scad>;

$fn = 100;

middLength = segmentX-segmentZ/3;
length = 20;
standardThickness = 2;

wrist();

module wrist() {
    translate([-segmentZ/2-hoseOuterRadius*2, 0, 0]) upperPreJoint(segmentY, segmentZ);
    difference() {
        union() {
            translate([0, segmentY/2-edgeThickness, -segmentZ/2]) cube([middLength, edgeThickness, segmentZ], false);
            translate([0, -segmentY/2, -segmentZ/2]) cube([middLength, edgeThickness, segmentZ], false);
        }
        translate([.1+20, -(segmentY+.2)/2, -segmentZ/1.5]) cube([middLength-20, segmentY+.2, segmentZ*2], false);
        
        //screw holes
        translate([length-smallHoleRadius-lineThickness-standardThickness, 0, segmentZ/3]) rotate([90, 0, 0]) screwHole(segmentY*2, true);
        translate([length-smallHoleRadius-lineThickness-standardThickness, 0, -segmentZ/3]) rotate([90, 0, 0]) screwHole(segmentY*2, true);
    }
}

