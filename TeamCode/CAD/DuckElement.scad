// import statments
include <UtilityKit.scad>;

$fn = 100;

difference() {
    union() {
        translate([20, -165, inchesToMillimeters(2)]) resize([inchesToMillimeters(4), 0, 0], true) import("Viking_Duck.stl", 10);
        cylinder(inchesToMillimeters(3), inchesToMillimeters(2), inchesToMillimeters(1));
        translate([0, 0, 150]) rotate([0, 90, 0]) ring(5, inchesToMillimeters(2)-10, inchesToMillimeters(2)-5, true);
    }
    translate([0, 0, -1]) cylinder(inchesToMillimeters(3.25), inchesToMillimeters(1.5), inchesToMillimeters(1));
}