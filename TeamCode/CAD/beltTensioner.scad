$fn = 100;

// BELT TENSIONER
lineWidth = .4;

// idler
idlerH = 12;
idlerR = 4;
boreR = 3;
guideR = 6;
guideH = .75;
taperH = .5;

// plate
plateL = 50;
plateW = 12;
plateH = 3;
edgeR = 4;

// spring mount
springMountR = 2;
springMountH = plateH/2 + 1;
springMountCapR = 3;
springMountCapH = 2;

// spindle
spindleCapR = 6;
spindleCapH = 1.2;
spindleBaseH = 2;
spindleR = boreR - lineWidth;
spindleH = plateH/2 + idlerH + 2*lineWidth + spindleBaseH;


beltTensioner();


module beltTensioner(){
    
    union(){

        // spindle
        cylinder(spindleH, spindleR, spindleR, center = false);
        translate([0, 0, spindleH]) cylinder(spindleCapH, spindleCapR, spindleCapR, center = false);
        cylinder(spindleBaseH+plateH/2, spindleCapR, spindleCapR, center = false);
 
        // spring mount
        translate([-20, 0, 0]) cylinder(springMountH, springMountR, springMountR, center = false);
        translate([-20, 0, springMountH]) cylinder(springMountCapH, springMountCapR, springMountCapR, center = false);
        
        // idler
        translate([0, 0, idlerH/2+plateH/2+lineWidth+spindleBaseH]) idler();

        // plate
        difference(){
            // plate
            plate();
    
            // mounting hole
            translate([20, 0, 0]) cylinder(5, 2, 2, center = true);
        }
    }
}

module plate(){
 
    minkowski()
    {
        cube([plateL-2*edgeR, plateW-2*edgeR, plateH-1], center = true);
        cylinder(1, edgeR, edgeR, center = true);
    }
}

module idler(){
 
    difference(){
        union(){
           cylinder(idlerH, idlerR, idlerR, center = true);
            // end caps
           translate([0, 0, idlerH/2-guideH/2]) cylinder(guideH, guideR, guideR, center = true);
           translate([0, 0, -idlerH/2+guideH/2]) cylinder(guideH, guideR, guideR, center = true);
           // tapers
           translate([0, 0, idlerH/2-guideH-taperH/2]) cylinder(taperH, idlerR, guideR, center = true);
           translate([0, 0, -idlerH/2+guideH+taperH/2]) cylinder(taperH, guideR, idlerR, center = true);
        }
        cylinder(idlerH+10, boreR, boreR, center = true);
    }
    
}
