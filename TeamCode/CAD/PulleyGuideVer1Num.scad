include <UtilityKit.scad>;

pulleyThickness = 26;
pulleyRadius = 48/2;
distanceFromUChannel = 6;
standardThickness = 2;
gapSize = lineThickness;
mountPlateLength = 48;
pulleyDistanceFromUChannel = 21.5;
pulleyMountRadius = 32/2;
mountX = pulleyDistanceFromUChannel+48;

totalThickness = pulleyThickness+distanceFromUChannel;
totalRadius = pulleyRadius+gapSize+standardThickness;