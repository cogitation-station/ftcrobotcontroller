// import statments
include <UtilityKit.scad>;

$fn = 100;

maxWidth = inchesToMillimeters(4);
maxHight = inchesToMillimeters(8);
grabberRadius = inchesToMillimeters(1.4);
standardThickness = 5;
holeCount = 11;

build();

module build() {
    difference() {
        union() {
            rotate([0, 180, 0]) cone();
            handle();
        }
        translate([0, 0, -standardThickness*2]) cylinder(maxHight/8+grabberRadius+standardThickness, maxWidth/8-standardThickness, maxWidth/8-standardThickness, false);
    }
}

module cone() {      


    
    difference() {
        union() {
            cylinder(maxHight/8, maxWidth/8, maxWidth/4, false);
            translate([0, 0, maxHight/8]) cylinder(maxHight/2, maxWidth/4, maxWidth/2, false);
        }
        translate([0, 0, 0]) cylinder(maxHight/8+standardThickness, maxWidth/8-standardThickness, maxWidth/4-standardThickness*.8, false);
        translate([0, 0, maxHight/8]) cylinder(maxHight/2+standardThickness, maxWidth/4-standardThickness, maxWidth/2-standardThickness, false);
        
        translate([0, 0, maxHight/8]) union() {
            for(i = [1 : holeCount]) {
                rotation = 360/(holeCount)*i;
                rotate([0, 0, rotation]) hull() {
                    translate([0, -standardThickness/2, 0]) cube([maxWidth, standardThickness,  standardThickness], false);
                    translate([0, -standardThickness, maxHight/2-standardThickness*2]) cube([maxWidth, standardThickness*2, standardThickness], false);
                }
            }
        }
    }
}

module handle() {
    difference() {
        union() {
            translate([0, 0, maxHight/8*3-grabberRadius]) sphere(grabberRadius);
            cylinder((maxHight/8+grabberRadius)/2, maxWidth/8, grabberRadius/4*3, false);
        }
        translate([0, 0, maxHight/8*3-grabberRadius]) sphere(grabberRadius-standardThickness);
        cylinder((maxHight/8+grabberRadius+standardThickness)/2, maxWidth/8-standardThickness, grabberRadius/4*3-standardThickness, false);
    }
}