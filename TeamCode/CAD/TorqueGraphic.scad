// import statments
include <UtilityKit.scad>;
use <ArmVer2.scad>;

$fn = 100;

graph(0, 0, 0);

segmentZ = 40;
segmentX = 200;
segmentY = 30;

gapSize = 10;

module graph(shoulderAngle = 0, elbowAngle = 0, wristAngle = 0) {
    rotate([0, shoulderAngle, 0]) union() {
        color( "PowderBlue", 1 ) segment(); // shoulder
        translate([segmentX, 0, 0]) rotate([0, elbowAngle, 0]) union() {
            color( "LightSteelBlue", 1 ) segment(); // elbow
            translate([segmentX, 0, 0]) rotate([0, wristAngle, 0]) union() {
                color( "SteelBlue", 1 ) wrist(); // wrist
                color( "Yellow", 1 ) translate([segmentX/8*7, 0, 0]) cube([segmentZ/3*2, segmentZ/3*2, segmentZ/3*2], true);
            }
        }
    }
}

module segment() {
    translate([segmentX/2, 0, 0]) cube([segmentX, segmentY, segmentZ], true);
}

module wrist() {
    translate([segmentX/4, 0, 0]) cube([segmentX/2, segmentY, segmentZ], true);
    translate([5+segmentX/2, -segmentY/2, (segmentZ)/2-5]) rotate([0, -90, 0]) rTriangle(5, segmentZ+5, segmentY, segmentZ+5);
    translate([5+segmentX/2, -segmentY/2, (-segmentZ)/2+5]) rotate([0, 180, 0]) rTriangle(5, segmentZ+5, segmentY, segmentZ+5);
    translate([segmentX/4*3, 0, segmentZ]) cube([segmentX/2, segmentY, segmentZ], true);
    translate([segmentX/4*3, 0, -segmentZ]) cube([segmentX/2, segmentY, segmentZ], true);
}