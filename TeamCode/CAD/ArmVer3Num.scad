// import statments
include <UtilityKit.scad>;

$fn = 100;

function getSegmentY(hoseCount) = getHoseHolderY(hoseCount)+getJointOuterThickness(hoseCount)*4+hoseOuterRadius*4+lineThickness*2;
function getHoseHolderY(hoseCount) = hoseHoleRadius*2*hoseCount+standardThickness*(hoseCount+1);
function getJointOuterThickness(hoseCount) = hoseCount*2.5;
function getAltSegmentY(hoseCount) = getHoseHolderY(hoseCount)+getJointOuterThickness(hoseCount+2)*4+hoseOuterRadius*4+lineThickness*2;

segmentZ = 38;
segmentLength = 8;//inches
jointAngle = 270;

//constants
standardThickness = 2.5;
pulleyRadius = 38/2; // radius of joints

//hose numbers
hoseOuterRadius = inchesToMillimeters(.25/2); // (in)
hoseInnerRadius = inchesToMillimeters(.169/2); // (in)
hoseHoleRadius = hoseOuterRadius+lineThickness;

//moniter numbers
knobLength = 10;
knobRadius = 3+lineThickness;
moniterWidth = inchesToMillimeters(.6)+.5;
moniterHoleRadius = inchesToMillimeters(0.14/2);
moniterLength = inchesToMillimeters(0.4)+.5;
moniterThickness = 10;

axleLength = 120-11-10-4;
hubRadius = 32/2;