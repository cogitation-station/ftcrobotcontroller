$fn = 100;

smallHoleRadius = 2; //(mm)
centerHoleRadius = 7; //(mm)
channelWidth = 48; //(mm)
channelLength = 264; // = (holes-1)*(gridSize*3)+uChannelWidth = (10-1)*(8*3)+48 //(mm)
plateThickness = 2.5; //(mm)
gridSize = 8; //(mm)

hoseOuterRadius = inchesToMillimeters(.25)/2; // (in)
hoseInnerRadius = inchesToMillimeters(.169)/2; // (in)

holderThickness = 10;
centerCubeHole = (gridSize*1.5-(sqrt((gridSize*2)*gridSize)))*2;

function inchesToMillimeters(inches) = inches * 25.4;


build();

module build() {
    translate([-plateThickness, -channelWidth/2, 0]) rotate([0, 90, 0]) uChannel(10);
    
    
    translate([0, 0, 0]) difference() {
        union() {
            translate([0, 0, 0]) rotate([0, 90, 0]) hoseHolder(); //hose mount -y
            rotate([180, 0, 0]) translate([0, -gridSize*3, 0]) rotate([0, 90, 0]) hoseHolder(); //hose mount +y
            
            translate([holderThickness/4, (gridSize*3)/2, 0]) cube([holderThickness/2, gridSize*3, (gridSize*2+smallHoleRadius+plateThickness/2)*2], true); //center of hose mount
        }
        
        translate([-1, 0, 0]) rotate([0, 90, 0]) holeCutter(); // screw holes -y
        translate([-1, gridSize*3, 0]) rotate([0, 90, 0]) holeCutter(); // screw holes +y
        
        translate([holderThickness/2-.25, gridSize*1.5, 0]) cube([holderThickness+1, centerCubeHole, smallHoleRadius*2+.4], true); // center hole of mount
        }
}

module uChannel(holeCount){ // u channel
    paths = [
    "C:/Users/colin/Documents/UChannelSTL/1120-0001-0048(1Hole).stl", //1
    "C:/Users/colin/Documents/UChannelSTL/1120-0002-0072(2Hole).stl", //2
    "C:/Users/colin/Documents/UChannelSTL/1120-0003-0096(3Hole).stl", //3
    "C:/Users/colin/Documents/UChannelSTL/1120-0004-0120(4Hole).stl", //4
    "C:/Users/colin/Documents/UChannelSTL/1120-0005-0144(5Hole).stl", //5
    "C:/Users/colin/Documents/UChannelSTL/1120-0006-0168(6Hole).stl", //6
    "C:/Users/colin/Documents/UChannelSTL/1120-0007-0192(7Hole).stl", //7
    "C:/Users/colin/Documents/UChannelSTL/1120-0008-0216(8Hole).stl", //8
    "C:/Users/colin/Documents/UChannelSTL/1120-0009-0240(9Hole).stl", //9
    "C:/Users/colin/Documents/UChannelSTL/1120-0010-0264(10Hole).stl", //10
    "C:/Users/colin/Documents/UChannelSTL/1120-0011-0288(11Hole).stl", //11
    "C:/Users/colin/Documents/UChannelSTL/1120-0012-0312(12Hole).stl", //12
    "C:/Users/colin/Documents/UChannelSTL/1120-0013-0336(13Hole).stl", //13
    "C:/Users/colin/Documents/UChannelSTL/1120-0014-0360(14Hole).stl", //14
    "C:/Users/colin/Documents/UChannelSTL/1120-0015-0384(15Hole).stl", //15
    "C:/Users/colin/Documents/UChannelSTL/1120-0016-0408(16Hole).stl", //16
    "C:/Users/colin/Documents/UChannelSTL/1120-0017-0432(17Hole).stl", //17
    "C:/Users/colin/Documents/UChannelSTL/1120-0018-0456(18Hole).stl"  //18
    ];
    import(paths[holeCount-1]);
}

module ring(thickness = 2, innerRadius = 0, outerRadius = 1, center = true) { // ring maker
    difference() {
        cylinder(thickness, outerRadius, outerRadius, center);
        translate([0, 0, -1]) cylinder(thickness+5, innerRadius, innerRadius, center);
    }
}

flangeInner = .1;
flangeOuter = .3;

module holeCutter() { // mounting holes
    rotate([0, 0, 45]) union() {
        translate([-gridSize, gridSize, 0]) cylinder(channelWidth, smallHoleRadius+.2, smallHoleRadius+.2, false);
        translate([-gridSize, -gridSize, 0]) cylinder(channelWidth, smallHoleRadius+.2, smallHoleRadius+.2, false);
        translate([gridSize, gridSize, 0]) cylinder(channelWidth, smallHoleRadius+.2, smallHoleRadius+.2, false);
        translate([gridSize, -gridSize, 0]) cylinder(channelWidth, smallHoleRadius+.2, smallHoleRadius+.2, false);
        
        translate([0, 0, -.5]) cylinder(holderThickness+1, hoseOuterRadius-flangeInner+.01, hoseOuterRadius-flangeInner+.01, false); // hose hole
        
        translate([0, 0, -1]) union() { // center flange thing
            for(i = [1 : channelWidth]) {
               translate([0, 0, i]) cylinder(1.1, hoseOuterRadius-flangeInner, hoseOuterRadius+flangeOuter, false);
                
            }
        }
    }
}

module hoseHolder() {
    difference() {
        translate([0, 0, 0]) ring(holderThickness/2, hoseOuterRadius-flangeInner+.01, gridSize*2+smallHoleRadius+plateThickness/2, false); // ring
        translate([0, (gridSize*2+smallHoleRadius+plateThickness/2+1)/2, (holderThickness+1)/2-.1]) cube([(gridSize*2+smallHoleRadius+plateThickness/2+1)*2, gridSize*2+smallHoleRadius+plateThickness/2+1, holderThickness+1], true);
    }
    
    translate([0, 0, 0]) ring(holderThickness, hoseOuterRadius-flangeInner+.01, hoseOuterRadius+plateThickness, false); // sleve
    
    rotate([0, 0, 45]) translate([hoseOuterRadius*2+flangeOuter, 0, 0]) cylinder(holderThickness*2, hoseOuterRadius, hoseOuterRadius, false);// peg
}

