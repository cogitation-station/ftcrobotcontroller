// import statments
include <UtilityKit.scad>;

$fn = 100;

function inchesToMillimeters(inches) = inches * 25.4;



grabber();
//hoseHolder();

module grabber() {
    translate([0, -segmentZ/2, 0]) rotate([0, 0, -90]) wrist(); //wrist joint
    translate([0, holderRadius+distance*1.25, 0]) pistonSetup(); // piston setup
    
    difference() {
        translate([0, segmentX+slotThickness, 0]) cube([segmentY, slotThickness*2, (pistonRadius+lineThickness+2)*2], true);
        translate([0, 50, 0]) rotate([-90, 0, 0]) cylinder(1000, pistonRadius+lineThickness, pistonRadius+lineThickness, false);
    }
}

module pistonSetup() {
    translate([0, 0, 0]) union() {
        translate([0, holderRadius*2, 0]) piston();
    }
    translate([0, distance/2+1+holderRadius*2, 0]) rotate([90, 0, 0])pistonCylinder();
    
    translate([0, (holderRadius*2+1)+slotThickness*2, -(mountRadius+(turnPulleyRadius-lineThickness-hoseInnerRadius))-(turnPulleyRadius*2)]) rotate([0, 90, -90]) hoseHolder();
    
    translate([0, (holderRadius*2+1), mountRadius+(turnPulleyRadius-lineThickness-hoseInnerRadius)]) union() {
        translate([0, slotThickness*2, (turnPulleyRadius*2)]) rotate([0, -90, 90]) hoseHolder();
            difference() {
                union() {
                    translate([-mountRadius, slotThickness, -turnPulleyRadius*2-lineThickness-hoseInnerRadius]) cube([mountRadius*2, slotThickness*2, mountRadius*2], false);
                }
                translate([0, -100, -pistonHolderRadius-pistonRadius]) rotate([-90, 0, 0]) cylinder(1000, pistonRadius+lineThickness, pistonRadius+lineThickness, false);
        }
    }
}


turnPulleyRadius = 5;


hoseOuterRadius = inchesToMillimeters(.25)/2; // (in)
hoseInnerRadius = inchesToMillimeters(.169)/2; // (in)
hoseWallThickness = 2;
slotThickness = 10;
hosePinRadius = hoseOuterRadius;
hoseDistance = 10;
mountRadius = hoseOuterRadius+hoseWallThickness;

lineThickness = .4;

leverX = 50;
leverY = 10; 
leverZ = 3;

axleRadius = 2;
axleLength = 5;

pulleyRadius = 19;
distance = PI*pulleyRadius; // half circumfrence of pulley

capRadius = 4;
capThickness = 1;

pusherRadius = inchesToMillimeters(.6);
pusherThickness = 3;

holderRadius = 5; 
holderThickness = axleLength-lineThickness*2;

pistonRadius = 2.5; //(mm)
pistonLength = distance*2; //(mm)

pegX = 5;
pegY = 10;
pegZ = 5;

grabThickness = inchesToMillimeters(2);
grabInnerRadius = inchesToMillimeters(2);
grabOuterRadius = inchesToMillimeters(2)+2;

transitionCalc = ((leverZ/2 + lineThickness + axleLength-lineThickness*2) - (pegZ*2+lineThickness*2)/2);

standardThickness = 2;


pistonHolderRadius = (pistonRadius+lineThickness+2);



segmentY = 75/4;
segmentZ = 38;
segmentX = (distance*1.25+holderRadius*3+1);






module wrist() {
    faces = [
    [0,1,2,3],//0
    [7,6,5,4],//1
    [0,3,7,4],//2
    [2,1,5,6],//3
    [4,5,1,0],//4
    [3,2,6,7]//5
    ];
    
    points1 = [
    [-segmentX/2, segmentY/2, -(pistonRadius+lineThickness+2)],//0
    [-segmentX/2, -segmentY/2, -(pistonRadius+lineThickness+2)],//1
    [segmentX/2, -segmentY/2, -segmentZ/2],//2
    [segmentX/2, segmentY/2, -segmentZ/2],//3
    [-segmentX/2, segmentY/2, (pistonRadius+lineThickness+2)],//4
    [-segmentX/2, -segmentY/2, (pistonRadius+lineThickness+2)],//5
    [segmentX/2, -segmentY/2, segmentZ/2],//6
    [segmentX/2, segmentY/2, segmentZ/2] //7
    ];
    
        translate([0, 0, 0]) difference(){
            union() {
                rotate([90, 0, 0]) cylinder(segmentY, segmentZ/2, segmentZ/2, true);
                difference() {
                    union() {
                        translate([-(segmentZ/2)/2, 0, 0]) cube([segmentZ/2, segmentY, segmentZ], true);
                        translate([-(segmentZ+distance*1.25+holderRadius*3+1)/2, 0, 0]) polyhedron(points1, faces);
                    }
                    
                    translate([-(segmentZ/2+distance*1.25+holderRadius*3+1)/2, 0, 0]) cube([segmentZ/2+distance*1.25+holderRadius*3+2, segmentY-8, segmentZ*2], true);
                }
                
            }
            translate([segmentX-segmentZ*1.5, 0, segmentZ/3]) rotate([0, 90, 0]) cylinder(segmentZ*10.5, hoseInnerRadius, hoseInnerRadius, true);
            translate([segmentX-segmentZ*1.5, 0, -segmentZ/3]) rotate([0, 90, 0]) cylinder(segmentZ*10.5, hoseInnerRadius, hoseInnerRadius, true);
            
            translate([0, 0, 0]) rotate([90, 0, 0]) cylinder(segmentY*2, segmentZ/4+lineThickness, segmentZ/4+lineThickness, true);
        }
}






module hoseHolder() {
    difference() {
        union() {
            translate([0, 0, 1]) ring(slotThickness-1, hoseOuterRadius, mountRadius, false);
            translate([mountRadius-2, 0, slotThickness/2]) cube([hosePinRadius*2, hosePinRadius*2, slotThickness], true);
            translate([(-hoseOuterRadius-hoseWallThickness)/2, 0, slotThickness/2]) cube([mountRadius, mountRadius*2, slotThickness], true);
        }
        translate([0, 0, -1]) cylinder(slotThickness+2, hoseOuterRadius, hoseOuterRadius, false);
    }
    
    ring(1, hoseInnerRadius, mountRadius, false);
    translate([mountRadius+hosePinRadius-2, 0, 0]) cylinder(slotThickness*2, hosePinRadius, hosePinRadius, false);
    
    pulleyCoords = mountRadius-standardThickness-lineThickness;
    
    //turn pulley
    translate([-turnPulleyRadius, 0, -turnPulleyRadius-lineThickness]) rotate([90, 0, 0]) difference() {
        union() {
            
            /*
            hull() {
                translate([0, 0, pulleyCoords-1]) cylinder(1, turnPulleyRadius, turnPulleyRadius, false);
                translate([0, 0, 0]) cylinder(1, turnPulleyRadius/2, turnPulleyRadius/2, true);
            }
            translate([0, 0, 0]) rotate([0, 0, 0]) hull() {
                translate([0, 0, 0]) cylinder(1, turnPulleyRadius/2, turnPulleyRadius/2, true);
                translate([0, 0, -pulleyCoords]) cylinder(1, turnPulleyRadius, turnPulleyRadius, false);
            }
            */
            
            
            //turn pulley mount
            translate([turnPulleyRadius, turnPulleyRadius+lineThickness, 0]) rotate([-90, 0, 0]) union() {
                for(i = [0 : 1]) {
                    translate([-mountRadius, mountRadius-standardThickness+(mountRadius-standardThickness/2)*(-i)*2, 0]) union() {
                    translate([-turnPulleyRadius-lineThickness-hoseInnerRadius, 0, -slotThickness]) cube([turnPulleyRadius+lineThickness+hoseInnerRadius+mountRadius, standardThickness, slotThickness*2], false);
                    }
                }
            }
    
        }
        translate([0, 0, -pulleyCoords*2]) cylinder(pulleyCoords*5, smallHoleRadius, smallHoleRadius, false);
    }
}







module theMightyRing() {
    rotate([-90, 0, 0]) ring(grabThickness, grabInnerRadius, grabOuterRadius, false);
}

module pistonCylinder() {
    ring(distance, pistonRadius+lineThickness, pistonRadius+lineThickness+2, true); // barrel
    translate([0, 0, -distance/2+6]) ring(2, pistonRadius+lineThickness, pistonRadius+lineThickness+4, true); // mounting ring
    translate([0, 0, -distance/2+10]) ring(2, pistonRadius+lineThickness, pistonRadius+lineThickness+4, true); // mounting ring
}

module piston(){
    //piston parts
    rotate([-90, 0, 0]) cylinder(pistonLength, pistonRadius, pistonRadius, false); //piston rod
    translate([0, pistonLength, 0]) rotate([-90, 0, 0]) cylinder(pusherThickness, pusherRadius, pusherRadius, false); //piston pusher
    translate([0, pistonLength-axleRadius, -holderRadius*2]) rotate([0, 90, 0]) ring(holderThickness, axleRadius, holderRadius, true); // pull ring
    
    translate([0, 0, 0]) rotate([0, 90, 0]) union() {
        difference() {
            union() {
                
                translate([0, -pegY/2, 0]) cube([holderRadius*2, pegY, holderThickness], true); //PEG
                translate([0, -pegY, 0]) ring(holderThickness, axleRadius, holderRadius, true); //PEG CIRCLE
            }
            translate([0, -pegY, 0]) cylinder(holderThickness+1, axleRadius+lineThickness, axleRadius+lineThickness, true); //PEG HOLE
        }
    }
}