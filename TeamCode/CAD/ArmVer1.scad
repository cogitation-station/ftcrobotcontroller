// import statments
include <UtilityKit.scad>;

$fn = 100;

arm();
runTests();
//pusher();
//pulley();

    sensorY = 23; //(mm)
    sensorYAlt = 40.5; //(mm)
    sensorX = 17 - 3; //(mm)
    sensorZ = 15; //(mm)
    sensorScrewDistance = 35.27; //(mm)
    sensorScrewRadius = 3.5; //(mm)

hoseOuterRadius = .25/2; // (in)
hoseInnerRadius = .169/2; // (in)



// CONSTANTS
g = 32*12; // gravitational acceleration (in/s^2)=(32 ft/s^2)(12 in/ft)
blockWeight = 113; //nominal weight of block with three weights, verified by measurement (g)
ballWeight = 30.1; //nominal weight of ball, verified by measurement (g)

// PHYSICAL TEST PARAMETERS FOR THE ROBOT
vmax = 16; // the vertical space under the raised tower
vmin = 11; // vertical distance (height) of the shoulder axis from the wrist joint when reaching forward to the floor level (in)
hmin = 11; // horizontal distance from the shoulder axis to the wrist joint when reaching to the floor level (in)
lowLevelHeight = 3.0; // +/-1.5 for tipping
midLevelHeight = 8.5; // +/-1.5 for tipping
topLevelHeight = 14.7; // +/-1.5 for tipping
capLevelHeight = 28.3; // 8 comes from the height of the shipping element

// TOWER
wtop = gramsToOunces(516); // weight of the tower top (oz) (whole extender)
wbottom = 1000; // weight of the tower bottom (oz)
rPulley = millimetersToInches(19); // (in)

// DRIVETRAIN
rWh = 2; // the radius of the drivetrain wheel (in)
wDT = 500; // the weight of the drivetrain (oz)
nDrivers = 4; // the number of drivetrain motors
mechanum = true; // indicates if mechanum wheels are being used
motorToDriveGearRatio = 1; // the motor to drive gear ratio

// ARM
w0 = 32; // counter weight on arm (oz)
w1 = gramsToOunces(140); // weight of the upper arm (oz)
w2 = gramsToOunces(90); // weight of the lower arm (oz)
w3 = 4; // weight of the gripper (oz)
w4 = gramsToOunces(blockWeight); // weight of the heaviest block or ball (oz)
l0 = 4; // length of counter weight (in)
l1 = 8; // length of upper arm (in)
l2 = 8; // length of lower arm (in)
l3 = 6; // length of gripper (in)
r0 = 4; // distance from shoulder axis to counter weight center of mass (in)
r1 = l1/2; // tapers, distance from shoulder axis to upper arm center of mass (in)
r2 = l2/2; // tapers, distance from elbow axis to lower arm center of mass (in)
r3 = l3/2; // distance from wrist axis to center of gripper mass (in)
r4 = 3; // distance from wrist axis to center of block or ball (in)

// MOTOR SPECS
tMotor = 338; // torque of the motor (oz in)
sMotor = 312; // motor speed (rpm)
omegaMotor = rpmToRadPerSec(sMotor); // angular frequency (rad/sec)  of motor

// TORQUE SERVO SPECS
tTServo = 350; // torque of the servo (oz in)
sTServo = 60; // speed of the torque servo (rpm)
omegaTServo = rpmToRadPerSec(sTServo); // angular frequency (rad/sec) of torque servo

// SPEED SERVO SPECS
tSServo = 150; // torque of the servo (oz in)
sSServo = 145; // speed of the torque servo (rpm)
omegaSServo = rpmToRadPerSec(sSServo); // angular frequency (rad/sec) of torque servo

// FREE PARAMETERS (actuator states)
z = 10; // the amount of linear extension of the tower (in)
th1 = 90; // the angle of the shoulder motor (deg) relative to its mount on the tower (pointing down is 0)
th2 = 0; // the angle of the elbow motor (deg) relative to its mount on the upper arm
th3 = 0; // the angle of the wrist motor (deg) relative to its mount on the lower arm

// DEPENDENT PARAMETERS (resulting from actuator states)
ph1 = th1; // the angle of the upper arm relative to the vertical or tower
ph2 = th1 + th2; // the angle of the lower arm relative to the vertical or tower
ph3 = th1 + th2 + th3; // the angle of the wrist relative to the vertical or tower

// OTHER PARAMETERS
lineThickness = .4; // 3D Printer line thickness used for space between moving parts

// FUNCTIONS
function requiredArmLength() = sqrt(hmin*hmin + vmin*vmin); // the arm length is l1 + l2 but can be divided any way
function shoulderTorque() = r1*w1 + (l1+r2)*w2 + (l1+l2+r3)*w3 + (l1+l2+r4)*w4 - r0*w0;
function elbowTorque() = r2*w2 + (l2+r3)*w3 + (l2+r4)*w4;
function wristTorque() = r3*w3 + r4*w4;
function linearLiftWeight() = w0 + w1 + w2 + w3 + w4 + wtop;
function totalRobotWeight() = w0 + w1 + w2 + w3 + w4 + wtop + wbottom + wDT;
function mechanumForce(f) = sin(45)*f; // mechanum wheel force vector breakdown, reduces acceleration force by a factor of .707
// the driving force per wheel is the motor torque divided by the radius of the wheel * gear ratio, reduced if mechanum wheel
function forcePerWheel() = mechanum ? sin(45) * motorToDriveGearRatio * tMotor/rWh : motorToDriveGearRatio * tMotor/rWh; // oz
function driveForce() = forcePerWheel() * nDrivers;
function maxAccel() = g*driveForce()/totalRobotWeight(); // maximum acceleration of the robot
      
function rpmToRadPerSec(rpm) = rpm*360/60*PI/180; // (rev/min)(360 deg/rev)(min/60 sec)(Pi radians/180 deg)
function topSpeed() = mechanum ? sin(45) * omegaMotor * rWh / motorToDriveGearRatio : omegaMotor * rWh / motorToDriveGearRatio;  // theoretical top speed of the wheel or chassis
function timeToReachTopSpeed() = topSpeed()/maxAccel();

// TEST FUNCTIONS hhhhheeeeeeeeeeeyyyyyyyyyyyyyyy
function testArmMovesUnderShoulder() = l1 < vmax && l2 < vmax;
function testArmReachesOutForwardToGround() = (l1 + l2)*(l1 + l2) >= hmin*hmin + vmin*vmin;
function canLiftMaxWeightAtShoulder() = shoulderTorque() <= tTServo;
function canLiftMaxWeightAtElbow() = elbowTorque() <= tTServo;
function canLiftMaxWeightAtWrist() = wristTorque() <= tTServo;
function canLiftLinearWeight() = linearLiftWeight() < tTServo/rPulley;
//function testArmCanReachTopLevel() = ;

module runTests(){
    echo("******************************");
    echo("GENERATING REPORT");
    echo(w0=w0, "oz");
    echo(w1=w1, "oz");
    echo(w2=w2, "oz");
    echo(w3=w3, "oz");
    echo(w4=w4, "oz");
    echo(l0=l0, "in");
    echo(l1=l1, "in");
    echo(l2=l2, "in");
    echo(l3=l3, "in");
    echo(r0=r0, "in");
    echo(r1=r1, "in");
    echo(r2=r2, "in");
    echo(r3=r3, "in");
    echo(r4=r4, "in");
    echo(wtop=wtop, "oz");
    echo(wbottom=wbottom, "oz");
    echo(wDT=wDT, "oz");
    echo("totalRobotWeight=", totalRobotWeight(), "oz");
    echo("maxAccel=", maxAccel(), "in/s^2");
    echo("topSpeed=", topSpeed(), "in/s");
    echo("timeToReachTopSpeed=", timeToReachTopSpeed(), "s");
   
    echo("RUNNING TESTS");
    if(!testArmMovesUnderShoulder()){
        echo("FAILS TEST: Arm Moves Under Shoulder");
    }
    if(!testArmReachesOutForwardToGround()){
        echo("FAILS TEST: Arm Reaches Out Forward To Ground");
    }
    shoulderTorque = shoulderTorque();
    echo(shoulderTorque=shoulderTorque, "oz-in");
    if(!canLiftMaxWeightAtShoulder()){
        echo("FAILS TEST: Can Lift Max Weight At Shoulder");
    }
    elbowTorque = elbowTorque();
    echo(elbowTorque=elbowTorque, "oz-in");
    if(!canLiftMaxWeightAtElbow()){
        echo("FAILS TEST: Can Lift Max Weight At Elbow");
    }
    wristTorque = wristTorque();
    echo(wristTorque=wristTorque, "oz-in");
    if(!canLiftMaxWeightAtWrist()){
        echo("FAILS TEST: Can Lift Max Weight At Wrist");
    }
   
    echo("linearLiftWeight=", linearLiftWeight(), "oz");
    if(!canLiftLinearWeight()){
        echo("FAILS TEST: Can Lift Linear Weight");
    }
   
    echo("TESTING COMPLETE");
    echo("******************************");
}

module upgradedArm(length = 200, startZ = 50, endZ = 30, startY = 50, endY = 25, lineThickness = .4) {
    holderDepth = 5;
    holderWallThickness = .5;
    pegLength = 10;
    pegRadius = inchesToMillimeters(hoseOuterRadius);
    
    translate([startZ*2-length+lineThickness+2.20001+holderDepth/2, 0, endZ/2-inchesToMillimeters(hoseOuterRadius)-3]) hoseMount(holderDepth, pegLength, pegRadius);
    rotate([180, 0, 0]) translate([startZ*2-length+lineThickness+2.20001+holderDepth/2, 0, endZ/2-inchesToMillimeters(hoseOuterRadius)-3]) hoseMount(holderDepth, pegLength, pegRadius);
    
    difference() {
        armSegment(length, startZ, endZ, startY, endY, lineThickness);
        translate([-length/2-20, 0, endZ/2-inchesToMillimeters(hoseOuterRadius)-3]) rotate([0, 90, 0]) cylinder(20, inchesToMillimeters(hoseOuterRadius), inchesToMillimeters(hoseOuterRadius), true);
        translate([-length/2-20, 0, -endZ/2+inchesToMillimeters(hoseOuterRadius)+3]) rotate([0, 90, 0]) cylinder(20, inchesToMillimeters(hoseOuterRadius), inchesToMillimeters(hoseOuterRadius), true);
    }
}

module hoseMount(holderDepth = 5, pegLength = 5, pegRadius = 1) {
    hoseRadius = inchesToMillimeters(hoseOuterRadius);
    //rotate([90, 0, 0]) translate([holderDepth, 0, -hoseRadius-pegRadius]) rotate([0, 90, 0]) cylinder(holderDepth, pegRadius, pegRadius, true);
    rotate([-90, 0, 0]) translate([pegLength/2, 0, -hoseRadius-pegRadius]) rotate([0, 90, 0]) cylinder(pegLength, pegRadius, pegRadius, true);
}

module arm(){
    upperSegmentX = inchesToMillimeters(l1);
    lowerSegmentX = inchesToMillimeters(l2);
    grabberSegmentX = inchesToMillimeters(l3);
    counterX = inchesToMillimeters(l0);
  
    upperSegmentY = 75; //(mm)
    lowerSegmentY = upperSegmentY/2; //(mm)
    grabberSegmentY = lowerSegmentY/2;
    grabberSegmentYAlt = lowerSegmentY * 1.5;
  
    upperSegmentZ = 38;
    lowerSegmentZ = 38;
    grabberSegmentZ = 38;
   
    grabberInnerRadius = inchesToMillimeters(4);
    grabberDepth = inchesToMillimeters(2);
    pistonRadius = inchesToMillimeters(.5);
   
    upperRotate = th1-90;
    lowerRotate = th2;
    grabberRotate = th3;
  
    rotate([0, upperRotate, 0]) union() {
        
        translate([-upperSegmentZ/2, 0, 0]) upperArmSegment(upperSegmentX, upperSegmentZ,  upperSegmentY, .4, counterX);//+x
        
  
        translate([-lowerSegmentX+upperSegmentZ, 0, 0]) rotate([0, lowerRotate, 0]) union() {
            difference() {
                translate([-upperSegmentZ/2, 0, 0]) upgradedArm(lowerSegmentX, lowerSegmentZ, upperSegmentZ, lowerSegmentY, upperSegmentY/2, .4);//-x
            
                translate([-lowerSegmentX+upperSegmentZ, 0, 0]) cube([upperSegmentZ+2.799, lowerSegmentY/3*2, upperSegmentZ+1], true);
                
                translate([-lowerSegmentX+upperSegmentZ*2-4, 0, 0]) cube([sensorX+1, sensorYAlt, sensorZ], true);
                
                translate([-lowerSegmentX+upperSegmentZ, 0, 0]) rotate([90, 0, 0]) cylinder(lowerSegmentY+1, 7, 7, true);
                
                //translate([-20, 0, -20]) cube([40, 20 ,40], false);
            }
            
    //        translate([-lowerSegmentX+lowerSegmentZ, 0, 0]) rotate([0, grabberRotate, 0]) translate([-lowerSegmentZ/2, 0, 0]) grabber(grabberSegmentX, grabberSegmentY, grabberSegmentYAlt, grabberSegmentZ, grabberInnerRadius, grabberDepth, pistonRadius);
        }
    }
  
}

module armSegment(Length = 200, startZ = 50, endZ = 30, startY = 50, endY = 25, lineThickness = .4){
  
  
    //translate([-segmentX+endZ*1.125, 0, 0]) cube([sensorX, sensorYAlt, sensorZ], true);
  

  
    magnetY = 22.75; //(mm)
    magnetYAlt = 11; // (mm)
    magnetX = 9.25; //(mm)
    magnetZAlt = 4.25; //(mm)
    magnetZ = 2; //(mm)
  
  
    segmentX = Length/2;
  
    //x y z
    points1 = [
    [-segmentX+startZ, startY/2, -startZ/2],//0
    [-segmentX+startZ, -startY/2, -startZ/2],//1
    [segmentX-endZ, -endY/2, -endZ/2],//2
    [segmentX-endZ, endY/2, -endZ/2],//3
    [-segmentX+startZ, startY/2, startZ/2],//4
    [-segmentX+startZ, -startY/2, startZ/2],//5
    [segmentX-endZ, -endY/2, endZ/2],//6
    [segmentX-endZ, endY/2, endZ/2] //7
    ];
  
    points2 = [
    [(-segmentX+startZ), startY/3, -startZ],//0
    [(-segmentX+startZ), -startY/3, -startZ],//1
    [(segmentX-endZ), -endY/3, -endZ],//2
    [(segmentX-endZ), endY/3, -endZ],//3
    [(-segmentX+startZ), startY/3, startZ],//4
    [(-segmentX+startZ), -startY/3, startZ],//5
    [(segmentX-endZ), -endY/3, endZ],//6
    [(segmentX-endZ), endY/3, endZ] //7
    ];
  
    faces = [
    [0,1,2,3],//0
    [7,6,5,4],//1
    [0,3,7,4],//2
    [2,1,5,6],//3
    [4,5,1,0],//4
    [3,2,6,7]//5
    ];
  
    points3 = [
    [endZ/2, endY/2, 0], //0
    [endZ/2, -endY/2, 0], //1
    [0, -endY/2, 0], //2
    [0, endY/2, 0], //3
    [endZ/2, -endY/2, -endZ/2], //4
    [endZ/2, endY/2, -endZ/2], //5
    ];
  
    points4 = [
    [endZ/4, endY, 0], //0
    [endZ/4, -endY, 0], //1
    [0, -endY, 0], //2
    [0, endY, ], //3
    [endZ/4, -endY, -endZ/4], //4
    [endZ/4, endY, -endZ/4], //5
    ];
  
    faces2 = [
    [0,1,2,3], //0 Top
    [1,0,5,4], //1 front
    [3,2,4,5], //2 bottom
    [2,1,4], //3 left
    [0,3,5], //4 right
    ];
  
    /*
    translate([segmentX-endZ/2*3-lineThickness*2, 0, -endZ/2]) difference() {
        union() {
            polyhedron(points3, faces2);
          
        }
        translate([endZ/6, 0, -endZ/18]) polyhedron(points4, faces2);
    }
    */
  
    /*
    translate([segmentX-endZ/3*2.5, 0, 0]) difference() {
        cylinder(8, endZ/3, endZ/3, true);
        cylinder(10, endZ/3.5, endZ/3.5, true);
        translate([endZ/3, 0, 0]) cube([endZ/3*2, endZ, endZ], true);
    }
    */
 
   translate([-segmentX+endZ, 0, 0]) difference() {
       union() {
    difference() {
        union() {
            polyhedron(points1, faces);
            //translate([segmentX-endZ+endZ/4, 0, 0]) cube([endZ/2, endY, endZ], true);
            translate([-segmentX+startZ-startZ/4, 0, 0]) cube([startZ/2, startY, startZ], true);
          
            translate([-segmentX+startZ/2, 0, 0]) difference(){
                rotate([90, 0, 0]) cylinder(startY, startZ/2, startZ/2, true);
                translate([startZ/2, 0, 0]) cube([startZ, startZ*2, startZ*2], true);
            }
          
        }
      
        translate([-segmentX+startZ/2, 0, 0]) cube([startZ+lineThickness*2+2, startY/2+lineThickness*2, startZ*3], true);
        translate([endZ/5, 0, 0]) polyhedron(points2, faces);
      
        translate([-segmentX+endZ*1.125, 0, 0]) cube([sensorX, sensorY, sensorZ], true);
      
      
    }
  
    translate([segmentX-endZ/2, 0, 0]) difference(){
        union() {
                rotate([90, 0, 0]) cylinder(endY, endZ/2, endZ/2, true);
                difference() {
                    translate([-endZ/4, 0, 0]) cube([endZ/2, endY, endZ], true);
                    translate([-endZ/4, 0, 0]) cube([endZ, endY/3*2, endZ*2], true);
                }
                  
                }
              
        translate([0, 0, 0]) rotate([90, 0, 0]) cylinder(endY*3, endZ/4-lineThickness, endZ/4-lineThickness, true);
              
        }
      
      
      //axle
    translate([-segmentX+startZ/2, 0, 0]) rotate([90, 0, 0]) cylinder(startY, startZ/4-lineThickness*2, startZ/4-lineThickness*2, true);
    }
  
    translate([segmentX-endZ/2, 0, endZ/3]) rotate([0, 90, 0]) cylinder(endZ*1.5, inchesToMillimeters(hoseInnerRadius), inchesToMillimeters(hoseInnerRadius), true);
    translate([segmentX-endZ/2, 0, -endZ/3]) rotate([0, 90, 0]) cylinder(endZ*1.5, inchesToMillimeters(hoseInnerRadius), inchesToMillimeters(hoseInnerRadius), true);
  
  
    translate([segmentX-endZ/2, 0, -endZ/2]) cube([magnetX, magnetY, magnetZ], true);
    translate([segmentX-endZ/2-magnetX/2, -magnetYAlt/2, -endZ/2]) cube([magnetX, magnetYAlt, magnetZAlt], false);
  
    translate([segmentX-endZ/2, 0, endZ/2]) cube([magnetX, magnetY, magnetZ], true);
    translate([segmentX-endZ/2-magnetX/2, -magnetYAlt/2, endZ/2-magnetZAlt]) cube([magnetX, magnetYAlt, magnetZAlt], false);
    }
}

module grabberMount(segmentX, segmentY, segmentZ, grabberDepth, pistonRadius){

    translate([-lineThickness, 0, 0]) rotate([0, -90, 0]) cylinder(40, 40, 40, center=false);

}

module pusher(){
    flywheelRadius = 30;
    flywheelThickness = 3;
    pulleyRadius = 19;
    pulleyThickness = 10;
    axleRadius = 10;
    axleLength = 25;
    // flywheel, lower
    cylinder(flywheelThickness, flywheelRadius, flywheelRadius, center=false);
    // pulley
    translate([0, 0, flywheelThickness]) cylinder(pulleyThickness, pulleyRadius, flywheelRadius, center=false);
    // flywheel, upper
    translate([0, 0, flywheelThickness+pulleyThickness]) cylinder(flywheelThickness, flywheelRadius, flywheelRadius, center=false);
    // axle
    cylinder(axleLength, axleRadius, flywheelRadius, center=false);
    // endcap
    translate([0, 0, axleLength]) cylinder(flywheelThickness, flywheelRadius, flywheelRadius, center=false);
   
    //thing
    translate([flywheelRadius-3, 0, 1]) cylinder(flywheelThickness, 2, 3, false);
   
}

module piston(segmentX, segmentY, segmentZ, grabberDepth, pistonRadius){
   
    wallThickness = 5;
   
    points = [
    [-segmentZ/2, -pistonRadius-.4-wallThickness, -segmentZ/2], //0
    [-segmentZ/2, -pistonRadius-.4, -segmentZ/2], //1
    [segmentZ/2-wallThickness, -pistonRadius-.4, -segmentZ/2], //2
    [segmentZ/2-wallThickness, pistonRadius+.4, -segmentZ/2], //3
    [-segmentZ/2, pistonRadius+.4, -segmentZ/2], //4
    [-segmentZ/2, pistonRadius+.4+wallThickness, -segmentZ/2], //5
    [segmentZ/2, pistonRadius+.4+wallThickness, -segmentZ/2], //6
    [segmentZ/2, -pistonRadius-.4-wallThickness, -segmentZ/2],  //7
    [-segmentZ/2, -pistonRadius-.4-wallThickness, segmentZ/2], //8
    [-segmentZ/2, -pistonRadius-.4, segmentZ/2], //9
    [segmentZ/2-wallThickness, -pistonRadius-.4, segmentZ/2], //10
    [segmentZ/2-wallThickness, pistonRadius+.4, segmentZ/2], //11
    [-segmentZ/2, pistonRadius+.4, segmentZ/2], //12
    [-segmentZ/2, pistonRadius+.4+wallThickness, segmentZ/2], //13
    [segmentZ/2, pistonRadius+.4+wallThickness, segmentZ/2], //14
    [segmentZ/2, -pistonRadius-.4-wallThickness, segmentZ/2]  //15
    ];
   
    faces = [
    [8,15,7,0], //0
    [6,5,13,14], //1
    [0,1,2,3,4,5,6,7], //2
    [8,9,10,11,12,13,14,15], //3
    [9,8,0,1], //4
    [13,12,4,5], //5
    [15,14,6,7], //6
    [11,10,2,3], //7
    [10,9,1,2], //8
    [12,11,3,4], //9
    ];
   
    translate([-segmentZ/2, 0, 0]) polyhedron(points, faces, 0);
   
    translate([0, 0, 30]) difference() {
        cube([segmentZ/8, segmentY, segmentZ], true);
        translate([segmentZ/4, 0, 0]) cube([segmentZ/2+1, segmentY/3*2, segmentZ+1], true);
    }
   
}

module pulley(){
    capThickness = 2;
    channelThickness = 5;
    boreRadius = 14/2;
    screwHoleRadius = 4/2;
    innerRadius = 35/2;
    outerRadius = 38/2;
    capRadius = 48/2;
    holeDistance = 12;
   
    difference(){
        union(){
            // core
            cylinder(channelThickness*2+capThickness*3, outerRadius, outerRadius, center=false);
            // bottom cap
            cylinder(capThickness, capRadius, capRadius, center=false);
            // bottom channel helper
            translate([0, 0, capThickness]) cylinder(channelThickness, capRadius, outerRadius, center=false);
            translate([0, 0, capThickness+channelThickness]) cylinder(channelThickness, outerRadius, capRadius, center=false);
            // middle cap
            translate([0, 0, capThickness+channelThickness*2]) cylinder(capThickness, capRadius, capRadius, center=false);
            // upper channel helper
            translate([0, 0, capThickness*2+channelThickness*2]) cylinder(channelThickness, capRadius, outerRadius, center=false);
            translate([0, 0, capThickness*2+channelThickness*3]) cylinder(channelThickness, outerRadius, capRadius, center=false);

            // top cap
            translate([0, 0, 2*capThickness+4*channelThickness]) cylinder(capThickness, capRadius, capRadius, center=false);

        }
           
        // inner hollow
        translate([0, 0, capThickness]) cylinder(4*channelThickness+3*capThickness+1, innerRadius, innerRadius, center=false);
        // bore hole
        cylinder(channelThickness*3, boreRadius, boreRadius, center=true);
        // screw holes
        translate([0, holeDistance, 0]) cylinder(3*channelThickness, screwHoleRadius, screwHoleRadius, center=true);
        translate([0, -holeDistance, 0]) cylinder(3*channelThickness, screwHoleRadius, screwHoleRadius, center=true);
        translate([holeDistance, 0, 0]) cylinder(3*channelThickness, screwHoleRadius, screwHoleRadius, center=true);
        translate([-holeDistance, 0, 0]) cylinder(3*channelThickness, screwHoleRadius, screwHoleRadius, center=true);
        // side holes
        translate([innerRadius/2, 0, 2*capThickness+channelThickness*3]) rotate([0, 90, 0]) cylinder((outerRadius*2-innerRadius/2), channelThickness/2, channelThickness/2, center=false);
        translate([innerRadius/2, 0, capThickness+channelThickness]) rotate([0, 90, 0]) cylinder((outerRadius*2-innerRadius/2), channelThickness/2, channelThickness/2, center=false);
    }
}




module upperArmSegment(segmentX, segmentZ, segmentY, lineThickness, counterX) {
    weightRadius = inchesToMillimeters(1.25)/2;
    weightHolderThickness = 3;
    
    //make basic arm
    
    
    
    //joint Things
    translate([segmentZ/2, 0, 0]) difference() {
        union() {
            rotate([90, 0, 0]) cylinder(segmentY, segmentZ/3, segmentZ/3, true);
            difference() {
                translate([-segmentZ/2, 0, 0]) upgradedArm(segmentX, segmentZ, segmentZ,  segmentY, segmentY, lineThickness);//ARM
                cube([segmentZ+1, segmentY/3*2, segmentZ+1], true);
            }
        
    
            //counter weight stuff
            translate([0, 0, 0]) union() {
                difference() {
                    union() {
                        translate([(counterX-segmentZ/2)/2, 0, 0]) cube([counterX-segmentZ/2, segmentY, segmentZ], true);
                        translate([counterX-segmentZ/2, 0, 0]) rotate([90, 0, 0]) cylinder(segmentY, segmentZ/2, segmentZ/2, true);
                
                    }
                    translate([counterX, 0, 0]) cube([segmentZ, segmentY+1, weightRadius*2], true);
                    translate([(counterX+1)/2, 0, 0]) cube([counterX+1, segmentY/3*2, segmentZ+1], true);
                    translate([counterX-segmentZ/2, 0, 0]) rotate([90, 0, 0]) cylinder(segmentY+1, weightRadius, weightRadius, true);
                }
        
                difference() {
                    translate([counterX-segmentZ/2, 0, 0]) rotate([90, 0, 0]) cylinder(segmentY, weightRadius+weightHolderThickness, weightRadius+weightHolderThickness, true);
                    translate([counterX-segmentZ/2, 0, 0]) rotate([90, 0, 0]) cylinder(segmentY+1, weightRadius, weightRadius, true);
                    translate([counterX, 0, 0]) cube([segmentZ, segmentY+1, weightRadius*2], true);
                }
            }
        }
        translate([0, 0, 0]) rotate([90, 0, 0]) linear_extrude(height = segmentY+1, center = true, convexity = 10, 0, slices = 20, scale = 1.0, $fn = 16) circle(r=4, $fn=6);
    }
}