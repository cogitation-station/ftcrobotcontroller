$fn = 100;

// variables for translating numbers 
function inchesToMillimeters(inches) = inches * 25.4; // translates inches to millimeters
function millimetersToInches(millimeters) = millimeters/25.4; // translates millimeters to inches
function gramsToOunces(grams) = grams * 0.035274; // translates grams to ounces
function channelLength(holes) = holes*3*gridSize+gridSize*3;
function ozInTogMm(ozIn) = ozIn*28.34952*25.4;
function PAIN(hypotenuse) = sqrt((hypotenuse*hypotenuse)/2);
function SUFFERING(sideLength) = PAIN((sqrt(sideLength*sideLength*2))/2); // to save time you could just devide by four instead.

module ring(thickness = 2, innerRadius = 0, outerRadius = 1, center = true) { // makes a ring with the given variables
    difference() {
        cylinder(thickness, outerRadius, outerRadius, center); // outer cylinder
        translate([0, 0, -1]) cylinder(thickness+5, innerRadius, innerRadius, center); // inner cylinder
    }
}

// standard Go Bilda U Channel numbers (mm)
uChannelWidth = 48; // width of u channel (mm)
gridSize = 8; // size of grid used for hole placement (mm)
plateThickness = 2.5; // thickness of u channel walls (mm)
centerHoleRadius = 7; // radius of the hole located at the center of a uchannel (mm)
smallHoleRadius = 2; // radius of small holes in u channel (mm)

// printing numbers
lineThickness = .4; // thickness of the lines made in the printer (mm)

module uChannel(holeCount){ // imports a uChannel with hole count from 1 - 18
    paths = [
    "C:/Users/colin/Documents/UChannelSTL/1120-0001-0048(1Hole).stl", //1 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0002-0072(2Hole).stl", //2 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0003-0096(3Hole).stl", //3 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0004-0120(4Hole).stl", //4 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0005-0144(5Hole).stl", //5 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0006-0168(6Hole).stl", //6 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0007-0192(7Hole).stl", //7 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0008-0216(8Hole).stl", //8 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0009-0240(9Hole).stl", //9 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0010-0264(10Hole).stl", //10 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0011-0288(11Hole).stl", //11 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0012-0312(12Hole).stl", //12 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0013-0336(13Hole).stl", //13 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0014-0360(14Hole).stl", //14 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0015-0384(15Hole).stl", //15 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0016-0408(16Hole).stl", //16 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0017-0432(17Hole).stl", //17 hole
    "C:/Users/colin/Documents/UChannelSTL/1120-0018-0456(18Hole).stl"  //18 hole
    ];
    import(paths[holeCount-1]);
}

module uChannelCutter(thickness = 10) { // cuts a holes that fit around the center hole in a go bilda uchannel
    translate([0, 0, 0]) rotate([0, 0, 45]) union() {
        translate([-gridSize, gridSize, 0]) cylinder(thickness, smallHoleRadius*1.25, smallHoleRadius*1.25, false);
        translate([-gridSize, -gridSize, 0]) cylinder(thickness, smallHoleRadius*1.25, smallHoleRadius*1.25, false);
        translate([gridSize, gridSize, 0]) cylinder(thickness, smallHoleRadius*1.25, smallHoleRadius*1.25, false);
        translate([gridSize, -gridSize, 0]) cylinder(thickness, smallHoleRadius*1.25, smallHoleRadius*1.25, false);
        
        translate([0, 0, 0]) cylinder(thickness, centerHoleRadius, centerHoleRadius, false); // center hole
    }
}

module moreHoleChannelCutter(thickness = 10) {
    uChannelCutter(thickness);
    translate([-gridSize*2, gridSize*2, 0]) cylinder(thickness, smallHoleRadius*1.25, smallHoleRadius*1.25, false);
    translate([-gridSize*2, -gridSize*2, 0]) cylinder(thickness, smallHoleRadius*1.25, smallHoleRadius*1.25, false);
    translate([gridSize*2, gridSize*2, 0]) cylinder(thickness, smallHoleRadius*1.25, smallHoleRadius*1.25, false);
    translate([gridSize*2, -gridSize*2, 0]) cylinder(thickness, smallHoleRadius*1.25, smallHoleRadius*1.25, false);
    translate([-gridSize, gridSize, 0]) cylinder(thickness, smallHoleRadius*1.25, smallHoleRadius*1.25, false);
    translate([-gridSize, -gridSize, 0]) cylinder(thickness, smallHoleRadius*1.25, smallHoleRadius*1.25, false);
    translate([gridSize, gridSize, 0]) cylinder(thickness, smallHoleRadius*1.25, smallHoleRadius*1.25, false);
    translate([gridSize, -gridSize, 0]) cylinder(thickness, smallHoleRadius*1.25, smallHoleRadius*1.25, false);
}

module rTriangle(thickness = 1, sideX = 5, sideY, sideZ = 5) {
    hull() {
        cube([thickness, sideY, sideZ], false);
        cube([sideX, sideY, thickness], false);
    }
}

module screwHole(length = 10, center = true) {
    cylinder(length, smallHoleRadius+lineThickness, smallHoleRadius+lineThickness, center);
}