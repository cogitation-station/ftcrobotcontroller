include <UtilityKit.scad>;
use <Field.scad>;

//runTests();
map();

// CONSTANTS
g = 32*12; // gravitational acceleration (in/s^2)=(32 ft/s^2)(12 in/ft)
blockWeight = 113; //nominal weight of block with three weights, verified by measurement (g)
ballWeight = 30.1; //nominal weight of ball, verified by measurement (g)

// PHYSICAL TEST PARAMETERS FOR THE ROBOT
vmax = 16; // the vertical space under the raised tower
vmin = 11; // vertical distance (height) of the shoulder axis from the wrist joint when reaching forward to the floor level (in)
hmin = 11; // horizontal distance from the shoulder axis to the wrist joint when reaching to the floor level (in)
lowLevelHeight = 3.0; // +/-1.5 for tipping
midLevelHeight = 8.5; // +/-1.5 for tipping
topLevelHeight = 14.7; // +/-1.5 for tipping
capLevelHeight = 28.3; // 8 comes from the height of the shipping element

// TOWER
wtop = gramsToOunces(516); // weight of the tower top (oz) (whole extender)
wbottom = 1000; // weight of the tower bottom (oz)
rPulley = millimetersToInches(19); // (in)

// DRIVETRAIN
rWh = 2; // the radius of the drivetrain wheel (in)
wDT = 500; // the weight of the drivetrain (oz)
nDrivers = 4; // the number of drivetrain motors
mechanum = true; // indicates if mechanum wheels are being used
motorToDriveGearRatio = 1; // the motor to drive gear ratio

// ARM
w0 = 32; // counter weight on arm (oz)
w1 = gramsToOunces(140); // weight of the upper arm (oz)
w2 = gramsToOunces(90); // weight of the lower arm (oz)
w3 = 4; // weight of the gripper (oz)
w4 = gramsToOunces(blockWeight); // weight of the heaviest block or ball (oz)
l0 = 4; // length of counter weight (in)
l1 = 8; // length of upper arm (in)
l2 = 8; // length of lower arm (in)
l3 = 6; // length of gripper (in)
r0 = 4; // distance from shoulder axis to counter weight center of mass (in)
r1 = l1/2; // tapers, distance from shoulder axis to upper arm center of mass (in)
r2 = l2/2; // tapers, distance from elbow axis to lower arm center of mass (in)
r3 = l3/2; // distance from wrist axis to center of gripper mass (in)
r4 = 3; // distance from wrist axis to center of block or ball (in)

// MOTOR SPECS
tMotor = 338; // torque of the motor (oz in)
sMotor = 312; // motor speed (rpm)
omegaMotor = rpmToRadPerSec(sMotor); // angular frequency (rad/sec)  of motor

// TORQUE SERVO SPECS
tTServo = 350; // torque of the servo (oz in)
sTServo = 60; // speed of the torque servo (rpm)
omegaTServo = rpmToRadPerSec(sTServo); // angular frequency (rad/sec) of torque servo

// SPEED SERVO SPECS
tSServo = 150; // torque of the servo (oz in)
sSServo = 145; // speed of the torque servo (rpm)
omegaSServo = rpmToRadPerSec(sSServo); // angular frequency (rad/sec) of torque servo

// BIG MOTOR SPECS
tBigMotor = ozInTogMm(3470);
sBigMotor = 30;
omegaBigMotor = rpmToRadPerSec(sBigMotor);

// FREE PARAMETERS (actuator states)
z = 10; // the amount of linear extension of the tower (in)
th1 = 90; // the angle of the shoulder motor (deg) relative to its mount on the tower (pointing down is 0)
th2 = 0; // the angle of the elbow motor (deg) relative to its mount on the upper arm
th3 = 0; // the angle of the wrist motor (deg) relative to its mount on the lower arm

// DEPENDENT PARAMETERS (resulting from actuator states)
ph1 = th1; // the angle of the upper arm relative to the vertical or tower
ph2 = th1 + th2; // the angle of the lower arm relative to the vertical or tower
ph3 = th1 + th2 + th3; // the angle of the wrist relative to the vertical or tower

// OTHER PARAMETERS
lineThickness = .4; // 3D Printer line thickness used for space between moving parts

// build numbers
density = 1; // grams per mm^3
valueOfSegments = [64, 32, 16, 8, 4, 2];
grabberWeight = 250;//g
grabberLength = inchesToMillimeters(5);//mm
loadWeight = 1800;//g
loadLength = inchesToMillimeters(2); // m

// FUNCTIONS
function requiredArmLength() = sqrt(hmin*hmin + vmin*vmin); // the arm length is l1 + l2 but can be divided any way
function shoulderTorque() = r1*w1 + (l1+r2)*w2 + (l1+l2+r3)*w3 + (l1+l2+r4)*w4 - r0*w0;
function elbowTorque() = r2*w2 + (l2+r3)*w3 + (l2+r4)*w4;
function wristTorque() = r3*w3 + r4*w4;
function linearLiftWeight() = w0 + w1 + w2 + w3 + w4 + wtop;
function totalRobotWeight() = w0 + w1 + w2 + w3 + w4 + wtop + wbottom + wDT;
function mechanumForce(f) = sin(45)*f; // mechanum wheel force vector breakdown, reduces acceleration force by a factor of .707
// the driving force per wheel is the motor torque divided by the radius of the wheel * gear ratio, reduced if mechanum wheel
function forcePerWheel() = mechanum ? sin(45) * motorToDriveGearRatio * tMotor/rWh : motorToDriveGearRatio * tMotor/rWh; // oz
function driveForce() = forcePerWheel() * nDrivers;
function maxAccel() = g*driveForce()/totalRobotWeight(); // maximum acceleration of the robot
      
function rpmToRadPerSec(rpm) = rpm*360/60*PI/180; // (rev/min)(360 deg/rev)(min/60 sec)(Pi radians/180 deg)
function topSpeed() = mechanum ? sin(45) * omegaMotor * rWh / motorToDriveGearRatio : omegaMotor * rWh / motorToDriveGearRatio;  // theoretical top speed of the wheel or chassis
function timeToReachTopSpeed() = topSpeed()/maxAccel();

// TEST FUNCTIONS hhhhheeeeeeeeeeeyyyyyyyyyyyyyyy
function testArmMovesUnderShoulder() = l1 < vmax && l2 < vmax;
function testArmReachesOutForwardToGround() = (l1 + l2)*(l1 + l2) >= hmin*hmin + vmin*vmin;
function canLiftMaxWeightAtShoulder() = shoulderTorque() <= tTServo;
function canLiftMaxWeightAtElbow() = elbowTorque() <= tTServo;
function canLiftMaxWeightAtWrist() = wristTorque() <= tTServo;
function canLiftLinearWeight() = linearLiftWeight() < tTServo/rPulley;
//function testArmCanReachTopLevel() = ;

// build functions
function weightOfSegment(x, y, z) = x*y*z*density;
function j5Torque(l5, w5) = l5/2*w5 + (l5+grabberLength/2)*grabberWeight + (l5+grabberLength+loadLength/2)*loadWeight;// l5 is expected to be around 3 inches while the grabber will be much longer
function j4Torque(l5, w5, l4, w4) = l4/2*w4 + (l4+l5/2)*w5 + (l4+l5+grabberLength/2)*grabberWeight + (l4+l5+grabberLength+loadLength/2)*loadWeight;
function j3Torque(l5, w5, l4, w4, l3, w3) = l3/2*w3 + (l3+l4/2)*w4 + (l3+l4+l5/2)*w5 + (l3+l4+l5+grabberLength/2)*grabberWeight + (l3+l4+l5+grabberLength+loadLength/2)*loadWeight;
function j2Torque(l5, w5, l4, w4, l3, w3, l2, w2) = l2/2*w2 + (l2+l3/2)*w3 + (l2+l3+l4/2)*w4 + (l2+l3+l4+l5/2)*w5 + (l2+l3+l4+l5+grabberLength/2)*grabberWeight + (l2+l3+l4+l5+grabberLength+loadLength/2)*loadWeight;






function j1Torque(l5, w5, l4, w4, l3, w3, l2, w2, l1, w1) = l1/2*w1 + (l1+l2/2)*w2 + (l2+l3/2)*w3 + (l2+l3+l4/2)*w4 + (l1+l2+l3+l4+l5/2)*w5 + (l1+l2+l3+l4+l5+grabberLength/2)*grabberWeight + (l1+l2+l3+l4+l5+grabberLength+loadLength/2)*loadWeight;






echo(j1Torque(inchesToMillimeters(3), 50, inchesToMillimeters(8), 250, inchesToMillimeters(8), 250, inchesToMillimeters(8), 250, inchesToMillimeters(8), 250), " shoulder");
echo(tBigMotor, " motor");

// GEOMETRIC FUNCTIONS
// the slope and intercept of a straight line defined by two points
function slope(p1, p2) = (p2.y-p1.y)/(p2.x-p1.x);
function yIntercept(p1, p2) = p2.y-(p2.y-p1.y)/(p2.x-p1.x)*p2.x;
// the intersection point of two lines given by slopes m1 and m2 and intercepts b1 and b2 (not valid for parallel lines, when m2=m1)
function intersectionPoint(m1, b1, m2, b2) = [(b1-b2)/(m2-m1), m1*(b1-b2)/(m2-m1) + b1];
// do line segments intersect

// reach of the arm (horizontal) for a set of angles and lengths
function reach(set) = l0*sin(t0) + l1*sin(t1+t0) + l2*sin(t2+t1+t0) + l3*sin(t3+t2+t1+t0) + l4*sin(t4+t3+t2+t1+t0) + l5*sin(t5+t4+t3+t2+t1+t0);
function height(h) = h - l0*cos(t0) - l1*cos(t1+t0) - l2*cos(t2+t1+t0) - l3*cos(t3+t2+t1+t0) - l4*cos(t4+t3+t2+t1+t0) - l5*cos(t5+t4+t3+t2+t1+t0);


module runTests(){
    echo("******************************");
    echo("GENERATING REPORT");
    echo(w0=w0, "oz");
    echo(w1=w1, "oz");
    echo(w2=w2, "oz");
    echo(w3=w3, "oz");
    echo(w4=w4, "oz");
    echo(l0=l0, "in");
    echo(l1=l1, "in");
    echo(l2=l2, "in");
    echo(l3=l3, "in");
    echo(r0=r0, "in");
    echo(r1=r1, "in");
    echo(r2=r2, "in");
    echo(r3=r3, "in");
    echo(r4=r4, "in");
    echo(wtop=wtop, "oz");
    echo(wbottom=wbottom, "oz");
    echo(wDT=wDT, "oz");
    echo("totalRobotWeight=", totalRobotWeight(), "oz");
    echo("maxAccel=", maxAccel(), "in/s^2");
    echo("topSpeed=", topSpeed(), "in/s");
    echo("timeToReachTopSpeed=", timeToReachTopSpeed(), "s");
   
    echo("RUNNING TESTS");
    if(!testArmMovesUnderShoulder()){
        echo("FAILS TEST: Arm Moves Under Shoulder");
    }
    if(!testArmReachesOutForwardToGround()){
        echo("FAILS TEST: Arm Reaches Out Forward To Ground");
    }
    shoulderTorque = shoulderTorque();
    echo(shoulderTorque=shoulderTorque, "oz-in");
    if(!canLiftMaxWeightAtShoulder()){
        echo("FAILS TEST: Can Lift Max Weight At Shoulder");
    }
    elbowTorque = elbowTorque();
    echo(elbowTorque=elbowTorque, "oz-in");
    if(!canLiftMaxWeightAtElbow()){
        echo("FAILS TEST: Can Lift Max Weight At Elbow");
    }
    wristTorque = wristTorque();
    echo(wristTorque=wristTorque, "oz-in");
    if(!canLiftMaxWeightAtWrist()){
        echo("FAILS TEST: Can Lift Max Weight At Wrist");
    }
   
    echo("linearLiftWeight=", linearLiftWeight(), "oz");
    if(!canLiftLinearWeight()){
        echo("FAILS TEST: Can Lift Linear Weight");
    }
   
    echo("TESTING COMPLETE");
    echo("******************************");
}

module generateReport(){
    echo("******************************");
    echo("GENERATING REPORT");
    echo(w0=w0, "oz");
    echo(w1=w1, "oz");
    echo(w2=w2, "oz");
    echo(w3=w3, "oz");
    echo(w4=w4, "oz");
    echo(l0=l0, "in");
    echo(l1=l1, "in");
    echo(l2=l2, "in");
    echo(l3=l3, "in");
    echo(r0=r0, "in");
    echo(r1=r1, "in");
    echo(r2=r2, "in");
    echo(r3=r3, "in");
    echo(r4=r4, "in");
    echo(wtop=wtop, "oz");
    echo(wbottom=wbottom, "oz");
    echo(wDT=wDT, "oz");
    echo("totalRobotWeight=", totalRobotWeight(), "oz");
    echo("maxAccel=", maxAccel(), "in/s^2");
    echo("topSpeed=", topSpeed(), "in/s");
    echo("timeToReachTopSpeed=", timeToReachTopSpeed(), "s");
   
   }
   
module runAllTests(){
    echo("RUNNING TESTS");
    if(!testArmMovesUnderShoulder()){
        echo("FAILS TEST: Arm Moves Under Shoulder");
    }
    if(!testArmReachesOutForwardToGround()){
        echo("FAILS TEST: Arm Reaches Out Forward To Ground");
    }
    shoulderTorque = shoulderTorque();
    echo(shoulderTorque=shoulderTorque, "oz-in");
    if(!canLiftMaxWeightAtShoulder()){
        echo("FAILS TEST: Can Lift Max Weight At Shoulder");
    }
    elbowTorque = elbowTorque();
    echo(elbowTorque=elbowTorque, "oz-in");
    if(!canLiftMaxWeightAtElbow()){
        echo("FAILS TEST: Can Lift Max Weight At Elbow");
    }
    wristTorque = wristTorque();
    echo(wristTorque=wristTorque, "oz-in");
    if(!canLiftMaxWeightAtWrist()){
        echo("FAILS TEST: Can Lift Max Weight At Wrist");
    }
   
    echo("linearLiftWeight=", linearLiftWeight(), "oz");
    if(!canLiftLinearWeight()){
        echo("FAILS TEST: Can Lift Linear Weight");
    }
   
    echo("TESTING COMPLETE");
    echo("******************************");
}

module run3SegmentSearch(){
    maxLengthOfSegment=inchesToMM(20);
    for(l1=[0: maxLengthOfSegment]){
        for(l2=[ 0 : maxLengthOfSegment]){
            for(l3=[ 0 : maxLengthOfSegment]){
            }
        }
    }
}

module run4SegmentSearch(){
    maxLengthOfSegment=inchesToMM(20);
    for(l1=[0: maxLengthOfSegment]){
        for(l2=[ 0 : maxLengthOfSegment]){
            for(l3=[ 0 : maxLengthOfSegment]){
                for(l4=[ 0 : maxLengthOfSegment]) {
                }
            }
        }
    }
}

module run5SegmentSearch(){
    maxLengthOfSegment=inchesToMm(20);
    for(l1=[0: maxLengthOfSegment]){
        for(l2=[ 0 : maxLengthOfSegment]){
            for(l3=[ 0 : maxLengthOfSegment]){
                for(l4=[ 0 : maxLengthOfSegment]) {
                    for(l5=[ 0 : maxLengthOfSegment]) { 
                    }
                }
            }
        }
    }
}

module arm(){
    
}