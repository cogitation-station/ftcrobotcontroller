// import statments
include <UtilityKit.scad>;

$fn = 100;

length = channelLength(1);
endPieces = false;

standardThickness = lineThickness*5;
size = plateThickness+standardThickness*2;

channelCap();

module wimpProtector(length = 10, endPieces = false) {
    union() { // main pieces
        translate([0, -size/2+standardThickness/2, 0]) cube([length, standardThickness, size], true);// wall Y-
        translate([0, size/2-standardThickness/2, 0]) cube([length, standardThickness, size], true);// wall y+
        translate([0, 0, -size/2+standardThickness/2]) cube([length, size, standardThickness], true); // bottom
    }
    
    if (endPieces) { // end pieces
        translate([length/2+standardThickness/2, 0, 0]) cube([standardThickness, size, size], true);// wall x+
        translate([-(length/2+standardThickness/2), 0, 0]) cube([standardThickness, size, size], true);// wall x-
    }
}

module channelCap() {
    difference() {
        union() {
            translate([length/2-plateThickness/2, 0, 0]) rotate([0, 0, 90]) wimpProtector(length, true);
            translate([0, length/2-plateThickness/2, 0]) wimpProtector(length, true);
            translate([0, -length/2+plateThickness/2, 0]) wimpProtector(length, true);
            //translate([gridSize*3-plateThickness, 0, 0]) rotate([90, 0, 90]) uChannel(1);
        }
        translate([0, length/2-plateThickness/2, standardThickness]) cube([length, plateThickness, size], true);
        translate([0, -length/2+plateThickness/2, standardThickness]) cube([length, plateThickness, size], true);
        translate([length/2-plateThickness/2, 0, standardThickness]) cube([plateThickness, length-standardThickness*2, size], true);
    }
    
}


