// import statments
include <UtilityKit.scad>;

$fn = 100;

function inchesToMillimeters(inches) = inches * 25.4;

turnPulleyRadius = 5;


hoseOuterRadius = inchesToMillimeters(.25)/2; // (in)
hoseInnerRadius = inchesToMillimeters(.169)/2; // (in)
hoseWallThickness = 2;
slotThickness = 10;
hosePinRadius = hoseOuterRadius;
hoseDistance = 10;
mountRadius = hoseOuterRadius+hoseWallThickness;

lineThickness = .4;

leverX = 50;
leverY = 10; 
leverZ = 3;

axleRadius = 2;
axleLength = 5;

pulleyRadius = 19;
distance = PI*pulleyRadius; // half circumfrence of pulley

capRadius = 4;
capThickness = 1;

pusherRadius = inchesToMillimeters(.6);
pusherThickness = 3;

holderRadius = 5; 
holderThickness = axleLength-lineThickness*2;

pistonRadius = 2.5; //(mm)
pistonLength = distance*2; //(mm)

pegX = 5;
pegY = 10;
pegZ = 5;

grabThickness = inchesToMillimeters(2);
grabInnerRadius = inchesToMillimeters(2);
grabOuterRadius = inchesToMillimeters(2)+2;

transitionCalc = ((leverZ/2 + lineThickness + axleLength-lineThickness*2) - (pegZ*2+lineThickness*2)/2);

standardThickness = 2;


pistonHolderRadius = (pistonRadius+lineThickness+2);



segmentY = 75/4;
segmentZ = 38;
segmentX = (distance*1.25+holderRadius*3+1);



pulleyCoords = mountRadius-standardThickness-lineThickness;

difference() {
    union() {
        hull() {
            translate([0, 0, pulleyCoords-1]) cylinder(1, turnPulleyRadius, turnPulleyRadius, false);
            translate([0, 0, 0]) cylinder(1, turnPulleyRadius/2, turnPulleyRadius/2, true);
        }
        translate([0, 0, 0]) rotate([0, 0, 0]) hull() {
            translate([0, 0, 0]) cylinder(1, turnPulleyRadius/2, turnPulleyRadius/2, true);
            translate([0, 0, -pulleyCoords]) cylinder(1, turnPulleyRadius, turnPulleyRadius, false);
        }
    }
    translate([0, 0, -pulleyCoords*2]) cylinder(pulleyCoords*5, smallHoleRadius, smallHoleRadius, false);
}