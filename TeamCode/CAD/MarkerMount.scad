// import statments
include <UtilityKit.scad>;
use <ChannelMount.scad>;

$fn = 100;

holderThickness = 5;
holderLength = 3;

build();

module build() {
    translate([-holderThickness, 0, 0]) channelMount(holderThickness, holderLength);
}

module hook() {
    
}